usage: pathena [options] <jobOption1.py> [<jobOption2.py> [...]]

  HowTo is available at https://panda-wms.readthedocs.io/en/latest/client/pathena.html

optional arguments:
  --helpGroup {PRINT,PATHENA,CONFIG,INPUT,OUTPUT,JOB,BUILD,SUBMIT,EVTFILTER,EXPERT,CONTAINERJOB,ALL}
                        Print individual group help (the group name is not
                        case-sensitive), where "ALL" will print all groups
                        together. Some options such as --inOutDsJson may SPAN
                        several groups
  -h, --help            Print this help

print:
  info print

  --debugMode           Send the job with the debug mode on. If this option is
                        specified the subjob will send stdout to the panda
                        monitor every 5 min. The number of debug subjobs per
                        user is limited. When this option is used and the
                        quota has already exceeded, the panda server supresses
                        the option so that subjobs will run without the debug
                        mode. If you submit multiple subjobs in a single job,
                        only the first subjob will set the debug mode on. Note
                        that you can turn the debug mode on/off by using pbook
                        after jobs are submitted
  -v                    Verbose

pathena:
  about pathena itself

  --version             Displays version
  --update              Update panda-client to the latest version

config:
  single configuration file to set multiple options

  --outRunConfig OUTRUNCONFIG
                        Save extracted config information to a local file
  --inRunConfig INRUNCONFIG
                        Use a saved config information to skip config
                        extraction
  --loadJson LOADJSON   Read command-line parameters from a json file which
                        contains a dict of {parameter: value}. Arguemnts for
                        Athena can be specified as {'atehna_args': [...,]}
  --dumpJson DUMPJSON   Dump all command-line parameters and submission result
                        such as returnCode, returnOut, jediTaskID, and
                        bulkSeqNumber if --bulkSubmission is used, to a json
                        file
  --parentTaskID PARENTTASKID, --parentTaskID PARENTTASKID
                        Set taskID of the paranet task to execute the task
                        while the parent is still running

input:
  input dataset(s)/files/format/seed

  --nEventsPerFile NEVENTSPERFILE
                        Number of events per file
  --inDS INDS           Input dataset names. wildcard and/or comma can be used
                        to concatenate multiple datasets
  --notExpandInDS       Allow jobs to use files across dataset boundaries in
                        input dataset container
  --inDsTxt INDSTXT     a text file which contains the list of datasets to run
                        over. newlines are replaced by commas and the result
                        is set to --inDS. lines starting with # are ignored
  --inOutDsJson INOUTDSJSON
                        A json file to specify input and output datasets for
                        bulk submission. It contains a json dump of [{'inDS':
                        a comma-concatenated input dataset names, 'outDS':
                        output dataset name}, ...]
  --secondaryDSs SECONDARYDSS
                        A versatile option to specify arbitrary secondary
                        inputs that takes a list of secondary datasets. See
                        PandaRun wiki page for detail
  --notExpandSecDSs     Use files across dataset boundaries in secondary
                        dataset containers
  --minDS MINDS         Dataset name for minimum bias stream
  --notExpandMinDS      Allow jobs to use files across dataset boundaries in
                        minimum bias dataset container
  --lowMinDS LOWMINDS   Dataset name for low pT minimum bias stream
  --notExpandLowMinDS   Allow jobs to use files across dataset boundaries in
                        low minimum bias dataset container
  --highMinDS HIGHMINDS
                        Dataset name for high pT minimum bias stream
  --notExpandHighMinDS  Allow jobs to use files across dataset boundaries in
                        high minimum bias dataset container
  --randomMin           randomize files in minimum bias dataset
  --cavDS CAVDS         Dataset name for cavern stream
  --notExpandCavDS      Allow jobs to use files across dataset boundaries in
                        cavern dataset container
  --randomCav           randomize files in cavern dataset
  --goodRunListDS GOODRUNLISTDS
                        A comma-separated list of pattern strings. Datasets
                        which are converted from Good Run List XML will be
                        used when they match with one of the pattern strings.
                        Either \ or "" is required when a wild-card is used.
                        If this option is omitted all datasets will be used
  --eventPickDS EVENTPICKDS
                        A comma-separated list of pattern strings. Datasets
                        which are converted from the run/event list will be
                        used when they match with one of the pattern strings.
                        Either \ or "" is required when a wild-card is used.
                        e.g., data\*
  --beamHaloDS BEAMHALODS
                        Dataset name for beam halo
  --beamHaloADS BEAMHALOADS
                        Dataset name for beam halo A-side
  --beamHaloCDS BEAMHALOCDS
                        Dataset name for beam halo C-side
  --beamGasDS BEAMGASDS
                        Dataset name for beam gas
  --beamGasHDS BEAMGASHDS
                        Dataset name for beam gas Hydrogen
  --beamGasCDS BEAMGASCDS
                        Dataset name for beam gas Carbon
  --beamGasODS BEAMGASODS
                        Dataset name for beam gas Oxygen
  --nFiles NFILES, --nfiles NFILES
                        Use an limited number of files in the input dataset
  --noRandom            Enter random seeds manually
  --forceStaged         Force files from primary DS to be staged to local
                        disk, even if direct-access is possible
  --avoidVP             Not to use sites where virtual placement is enabled
  --generalInput        Read input files with general format except
                        POOL,ROOT,ByteStream
  --shipInput           Ship input files to remote WNs
  --inputFileList INPUTFILELIST
                        A local file which specifies names of files to be used
                        in the input dataset. One filename per line in the the
                        local file
  --notSkipMissing      If input files are not read from SE, they will be
                        skipped by default. This option disables the
                        functionality
  --forceDirectIO       Use directIO if directIO is available at the site
  --inputType INPUTTYPE
                        A regular expression pattern. Only files matching with
                        the pattern in input dataset are used
  --pfnList PFNLIST     Name of file which contains a list of input PFNs.
                        Those files can be un-registered in DDM
  --skipFilesUsedBy SKIPFILESUSEDBY
                        A comma-separated list of TaskIDs. Files used by those
                        tasks are skipped when running a new task
  --respectLB           To generate jobs repecting lumiblock boundaries

output:
  output dataset/files

  --inOutDsJson INOUTDSJSON
                        A json file to specify input and output datasets for
                        bulk submission. It contains a json dump of [{'inDS':
                        a comma-concatenated input dataset names, 'outDS':
                        output dataset name}, ...]
  --addNthFieldOfInDSToLFN ADDNTHFIELDOFINDSTOLFN
                        A middle name is added to LFNs of output files when
                        they are produced from one dataset in the input
                        container or input dataset list. The middle name is
                        extracted from the dataset name. E.g., if
                        --addNthFieldOfInDSToLFN=2 and the dataset name is
                        data10_7TeV.00160387.physics_Muon..., 00160387 is
                        extracted and LFN is something like
                        user.hoge.TASKID.00160387.blah. Concatenate multiple
                        field numbers with commas if necessary, e.g.,
                        --addNthFieldOfInDSToLFN=2,6.
  --addNthFieldOfInFileToLFN ADDNTHFIELDOFINFILETOLFN
                        A middle name is added to LFNs of output files
                        similarly as --addNthFieldOfInDSToLFN, but strings are
                        extracted from input file names
  --appendStrToExtStream
                        append the first part of filenames to extra stream
                        names for --individualOutDS. E.g., if this option is
                        used together with --individualOutDS,
                        %OUT.AOD.pool.root will be contained in an EXT0_AOD
                        dataset instead of an EXT0 dataset
  --mergeOutput         merge output files
  --mergeLog            merge log files. relevant only with --mergeOutput
  --mergeScript MERGESCRIPT
                        Specify user-defied script or execution string for
                        output merging
  --outDS OUTDS         Name of an output dataset. OUTDS will contain all
                        output files
  --destSE DESTSE       Destination strorage element
  --noOutput            Send job even if there is no output file
  --official            Produce official dataset
  --unlimitNumOutputs   Remove the limit on the number of outputs. Note that
                        having too many outputs per job causes a severe load
                        on the system. You may be banned if you carelessly use
                        this option
  --descriptionInLFN DESCRIPTIONINLFN
                        LFN is user.nickname.jobsetID.something (e.g.
                        user.harumaki.12345.AOD._00001.pool) by default. This
                        option allows users to put a description string into
                        LFN. i.e.,
                        user.nickname.jobsetID.description.something
  --extOutFile EXTOUTFILE
                        A comma-separated list of extra output files which
                        cannot be extracted automatically. Either \ or "" is
                        required when a wildcard is used. e.g.,
                        output1.txt,output2.dat,JiveXML_\*.xml
  --supStream SUPSTREAM
                        suppress some output streams. Either \ or "" is
                        required when a wildcard is used. e.g.,
                        ESD,TAG,GLOBAL,StreamDESD\*
  --allowNoOutput ALLOWNOOUTPUT
                        A comma-separated list of regexp patterns. Output
                        files are allowed not to be produced if their
                        filenames match with one of regexp patterns. Jobs go
                        to finished even if they are not produced on WN
  --spaceToken SPACETOKEN
                        spacetoken for outputs. e.g., ATLASLOCALGROUPDISK
  --allowTaskDuplication
                        As a general rule each task has a unique outDS and
                        history of file usage is recorded per task. This
                        option allows multiple tasks to contribute to the same
                        outDS. Typically useful to submit a new task with the
                        outDS which was used by another broken task. Use this
                        option very carefully at your own risk, since file
                        duplication happens when the second task runs on the
                        same input which the first task successfully processed

job:
  job running control on grid

  --split nJobs, --nJobs nJobs
                        Number of sub-jobs to be generated.
  --nFilesPerJob NFILESPERJOB
                        Number of files on which each sub-job runs
  --nEventsPerJob NEVENTSPERJOB
                        Number of events per subjob. This info is used mainly
                        for job splitting. If you run on MC datasets, the
                        total number of subjobs is
                        nEventsPerFile*nFiles/nEventsPerJob. For data, the
                        number of events for each file is retrieved from AMI
                        and subjobs are created accordingly. Note that if you
                        run transformations you need to explicitly specify
                        maxEvents or something in --trf to set the number of
                        events processed in each subjob. If you run normal
                        jobOption files, evtMax and skipEvents in appMgr are
                        automatically set on WN.
  --nEventsPerFile NEVENTSPERFILE
                        Number of events per file
  --nGBPerJob NGBPERJOB
                        Instantiate one sub job per NGBPERJOB GB of input
                        files. --nGBPerJob=MAX sets the size to the default
                        maximum value
  --nGBPerMergeJob NGBPERMERGEJOB
                        Instantiate one merge job per NGBPERMERGEJOB GB of
                        pre-merged files
  --nMin NMIN           Number of minimum bias files per sub job
  --nLowMin NLOWMIN     Number of low pT minimum bias files per job
  --nHighMin NHIGHMIN   Number of high pT minimum bias files per job
  --nCav NCAV           Number of cavern files per job
  --useAMIEventLevelSplit
                        retrive the number of events per file from AMI to
                        split the job using --nEventsPerJob
  --mergeScript MERGESCRIPT
                        Specify user-defied script or execution string for
                        output merging
  --useCommonHalo       use an integrated DS for BeamHalo
  --nBeamHalo NBEAMHALO
                        Number of beam halo files per sub job
  --nBeamHaloA NBEAMHALOA
                        Number of beam halo files for A-side per sub job
  --nBeamHaloC NBEAMHALOC
                        Number of beam halo files for C-side per sub job
  --useCommonGas        use an integrated DS for BeamGas
  --nBeamGas NBEAMGAS   Number of beam gas files per sub job
  --nBeamGasH NBEAMGASH
                        Number of beam gas files for Hydrogen per sub job
  --nBeamGasC NBEAMGASC
                        Number of beam gas files for Carbon per sub job
  --nBeamGasO NBEAMGASO
                        Number of beam gas files for Oxygen per sub job
  --useAMIAutoConf      Use AMI for AutoConfiguration
  --nThreads NTHREADS   The number of threads for AthenaMT. If this option is
                        set to larger than 1, Athena is executed with
                        --threads=$ATHENA_PROC_NUMBER at sites which have
                        nCore>1. This means that even if you set nThreads=2
                        jobs can go to sites with nCore=8 and your application
                        will use the 8 cores there
  --unlimitNumOutputs   Remove the limit on the number of outputs. Note that
                        having too many outputs per job causes a severe load
                        on the system. You may be banned if you carelessly use
                        this option
  --allowNoOutput ALLOWNOOUTPUT
                        A comma-separated list of regexp patterns. Output
                        files are allowed not to be produced if their
                        filenames match with one of regexp patterns. Jobs go
                        to finished even if they are not produced on WN
  --useNextEvent        Set this option if your jobO uses theApp.nextEvent(),
                        e.g. for G4. Note that this option is not required
                        when you run transformations using --trf
  --trf TRF             run transformation, e.g. --trf "csc_atlfast_trf.py %IN
                        %OUT.AOD.root %OUT.ntuple.root -1 0"
  --respectSplitRule    force scout jobs to follow split rules like nGBPerJob
  -c COMMAND            One-liner, runs before any jobOs
  -p BOOTSTRAP          location of bootstrap file
  -s                    show printout of included files
  --cpuTimePerEvent CPUTIMEPEREVENT
                        Expected HS06 seconds per event (~= 10 * the expected
                        duration per event in seconds)
  --fixedCpuTime        Use fixed cpuTime instead of estimated cpuTime
  --maxWalltime MAXWALLTIME
                        Max walltime for each job in hours. Note that this
                        option works only when the nevents metadata of input
                        files are available in rucio

build:
  build/compile the package and env setup

  --athenaTag ATHENATAG
                        Use differnet version of Athena on remote WN. By
                        defualt the same version which you are locally using
                        is set up on WN. e.g.,
                        --athenaTag=AtlasProduction,14.2.24.3
  --noBuild             Skip buildJob
  --noCompile           Just upload a tarball in the build step to avoid the
                        tighter size limit imposed by --noBuild. The tarball
                        contains binaries compiled on your local computer, so
                        that compilation is skipped in the build step on
                        remote WN
  --extFile EXTFILE     pathena exports files with some special extensions
                        (.C, .dat, .py .xml) in the current directory. If you
                        want to add other files, specify their names, e.g.,
                        data1.root,data2.doc
  --excludeFile EXCLUDEFILE
                        specify a comma-separated string to exclude files
                        and/or directories when gathering files in local
                        working area. Either \ or "" is required when a
                        wildcard is used. e.g., doc,\*.C
  --gluePackages GLUEPACKAGES
                        list of glue packages which pathena cannot find due to
                        empty i686-slc4-gcc34-opt. e.g.,
                        External/AtlasHepMC,External/Lhapdf
  --tmpDir TMPDIR       Temporary directory in which an archive file is
                        created
  --dbRelease DBRELEASE
                        DBRelease or CDRelease (DatasetName:FileName). e.g., d
                        do.000001.Atlas.Ideal.DBRelease.v050101:DBRelease-5.1.
                        1.tar.gz. If --dbRelease=LATEST, the latest DBRelease
                        is used
  --addPoolFC ADDPOOLFC
                        file names to be inserted into PoolFileCatalog.xml
                        except input files. e.g., MyCalib1.root,MyGeom2.root
  --voms VOMSROLES      generate proxy with paticular roles. e.g., atlas:/atla
                        s/ca/Role=production,atlas:/atlas/fr/Role=pilot
  --outTarBall OUTTARBALL
                        Save a gzipped tarball of local files which is the
                        input to buildXYZ
  --inTarBall INTARBALL
                        Use a gzipped tarball of local files as input to
                        buildXYZ. Generall the tarball is created by using
                        --outTarBall
  --cmtConfig CMTCONFIG
                        CMTCONFIG=i686-slc5-gcc43-opt is used on remote
                        worker-node by default even if you use another
                        CMTCONFIG locally. This option allows you to use
                        another CMTCONFIG remotely. e.g., --cmtConfig
                        x86_64-slc5-gcc43-opt.
  -3                    Use python3

submit:
  job submission/site/retry

  --site SITE           Site name where jobs are sent. If omitted, jobs are
                        automatically sent to sites where input is available.
                        A comma-separated list of sites can be specified (e.g.
                        siteA,siteB,siteC), so that best sites are chosen from
                        the given site list. If AUTO is appended at the end of
                        the list (e.g. siteA,siteB,siteC,AUTO), jobs are sent
                        to any sites if input is not found in the previous
                        sites
  --sameSecRetry        Use the same secondary input files when jobs are
                        retried
  --express             Send the job using express quota to have higher
                        priority. The number of express subjobs in the queue
                        and the total execution time used by express subjobs
                        are limited (a few subjobs and several hours per day,
                        respectively). This option is intended to be used for
                        quick tests before bulk submission. Note that buildXYZ
                        is not included in quota calculation. If this option
                        is used when quota has already exceeded, the panda
                        server will ignore the option so that subjobs have
                        normal priorities. Also, if you submit 1 buildXYZ and
                        N runXYZ subjobs when you only have quota of M (M <
                        N), only the first M runXYZ subjobs will have higher
                        priorities
  --noEmail             Suppress email notification
  --bulkSubmission      Bulk submit tasks. When this option is used,
                        --inOutDsJson is required while --inDS and --outDS are
                        ignored. It is possible to use %DATASET_IN and
                        %DATASET_OUT in --trf which are replaced with actual
                        dataset names when tasks are submitted, and
                        %BULKSEQNUMBER which is replaced with a sequential
                        number of tasks in the bulk submission
  --noOutput            Send job even if there is no output file
  --memory MEMORY       Required memory size in MB per core. e.g., for 1GB per
                        core --memory 1024
  --fixedRamCount       Use fixed memory size instead of estimated memory size
  --outDiskCount OUTDISKCOUNT
                        Expected output size in kB per 1 MB of input. The
                        system automatically calculates this value using
                        successful jobs and the value contains a safety offset
                        (100kB). Use this option to disable it when jobs
                        cannot have enough input files due to the offset
  --nCore NCORE         The number of CPU cores. Note that the system
                        distinguishes only nCore=1 and nCore>1. This means
                        that even if you set nCore=2 jobs can go to sites with
                        nCore=8 and your application must use the 8 cores
                        there. The number of available cores is defined in an
                        environment variable, $ATHENA_PROC_NUMBER, on WNs.
                        Your application must check the env variable when
                        starting up to dynamically change the number of cores
  --nThreads NTHREADS   The number of threads for AthenaMT. If this option is
                        set to larger than 1, Athena is executed with
                        --threads=$ATHENA_PROC_NUMBER at sites which have
                        nCore>1. This means that even if you set nThreads=2
                        jobs can go to sites with nCore=8 and your application
                        will use the 8 cores there
  --excludedSite EXCLUDEDSITE
                        A comma-separated list of sites which are not used for
                        site section, e.g., ABC,OPQ*,XYZ which excludes ABC,
                        XYZ, and OPQ<blah> due to the wildcard
  --noSubmit            Don't submit jobs
  --prodSourceLabel PRODSOURCELABEL
                        set prodSourceLabel
  --processingType PROCESSINGTYPE
                        set processingType
  --workingGroup WORKINGGROUP
                        set workingGroup
  --disableAutoRetry    disable automatic job retry on the server side
  --maxAttempt MAXATTEMPT
                        Maximum number of reattempts for each job (3 by
                        default and not larger than 50)
  --useNewCode          When task are resubmitted with the same outDS, the
                        original souce code is used to re-run on
                        failed/unprocessed files. This option uploads new
                        source code so that jobs will run with new binaries
  --priority PRIORITY   Set priority of the task (1000 by default). The value
                        must be between 900 and 1100. Note that priorities of
                        tasks are relevant only in each user's share, i.e.,
                        your tasks cannot jump over other user's tasks even if
                        you give higher priorities.
  --osMatching          To let the brokerage choose sites which have the same
                        OS as the local machine has.

evtFilter:
  event filter such as good run and event pick

  --goodRunListXML GOODRUNLISTXML
                        Good Run List XML which will be converted to datasets
                        by AMI
  --goodRunListDataType GOODRUNDATATYPE
                        specify data type when converting Good Run List XML to
                        datasets, e.g, AOD (default)
  --goodRunListProdStep GOODRUNPRODSTEP
                        specify production step when converting Good Run List
                        to datasets, e.g, merge (default)
  --goodRunListDS GOODRUNLISTDS
                        A comma-separated list of pattern strings. Datasets
                        which are converted from Good Run List XML will be
                        used when they match with one of the pattern strings.
                        Either \ or "" is required when a wild-card is used.
                        If this option is omitted all datasets will be used
  --eventPickEvtList EVENTPICKEVTLIST
                        a file name which contains a list of runs/events for
                        event picking
  --eventPickDataType EVENTPICKDATATYPE
                        type of data for event picking. one of AOD,ESD,RAW
  --ei_api EI_API       flag to signalise mc in event picking
  --eventPickStreamName EVENTPICKSTREAMNAME
                        stream name for event picking. e.g.,
                        physics_CosmicCaloEM
  --eventPickDS EVENTPICKDS
                        A comma-separated list of pattern strings. Datasets
                        which are converted from the run/event list will be
                        used when they match with one of the pattern strings.
                        Either \ or "" is required when a wild-card is used.
                        e.g., data\*
  --eventPickAmiTag EVENTPICKAMITAG
                        AMI tag used to match TAG collections names. This
                        option is required when you are interested in older
                        data than the latest one. Either \ or "" is required
                        when a wild-card is used. e.g., f2\*
  --eventPickWithGUID   Using GUIDs together with run and event numbers in
                        eventPickEvtList to skip event lookup

expert:
  for experts/developers only

  --noLoopingCheck      Disable looping job check
  --devSrv              Please don't use this option. Only for developers to
                        use the dev panda server
  --intrSrv             Please don't use this option. Only for developers to
                        use the intr panda server
  --queueData QUEUEDATA
                        Please don't use this option. Only for developers

containerJob:
  For container-based jobs

  --containerImage CONTAINERIMAGE
                        Name of a container image
  --architecture ARCHITECTURE
                        Base OS platform, CPU, and/or GPU requirements. The
                        format is @base_platform#CPU_spec&GPU_spec where base
                        platform, CPU, or GPU spec can be omitted. If base
                        platform is not specified it is automatically taken
                        from $ALRB_USER_PLATFORM. CPU_spec =
                        architecture<-vendor<-instruction set>>, GPU_spec =
                        vendor<-model>. A wildcards can be used if there is no
                        special requirement for the attribute. E.g.,
                        #x86_64-*-avx2&nvidia to ask for x86_64 CPU with avx2
                        support and nvidia GPU
