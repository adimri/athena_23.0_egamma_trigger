#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackingPrimitives.h" //Particle Hypothesis
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>


#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

using namespace std;

  MyxAODAnalysis :: MyxAODAnalysis (const std::string &name,
                                    ISvcLocator *pSvcLocator)
      : EL::AnaAlgorithm (name, pSvcLocator)
      ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
      ,m_matchtool(this,"Trig::MatchingTool")
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    ANA_MSG_INFO ("Inside  Constructor.");

    declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
                    "Minimum electron pT (in MeV)" );
    declareProperty( "SampleName", m_sampleName = "Unknown",
                    "Descriptive name for the processed sample" );
    declareProperty( "JetPtCut", m_JetPtCut = 20.0,
                    "Minimum Jet pT (in MeV)" );
    
    //For Trigger Tools
    declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
    declareProperty("triggers", m_trigList, "trigger selection list");

    //Matching
    declareProperty("MatchingTool", m_matchtool);
    

  }

  void MyxAODAnalysis::SetHists(TH1* My_hist,
      const std::string &xaxistitle="None",const std::string &yaxistitle="None"){

        //Here I just set some basic properties of the histogram that I will define.

        My_hist->GetXaxis()->SetTitle(xaxistitle.c_str());
        My_hist->GetYaxis()->SetTitle(yaxistitle.c_str());
      }

  StatusCode MyxAODAnalysis :: initialize ()
  {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_INFO ("In Initialize.");

    limit_nof_triggers();
    limit_nof_triggers_Photons();
    
    //Define the histograms
    Init_Hists();

    //Check if the decision tool is working properly
    ANA_CHECK(trigDecisionTool.retrieve());
    ANA_CHECK(m_matchtool.retrieve());
    
    // GRL. Good Run List Tool
    m_grl.setTypeAndName("GoodRunsListSelectionTool/myGRLTool");

    //Offline Electron Selector Tools (from Qipeng)
    std::string confDir = "ElectronPhotonSelectorTools/offline/mc23_20230728_HI/";
    m_electronLooseLHSelector = new AsgElectronLikelihoodTool ("LooseLH");;
    //ANA_CHECK(m_electronLooseLHSelector->setProperty("WorkingPoint", "LooseLHElectron"));
    ANA_CHECK(m_electronLooseLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronLooseLHSelector->initialize());

    m_electronMediumLHSelector = new AsgElectronLikelihoodTool ("MediumLH");;
    //ANA_CHECK(m_electronMediumLHSelector->setProperty("WorkingPoint", "MediumLHElectron"));
    ANA_CHECK(m_electronMediumLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodMediumOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronMediumLHSelector->initialize());

    m_electronTightLHSelector = new AsgElectronLikelihoodTool ("TightLH");;
    //ANA_CHECK(m_electronTightLHSelector->setProperty("WorkingPoint", "TightLHElectron"));
    ANA_CHECK(m_electronTightLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodTightOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronTightLHSelector->initialize());

    //Offline Photon Tool Selectors (from Qipeng)
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    ANA_CHECK(m_photonTightIsEMSelector->setProperty("WorkingPoint", "TightPhoton"));
    ANA_CHECK(m_photonTightIsEMSelector->initialize());

    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
    ANA_CHECK(m_photonLooseIsEMSelector->setProperty("WorkingPoint", "LoosePhoton"));
    ANA_CHECK(m_photonLooseIsEMSelector->initialize());

    m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonMediumIsEMSelector" );
    ANA_CHECK(m_photonMediumIsEMSelector->setProperty("WorkingPoint", "MediumPhoton"));
    ANA_CHECK(m_photonMediumIsEMSelector->initialize());


    //Open File for checking purposes
    ns.open("CheckFile_New.txt");
    ns<<".....................Begin....................."<<endl;

    return StatusCode::SUCCESS;
  }

  
  void MyxAODAnalysis::Init_Hists(){
    std::string Ghost_name;
    double pi = TMath::Pi();
    char name[500];
    
    //For Electrons
    for(int trig_count=0;trig_count<nof_triggers;trig_count++){
      for(int i=0;i<4;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_trigList.at(trig_count);
            if(i==0) Ghost_name += "_All_Off";
            if(i==1) Ghost_name += "_Loose_LH_Off";
            if(i==2) Ghost_name += "_Medium_LH_Off";
            if(i==3) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "_SumFcalEt";
            if(j==1) Ghost_name += "_Pt";
            if(j==2) Ghost_name += "_Eta";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            for(int l=0;l<2;l++){
              if(l==0) Ghost_name +="_pt_All";
              if(l==1) Ghost_name +="_pt_geq_20";

              if(j==0){
                sprintf(name,"Electron_Evt_%s",Ghost_name.c_str());
                Electron_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);

                //For saving the denominator
                sprintf(name,"Electron_Total_%s",Ghost_name.c_str());
                Electron_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
              }

              if(j==1){
                sprintf(name,"Electron_Evt_%s",Ghost_name.c_str());
                Electron_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);

                //For saving the denominator
                sprintf(name,"Electron_Total_%s",Ghost_name.c_str());
                Electron_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
              }
              
              if(j==2){
                sprintf(name,"Electron_Evt_%s",Ghost_name.c_str());
                Electron_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);

                //For saving the denominator
                sprintf(name,"Electron_Total_%s",Ghost_name.c_str());
                Electron_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
              }

            }
          }
        }
      }
    }
    
    //For Photons
    for(int trig_count=0;trig_count<nof_triggers_Photons;trig_count++){
      for(int i=0;i<4;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_trigList_Photons.at(trig_count);
            if(i==0) Ghost_name += "_All_Off";
            if(i==1) Ghost_name += "_Loose_LH_Off";
            if(i==2) Ghost_name += "_Medium_LH_Off";
            if(i==3) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "_SumFcalEt";
            if(j==1) Ghost_name += "_Pt";
            if(j==2) Ghost_name += "_Eta";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            for(int l=0;l<2;l++){
              if(l==0) Ghost_name +="_pt_All";
              if(l==1) Ghost_name +="_pt_geq_20";

              if(j==0){
                sprintf(name,"Photon_Evt_%s",Ghost_name.c_str());
                Photon_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);

                //For saving the denominator
                sprintf(name,"Photon_Total_%s",Ghost_name.c_str());
                Photon_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
              }

              if(j==1){
                sprintf(name,"Photon_Evt_%s",Ghost_name.c_str());
                Photon_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);

                //For saving the denominator
                sprintf(name,"Photon_Total_%s",Ghost_name.c_str());
                Photon_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
              }
              
              if(j==2){
                sprintf(name,"Photon_Evt_%s",Ghost_name.c_str());
                Photon_Hists_Pass[trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);

                //For saving the denominator
                sprintf(name,"Photon_Total_%s",Ghost_name.c_str());
                Photon_Hists_Total[trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
              }

            }
          }
        }
      }
    }

    //For Efficiency of L1 wrt MinBias
    for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<4;i++){
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_L1_trigList.at(trig_count)+"_"+m_L1_ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_All_Off";
              if(i==1) Ghost_name += "_Loose_LH_Off";
              if(i==2) Ghost_name += "_Medium_LH_Off";
              if(i==3) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "_SumFcalEt";
              if(j==1) Ghost_name += "_Pt";
              if(j==2) Ghost_name += "_Eta";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              for(int l=0;l<2;l++){
                if(l==0) Ghost_name +="_pt_All";
                if(l==1) Ghost_name +="_pt_geq_20";

                if(j==0){
                  sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                  sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);

                  //For saving the denominator
                  sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                  sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                }

                if(j==1){
                  sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                  sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);

                  //For saving the denominator
                  sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                  sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                }
                
                if(j==2){
                  sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                  sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);

                  //For saving the denominator
                  sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
                  Photon_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                  sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                }

              }
            }
          }
        }
      }
    }
    
    /*
    //Photon HLT Trigger Efficiency wrt Min Bias
    for(int trig_count=0;trig_count<nof_HLT_triggers_Photon;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<4;i++){
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_trigList_Photon.at(trig_count)+"_"+m_L1_ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_All_Off";
              if(i==1) Ghost_name += "_Loose_LH_Off";
              if(i==2) Ghost_name += "_Medium_LH_Off";
              if(i==3) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "_SumFcalEt";
              if(j==1) Ghost_name += "_Pt";
              if(j==2) Ghost_name += "_Eta";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              for(int l=0;l<2;l++){
                if(l==0) Ghost_name +="_pt_All";
                if(l==1) Ghost_name +="_pt_geq_20";

                if(j==0){
                  sprintf(name,"Photon_HLT_MB_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                  // sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);

                  //For saving the denominator
                  sprintf(name,"Photon_HLT_MB_Total_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                  // sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,7000,-0.5,6999.5);
                }

                if(j==1){
                  sprintf(name,"Photon_HLT_MB_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                  // sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);

                  //For saving the denominator
                  sprintf(name,"Photon_HLT_MB_Total_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                  // sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,500,-0.5,499.5);
                }
                
                if(j==2){
                  sprintf(name,"Photon_HLT_MB_Evt_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                  // sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);

                  //For saving the denominator
                  sprintf(name,"Photon_HLT_MB_Total_%s",Ghost_name.c_str());
                  Photon_Hists_MB_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                  // sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                  // Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l] = new TH1D(name,name,1200,-6,6);
                }

              }
            }
          }
        }
      }
    }
    */

    ANA_MSG_INFO("Initialization Of Histograms completed.");
  }
  

  //By Hand Execution of dR calculation
  double MyxAODAnalysis::dR(double eta1,double phi1, double eta2, double phi2){
    double deta = fabs(eta1 - eta2);
    double dphi = 0;

    if(fabs(phi1 - phi2) < TMath::Pi()){
      dphi = fabs(phi1 - phi2);
    }
    else{
      dphi = 2*TMath::Pi() - fabs(phi1 - phi2);
    }
    
    return sqrt(deta*deta + dphi*dphi);

  }
  
  //By hand execution of L1_eEM15 matching for Electron
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()<13*1e-3){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_eEM15 matching for Photon
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()<13*1e-3){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_EM12 matching for Electron
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  //By hand execution of L1_EM12 matching for Photon
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  StatusCode MyxAODAnalysis :: execute ()
  {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.


    //Main Code Here
    //Retrieve the eventInfo object from the event store
    const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

    ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    
    if (!m_grl->passRunLB(*eventInfo)) { //Check if the run passes the good run list tool
      ANA_MSG_DEBUG ("drop event: GRL");
      return StatusCode::SUCCESS;
    }

    const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
    ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get photon pointer

    const xAOD::PhotonContainer *Online_photon_info = nullptr; //Define online photon pointer
    ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_egamma_Photons"));  //Get photon pointer

    const xAOD::ElectronContainer *electron_info = nullptr; //Define electron pointer
    ANA_CHECK (evtStore()->retrieve (electron_info, "Electrons"));  //Get electron pointer

    const xAOD::ElectronContainer *Online_electron_info = nullptr; //Define Online electron pointer
    ANA_CHECK (evtStore()->retrieve (Online_electron_info, "HLT_egamma_Electrons"));  //Get electron pointer


    std::vector<const xAOD::IParticle*> myParticles;
    
    
    //Making Efficiency Plots
    
    //Compute fcalEt of the Event
    const xAOD::HIEventShapeContainer *hies = 0;  
    ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

    double m_sumEt_A = 0;
    double m_sumEt_C = 0;
    for (const xAOD::HIEventShape *ES : *hies) {
      double et = ES->et()*1e-3;
      double eta = ES->etaMin();

      int layer = ES->layer();
      if (layer != 21 && layer != 22 && layer != 23) continue;
      if (eta > 0) {
        m_sumEt_A += et;
      } else {
        m_sumEt_C += et;
      }
    }
    double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
    // if(m_sumEt>2000) ANA_MSG_INFO("Fcal Sum_Et = "<<m_sumEt);

    
    
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
    ns<<endl;

    const xAOD::EmTauRoIContainer *m_EMTauRoIs = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_EMTauRoIs, "LVL1EmTauRoIs"));  //Get the event info pointer  

    const xAOD::eFexEMRoIContainer *m_eEM15 = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_eEM15, "L1_eEMRoI"));  //Get the event info pointer

    
    //Below lies the code to do trigger matching for both electrons and photons
    
    

    // ANA_MSG_INFO ("No of Triggers: "<<nof_triggers);
    // exit(0);
    
    for(int trig_count=0;trig_count<nof_triggers;trig_count++){//Loop over all triggers in the list
      string ref_trigname = m_ref_triglist.at(trig_count);
      string trigname = m_trigList.at(trig_count);  //Get The Trigger Name
      bool trigDecision = trigDecisionTool->isPassed(ref_trigname);//Event passed the relevant trigger?

      if(trigDecision){ 
        ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        ns<<endl;

        //Electron Work Begins.
        bool off_elec_type[4]; //Category of Offline Electron. All,Loose,Tight

        if(electron_info->size()>0){
          // ns<<"Some Offline Electrons!"<<endl;
          for(const auto eg : *electron_info){  //Loop Over Offline Electrons
            int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one
            bool pass_ref=false;//Check if the same offline object matches with reference trigger 
            
            //Initializing pass ref
            if(ref_trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
              for (const auto EMTauRoI: *m_EMTauRoIs){ 
                pass_ref = L1_eEM12_TrigMatch(eg,EMTauRoI,pass_ref);
                if(pass_ref) {
                  // ns<<"EM12 Electron Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                  break;       
                }
              }
            }
            else if(ref_trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
              for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                pass_ref = L1_eEM15_TrigMatch(eg,eEM15,pass_ref);
                if(pass_ref) {
                  // ns<<"EM15 Electron Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                  break;
                }
              }
            }
            else {  //Matching the remaining reference triggers using matchtool. This function cannot match L1 triggers for some reason.
              pass_ref=m_matchtool->match({eg},ref_trigname);
            }

            double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            double eg_phi = eg->phi();
            
            off_elec_type[0] = true; 
            off_elec_type[1] = bool(m_electronLooseLHSelector ->accept(eg));
            off_elec_type[2] = bool(m_electronMediumLHSelector->accept(eg));
            off_elec_type[3] = bool(m_electronTightLHSelector->accept(eg));

            //Checking Purposes
            // off_elec_type[1] = true;
            // off_elec_type[2] = true;
            // off_elec_type[3] = true;

            // ns<<"Off_Electron_Type = ("<<off_elec_type[0]<<","<<off_elec_type[1]<<","<<off_elec_type[2]<<","<<off_elec_type[3]<<")"<<endl;
            // ns<<"(Pass_Ref,Pass) = ("<<pass_ref<<","<<pass<<")"<<endl;

            if(pass_ref){ 
              if(pass){ //Histograms for electrons that pass the actual and reference trigger
                // idx++;
                ns<<"Electron # "<<idx<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<". Matches Trigger: "<<trigname<<" and Reference Trigger: "<<ref_trigname<<endl;
                for(int electron_type=0;electron_type<4;electron_type++){  
                  if(!off_elec_type[electron_type]) continue;
                  for(int j=0;j<3;j++){
                    double Hist_xval;
                    if(j==0) Hist_xval = m_sumEt;
                    if(j==1) Hist_xval = eg_pt;
                    if(j==2) Hist_xval = eg_eta;

                    if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Electron_Hists_Pass[trig_count][electron_type][j][0][1]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Electron_Hists_Pass[trig_count][electron_type][j][1][1]->Fill(Hist_xval);
                      } 
                      
                      Electron_Hists_Pass[trig_count][electron_type][j][2][1]->Fill(Hist_xval);
                    }  
                    if(true){ //All pt. Last Index(5th) should be 0.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Electron_Hists_Pass[trig_count][electron_type][j][0][0]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Electron_Hists_Pass[trig_count][electron_type][j][1][0]->Fill(Hist_xval);
                      } 
                      
                      Electron_Hists_Pass[trig_count][electron_type][j][2][0]->Fill(Hist_xval);
                    }
                  }//Fcal Et, pt, eta

                }//electron type loop ends
              } //m_matchtool pass condition ends

              //Histograms showing all electrons in the passes the reference trigger
              for(int electron_type=0;electron_type<4;electron_type++){  
                if(!off_elec_type[electron_type]) continue;

                for(int j=0;j<3;j++){
                  double Hist_xval;
                  if(j==0) Hist_xval = m_sumEt;
                  if(j==1) Hist_xval = eg_pt;
                  if(j==2) Hist_xval = eg_eta;

                  if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Electron_Hists_Total[trig_count][electron_type][j][0][1]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Electron_Hists_Total[trig_count][electron_type][j][1][1]->Fill(Hist_xval);
                    } 
                    
                    Electron_Hists_Total[trig_count][electron_type][j][2][1]->Fill(Hist_xval);
                  }
                  if(true){ //All pt. Last Index(5th) should be 0.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Electron_Hists_Total[trig_count][electron_type][j][0][0]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Electron_Hists_Total[trig_count][electron_type][j][1][0]->Fill(Hist_xval);
                    } 
                    
                    Electron_Hists_Total[trig_count][electron_type][j][2][0]->Fill(Hist_xval);
                  }
                } //Reference filling ends

              }//electron type loop ends
            }//pass_ref condition ends
          }//Offline Electron Loop ends
        }//Offline Electron Size Condition
        else{
        // ns<<"This event has no offline electrons."<<endl;
        }
        // ns<<"Exited the Offline Electron Size condition.";
        //Electron Work Ends.
        
      }//TrigDecision condition.
      else{
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" fails the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        // ns<<endl;
      }
    }

    for(int trig_count=0;trig_count<nof_triggers_Photons;trig_count++){//Loop over all photon triggers in the list
      string ref_trigname = m_ref_triglist_Photons.at(trig_count); //Get Reference Trigger Name
      string trigname = m_trigList_Photons.at(trig_count);  //Get The Trigger Name
      bool trigDecision = trigDecisionTool->isPassed(ref_trigname);//Event passed the relevant trigger?

      if(trigDecision){ 
        ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        ns<<endl;
        
        //Work for Photon Begins
        bool off_photon_type[4];
        
        if(photon_info->size()>0){
          for(const auto eg : *photon_info){  //Loop Over Offline Photons
            int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one
            bool pass_ref = false; //Check if the same offline object matches with reference trigger
            
            //Initializing pass ref
            if(ref_trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
              for (const auto EMTauRoI: *m_EMTauRoIs){ 
                pass_ref = L1_eEM12_TrigMatch(eg,EMTauRoI,pass_ref);
                if(pass_ref) {
                  // ns<<"EM12 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                  break;       
                }
              }
            }
            else if(ref_trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
              for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                pass_ref = L1_eEM15_TrigMatch(eg,eEM15,pass_ref);
                if(pass_ref) {
                  // ns<<"EM15 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                  break;
                }
              }
            }
            else {  //Matching the remaining reference triggers using matchtool. This function cannot match L1 triggers for some reason.
              pass_ref=m_matchtool->match({eg},ref_trigname);
            }

            //Photon Properties
            double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            double eg_phi = eg->phi();
            
            off_photon_type[0] = true;
            off_photon_type[1] = bool(m_photonLooseIsEMSelector->accept(eg));
            off_photon_type[2] = bool(m_photonMediumIsEMSelector->accept(eg));
            off_photon_type[3] = bool(m_photonTightIsEMSelector->accept(eg));
            
            // ns<<"Off_Photon_Type = ("<<off_photon_type[0]<<","<<off_photon_type[1]<<","<<off_photon_type[2]<<","<<off_photon_type[3]<<")"<<endl;
            // ns<<"(Pass_Ref,Pass) = ("<<pass_ref<<","<<pass<<")"<<endl;

            if(pass_ref){
              if(pass){ //Histograms for photons that pass
                ns<<"Photon # "<<++idx<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<endl;
                for(int photon_type=0;photon_type<4;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;
                  for(int j=0;j<3;j++){
                    double Hist_xval;
                    if(j==0) Hist_xval = m_sumEt;
                    if(j==1) Hist_xval = eg_pt;
                    if(j==2) Hist_xval = eg_eta;

                    if(eg_pt>=20){  //pT>=20 GeV condition. Last Index(5th) should be 1.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_Pass[trig_count][photon_type][j][0][1]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_Pass[trig_count][photon_type][j][1][1]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_Pass[trig_count][photon_type][j][2][1]->Fill(Hist_xval);
                    }
                    if(true){ //All pt. Last Index(5th) should be 0.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_Pass[trig_count][photon_type][j][0][0]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_Pass[trig_count][photon_type][j][1][0]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_Pass[trig_count][photon_type][j][2][0]->Fill(Hist_xval);
                    }
                  }//Fcal,Eta,pT
                }//Photon type loop ends
              } //m_matchtool pass condition ends

              //Histograms showing all photons in the passes event
              for(int photon_type=0;photon_type<4;photon_type++){  
                if(!off_photon_type[photon_type]) continue;
                for(int j=0;j<3;j++){
                  double Hist_xval;
                  if(j==0) Hist_xval = m_sumEt;
                  if(j==1) Hist_xval = eg_pt;
                  if(j==2) Hist_xval = eg_eta;

                  if(eg_pt>=20){  //pT>=20 GeV condition. Last Index(5th) should be 1.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_Total[trig_count][photon_type][j][0][1]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_Total[trig_count][photon_type][j][1][1]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_Total[trig_count][photon_type][j][2][1]->Fill(Hist_xval);
                  }
                  if(true){ //All pt. Last Index(5th) should be 0.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_Total[trig_count][photon_type][j][0][0]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_Total[trig_count][photon_type][j][1][0]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_Total[trig_count][photon_type][j][2][0]->Fill(Hist_xval);
                  }
                }//Filling pass_ref photons
              }//Photon type loop ends for Only reference photons
            }//Pass_ref condtion ends
          }//Offline Photon Loop ends
        }//Offline Photon Size Condition
        else{
          // ns<<"This event has no offline photons."<<endl;
        }
        //Photon Work Ends
        
      }//TrigDecision condition.
      else{
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" fails the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        // ns<<endl;
      }
    }
    
    // ns<<endl<<"nof_L1_triggers: "<<nof_L1_triggers<<" and nof_L1_ref_triggers: "<<nof_L1_ref_triggers;
    
    //Below lies the code to match the MinBias electrons/photons to the offline electrons/photos
    
    for(int L1_ref_trig = 0; L1_ref_trig<nof_L1_ref_triggers;L1_ref_trig++){
      string ref_trigname = m_L1_ref_trigList.at(L1_ref_trig);
      bool ref_trigDecision = trigDecisionTool->isPassed(ref_trigname);

      if(ref_trigDecision){
        ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for   name: "<<ref_trigname;
        ns<<endl;
        for(int L1_trig=0;L1_trig<nof_L1_triggers;L1_trig++){
          string trigname = m_L1_trigList.at(L1_trig);

          //Electrion Work Begins
          bool off_elec_type[4]; //Category of Offline Electron. All,Loose,Medium,Tight
          if(electron_info->size()>0){
            for(const auto eg : *electron_info){  //Loop Over Offline Electrons
              bool pass = false;
              
              if(trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
                for (const auto EMTauRoI: *m_EMTauRoIs){ 
                  pass = L1_eEM12_TrigMatch(eg,EMTauRoI,pass);
                  if(pass) {
                    // ns<<"EM12 Electron Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                    break;       
                  }
                }
              }

              if(trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
                for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                  pass = L1_eEM15_TrigMatch(eg,eEM15,pass);
                  if(pass) {
                    // ns<<"EM15 Electron Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                    break;
                  }
                }
              } 
              double eg_mass = eg->m()*0.001; //GeV
              double eg_pt = eg->pt()*0.001;//GeV
              double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
              double eg_eta = eg->eta();
              double eg_phi = eg->phi();
              
              off_elec_type[0] = true; 
              off_elec_type[1] = bool(m_electronLooseLHSelector ->accept(eg));
              off_elec_type[2] = bool(m_electronMediumLHSelector->accept(eg));
              off_elec_type[3] = bool(m_electronTightLHSelector->accept(eg));

              if(pass){
                ns<<"Electron matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<". Matches Trigger: "<<trigname<<" and MinBias Reference Trigger: "<<ref_trigname<<endl;

                for(int electron_type=0;electron_type<4;electron_type++){  
                  if(!off_elec_type[electron_type]) continue;
                  for(int j=0;j<3;j++){
                    double Hist_xval;
                    if(j==0) Hist_xval = m_sumEt;
                    if(j==1) Hist_xval = eg_pt;
                    if(j==2) Hist_xval = eg_eta;

                    if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][0][1]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][1][1]->Fill(Hist_xval);
                      } 
                      
                      Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][2][1]->Fill(Hist_xval);
                    }  
                    if(true){ //All pt. Last Index(5th) should be 0.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][0][0]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][1][0]->Fill(Hist_xval);
                      } 
                      
                      Electron_Hists_L1_Pass[L1_trig][L1_ref_trig][electron_type][j][2][0]->Fill(Hist_xval);
                    }
                  }//Fcal Et, pt, eta
                }//electron type loop ends
              }//pass condition ends

              //Histograms showing all electrons that passes the MinBias reference trigger
              for(int electron_type=0;electron_type<4;electron_type++){  
                if(!off_elec_type[electron_type]) continue;

                for(int j=0;j<3;j++){
                  double Hist_xval;
                  if(j==0) Hist_xval = m_sumEt;
                  if(j==1) Hist_xval = eg_pt;
                  if(j==2) Hist_xval = eg_eta;

                  if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][0][1]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][1][1]->Fill(Hist_xval);
                    } 
                    
                    Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][2][1]->Fill(Hist_xval);
                  }
                  if(true){ //All pt. Last Index(5th) should be 0.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][0][0]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][1][0]->Fill(Hist_xval);
                    } 
                    
                    Electron_Hists_L1_Total[L1_trig][L1_ref_trig][electron_type][j][2][0]->Fill(Hist_xval);
                  }
                } //Reference filling ends
              }//electron type loop ends for reference filling

            }//Offline Electron Loop Ends
          }//Elecron Size>0 condition Ends
          else{
          // ns<<"No electrons in this event. "<<endl;
          }
          //Electron Work Ends

          //Photon Work Begins
          bool off_photon_type[4]; //Category of Offline Photons. All,Loose,Medium,Tight
          if(photon_info->size()>0){
            for(const auto eg : *photon_info){  //Loop Over Offline Photons
              bool pass = false;
              
              if(trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
                for (const auto EMTauRoI: *m_EMTauRoIs){ 
                  pass = L1_eEM12_TrigMatch(eg,EMTauRoI,pass);
                  if(pass) {
                    // ns<<"EM12 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                    break;       
                  }
                }
              }

              if(trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
                for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                  pass = L1_eEM15_TrigMatch(eg,eEM15,pass);
                  if(pass) {
                    // ns<<"EM15 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                    break;
                  }
                }
              } 
              double eg_mass = eg->m()*0.001; //GeV
              double eg_pt = eg->pt()*0.001;//GeV
              double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
              double eg_eta = eg->eta();
              double eg_phi = eg->phi();
              
              off_photon_type[0] = true;
              off_photon_type[1] = bool(m_photonLooseIsEMSelector->accept(eg));
              off_photon_type[2] = bool(m_photonMediumIsEMSelector->accept(eg));
              off_photon_type[3] = bool(m_photonTightIsEMSelector->accept(eg));

              if(pass){
                ns<<"Photon matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<". Matches Trigger: "<<trigname<<" and MinBias Reference Trigger: "<<ref_trigname<<endl;

                for(int photon_type=0;photon_type<4;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;
                  for(int j=0;j<3;j++){
                    double Hist_xval;
                    if(j==0) Hist_xval = m_sumEt;
                    if(j==1) Hist_xval = eg_pt;
                    if(j==2) Hist_xval = eg_eta;

                    if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][0][1]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][1][1]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][2][1]->Fill(Hist_xval);
                    }  
                    if(true){ //All pt. Last Index(5th) should be 0.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][0][0]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][1][0]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_L1_Pass[L1_trig][L1_ref_trig][photon_type][j][2][0]->Fill(Hist_xval);
                    }
                  }//Fcal Et, pt, eta
                }//electron type loop ends
              }//pass condition ends

              //Histograms showing all photons that passes the MinBias reference trigger
              for(int photon_type=0;photon_type<4;photon_type++){  
                if(!off_photon_type[photon_type]) continue;

                for(int j=0;j<3;j++){
                  double Hist_xval;
                  if(j==0) Hist_xval = m_sumEt;
                  if(j==1) Hist_xval = eg_pt;
                  if(j==2) Hist_xval = eg_eta;

                  if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][0][1]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][1][1]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][2][1]->Fill(Hist_xval);
                  }
                  if(true){ //All pt. Last Index(5th) should be 0.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][0][0]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][1][0]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_L1_Total[L1_trig][L1_ref_trig][photon_type][j][2][0]->Fill(Hist_xval);
                  }
                } //Reference filling ends
              }//photon type loop ends for reference filling
                
            }// Offline Photon Loop Ends 
          }//Photon Size>0 condition ends
          else{
            // ns<<"No photons in this event. "<<endl;
          }

          //Photon Work Ends
        }//L1 trig loop ends
      }//TrigDecision condition ends
      else{
          // ns<<"Event(Electron+Photon) Fails the MinBias trigger: "<<ref_trigname<<endl;
        }
    }//L1_ref trig loop ends
    
    
    /*
    //Below lies the code to compute efficiency of id phtons wrt MinBias
    for(int L1_ref_trig = 0; L1_ref_trig<nof_L1_ref_triggers;L1_ref_trig++){
      string ref_trigname = m_L1_ref_trigList.at(L1_ref_trig);
      bool ref_trigDecision = trigDecisionTool->isPassed(ref_trigname);

      if(ref_trigDecision){
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for   name: "<<ref_trigname;
        ns<<endl;
        for(int HLT_trig=0;HLT_trig<nof_HLT_triggers_Photon;HLT_trig++){
          string trigname = m_trigList_Photon.at(HLT_trig);
          bool off_photon_type[4]; //Category of Offline Electron. All,Loose,Tight

          if(photon_info->size()>0){
            for(const auto eg : *photon_info){  //Loop Over Offline Photons
              bool pass = m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one;
              
              double eg_mass = eg->m()*0.001; //GeV
              double eg_pt = eg->pt()*0.001;//GeV
              double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
              double eg_eta = eg->eta();
              double eg_phi = eg->phi();
              
              off_photon_type[0] = true;
              off_photon_type[1] = bool(m_photonLooseIsEMSelector->accept(eg));
              off_photon_type[2] = bool(m_photonMediumIsEMSelector->accept(eg));
              off_photon_type[3] = bool(m_photonTightIsEMSelector->accept(eg));

              if(pass){
                ns<<"Photon matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<". Matches HLT Trigger: "<<trigname<<" and MinBias Reference Trigger: "<<ref_trigname<<endl;

                for(int photon_type=0;photon_type<4;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;
                  for(int j=0;j<3;j++){
                    double Hist_xval;
                    if(j==0) Hist_xval = m_sumEt;
                    if(j==1) Hist_xval = eg_pt;
                    if(j==2) Hist_xval = eg_eta;

                    if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(5th) should be 1.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][0][1]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][1][1]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][2][1]->Fill(Hist_xval);
                    }  
                    if(true){ //All pt. Last Index(5th) should be 0.
                      if(fabs(eg_eta)<1.37){//Barrel
                        Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][0][0]->Fill(Hist_xval);
                      } 
                    
                      if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                        Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][1][0]->Fill(Hist_xval);
                      } 
                      
                      Photon_Hists_MB_Pass[HLT_trig][L1_ref_trig][photon_type][j][2][0]->Fill(Hist_xval);
                    }
                  }//Fcal Et, pt, eta
                }//photon type loop ends
              }//pass condition ends

              //Histograms showing all electrons that passes the MinBias reference trigger
              for(int photon_type=0;photon_type<4;photon_type++){  
                if(!off_photon_type[photon_type]) continue;

                for(int j=0;j<3;j++){
                  double Hist_xval;
                  if(j==0) Hist_xval = m_sumEt;
                  if(j==1) Hist_xval = eg_pt;
                  if(j==2) Hist_xval = eg_eta;

                  if(eg_pt>=20){ //pT>=20 GeV condition. Last Index(6th) should be 1.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][0][1]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][1][1]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][2][1]->Fill(Hist_xval);
                  }
                  if(true){ //All pt. Last Index(6th) should be 0.
                    if(fabs(eg_eta)<1.37){//Barrel
                      Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][0][0]->Fill(Hist_xval);
                    } 
                  
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                      Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][1][0]->Fill(Hist_xval);
                    } 
                    
                    Photon_Hists_MB_Total[HLT_trig][L1_ref_trig][photon_type][j][2][0]->Fill(Hist_xval);
                  }
                } //Reference filling ends
              }//photon type loop ends for reference filling

            }
          }
          else{
          // ns<<"No photons in this event. "<<endl;
          }
        }
      }//HLT trig loop ends
      else{
          // ns<<"Event(Photon) Fails the MinBias trigger: "<<ref_trigname<<endl;
        }
    }//L1_ref trig loop ends
    */

    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";

    return StatusCode::SUCCESS;
  }


  void MyxAODAnalysis::Save_Hists(std::string outfilename){
    std::string Ghost_name;
    char name[500];

    TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");

    //Saving the Efficiency related histograms
    //For Electrons
    for(int trig_count=0;trig_count<nof_triggers;trig_count++){
      for(int i=0;i<4;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_trigList.at(trig_count);
            if(i==0) Ghost_name += "_All_Off";
            if(i==1) Ghost_name += "_Loose_LH_Off";
            if(i==2) Ghost_name += "_Medium_LH_Off";
            if(i==3) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "_SumFcalEt";
            if(j==1) Ghost_name += "_Pt";
            if(j==2) Ghost_name += "_Eta";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            for(int l=0;l<2;l++){
              if(l==0) Ghost_name +="_pt_All";
              if(l==1) Ghost_name +="_pt_geq_20";

              sprintf(name,"Electron_Evt_%s",Ghost_name.c_str());
              Outfile->WriteObject(Electron_Hists_Pass[trig_count][i][j][k][l],name);

              sprintf(name,"Electron_Total_%s",Ghost_name.c_str());
              Outfile->WriteObject(Electron_Hists_Total[trig_count][i][j][k][l],name);
            }
          }
        }
      }
    }

    //For Photons
    for(int trig_count=0;trig_count<nof_triggers_Photons;trig_count++){
      for(int i=0;i<4;i++){
        for(int j=0;j<3;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_trigList_Photons.at(trig_count);
            if(i==0) Ghost_name += "_All_Off";
            if(i==1) Ghost_name += "_Loose_LH_Off";
            if(i==2) Ghost_name += "_Medium_LH_Off";
            if(i==3) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "_SumFcalEt";
            if(j==1) Ghost_name += "_Pt";
            if(j==2) Ghost_name += "_Eta";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            for(int l=0;l<2;l++){
              if(l==0) Ghost_name +="_pt_All";
              if(l==1) Ghost_name +="_pt_geq_20";

              sprintf(name,"Photon_Evt_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Hists_Pass[trig_count][i][j][k][l],name);

              sprintf(name,"Photon_Total_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Hists_Total[trig_count][i][j][k][l],name);
            }
          }
        }
      }
    }
    
    //Efficiency L1 wrt Min Bias
    for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<4;i++){
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_L1_trigList.at(trig_count)+"_"+m_L1_ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_All_Off";
              if(i==1) Ghost_name += "_Loose_LH_Off";
              if(i==2) Ghost_name += "_Medium_LH_Off";
              if(i==3) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "_SumFcalEt";
              if(j==1) Ghost_name += "_Pt";
              if(j==2) Ghost_name += "_Eta";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              for(int l=0;l<2;l++){
                if(l==0) Ghost_name +="_pt_All";
                if(l==1) Ghost_name +="_pt_geq_20";

                
                sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
                Outfile->WriteObject(Photon_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l],name);

                sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                Outfile->WriteObject(Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l],name);

                
                sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
                Outfile->WriteObject(Photon_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l],name);

                sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                Outfile->WriteObject(Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l],name);

              }
            }
          }
        }
      }
    }
    
    /*
    //Efficiency HLT Photons wrt Min Bias
    for(int trig_count=0;trig_count<nof_HLT_triggers_Photon;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<4;i++){
          for(int j=0;j<3;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_trigList_Photon.at(trig_count)+"_"+m_L1_ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_All_Off";
              if(i==1) Ghost_name += "_Loose_LH_Off";
              if(i==2) Ghost_name += "_Medium_LH_Off";
              if(i==3) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "_SumFcalEt";
              if(j==1) Ghost_name += "_Pt";
              if(j==2) Ghost_name += "_Eta";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              for(int l=0;l<2;l++){
                if(l==0) Ghost_name +="_pt_All";
                if(l==1) Ghost_name +="_pt_geq_20";

                
                sprintf(name,"Photon_HLT_MB_Evt_%s",Ghost_name.c_str());
                Outfile->WriteObject(Photon_Hists_MB_Pass[trig_count][ref_trig_count][i][j][k][l],name);

                // sprintf(name,"Electron_L1_Evt_%s",Ghost_name.c_str());
                // Outfile->WriteObject(Electron_Hists_L1_Pass[trig_count][ref_trig_count][i][j][k][l],name);

                
                sprintf(name,"Photon_HLT_MB_Total_%s",Ghost_name.c_str());
                Outfile->WriteObject(Photon_Hists_MB_Total[trig_count][ref_trig_count][i][j][k][l],name);

                // sprintf(name,"Electron_L1_Total_%s",Ghost_name.c_str());
                // Outfile->WriteObject(Electron_Hists_L1_Total[trig_count][ref_trig_count][i][j][k][l],name);

              }
            }
          }
        }
      }
    }
    */
    
    Outfile->Close();
    ANA_MSG_INFO("All Histograms have been saved.");
  }


  StatusCode MyxAODAnalysis :: finalize ()
  {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.

    ANA_MSG_INFO ("In Finalize.");

    // std::string outfilename = "/afs/cern.ch/user/a/adimri/Egamma_Trigger/Output_New.root";
    std::string outfilename = "Output_New.root";
    Save_Hists(outfilename);

    ns<<endl<<"....................End....................."<<endl;
    ns.close();
    return StatusCode::SUCCESS;
  }
