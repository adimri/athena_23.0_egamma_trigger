#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <AsgTools/ToolHandle.h> 

//Taken from Qipeng
#include "TriggerMatchingTool/R3MatchingTool.h"
// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> // GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 


  class MyxAODAnalysis : public EL::AnaAlgorithm
  {
    private:

    double m_electronPtCut; // Electron pT cut
    std::string m_sampleName; // Sample name
    double m_JetPtCut; //Jet pT Cut
  

    public:
    unsigned int trigDecision = 0; //< Trigger decision

    ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool;
    ToolHandle<Trig::IMatchingTool> m_matchtool;
    
    //Do by hand
    double dR(double eta1,double phi1, double eta2, double phi2); //dR calculation by hand
    bool L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match=false); //By hand execution of L1_EM15 matching
    bool L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match=false);  //By hand execution of L1_EM15 matching
    bool L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching
    bool L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching

    std::vector<std::string> m_L1_trigList = {"L1_EM12","L1_eEM15"};
    int nof_L1_triggers = int(m_L1_trigList.size()*1.0); //L1 Trigger Size

    std::vector<std::string> m_trigList_Photon = {"HLT_g15_loose_ion_L1EM12","HLT_g15_loose_ion_L1eEM15"};
    int nof_HLT_triggers_Photon = int(m_trigList_Photon.size()*1.0); //L1 Trigger Size for Photons

    std::vector<std::string> m_L1_ref_trigList = {
    "HLT_mb_sptrk_pc_L1ZDC_A_C_VTE50",
    "HLT_noalg_L1TE600p0ETA49",
    "HLT_noalg_L1TE50_VTE600p0ETA49"};
    //3 reference for each if the L1 triggers

    int nof_L1_ref_triggers = int(m_L1_ref_trigList.size()*1.0); //L1 Reference Trigger Size

    //Define histograms
    TH1D *Photon_Hists_MB_Pass[2][3][4][3][3][2]; //Efficiency measured wrt to MinBias
    TH1D *Photon_Hists_MB_Total[2][3][4][3][3][2];  //Efficiency measured wrt to MinBias
    TH1D *Photon_Hists_L1_Pass[2][3][4][3][3][2];
    TH1D *Photon_Hists_L1_Total[2][3][4][3][3][2];
    TH1D *Electron_Hists_L1_Pass[2][3][4][3][3][2];
    TH1D *Electron_Hists_L1_Total[2][3][4][3][3][2];
    //First [] is for the L1 triggers
    //Second [] is for the MinBias Triggers wrt to which we need to compute efficiency
    //Third [] is for All,loose,medium and tight electrons
    //Fourth [] is for Sum Fcal Et, pt, eta as xaxis
    //Fifth [] is fot Eta Coverage: Barrel, Eta, All
    //Sixth [] is for pT cut: All pt, pt>=20GeV

    std::vector<std::string> m_trigList= {
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_lhloose_nogsf_ion_L1EM12",
    "HLT_e15_loose_nogsf_ion_L1EM12",
    "HLT_e15_lhmedium_nogsf_ion_L1EM12",
    "HLT_e15_medium_nogsf_ion_L1EM12",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_lhloose_nogsf_ion_L1eEM15",
    "HLT_e15_lhmedium_nogsf_ion_L1eEM15",
    "HLT_e15_loose_nogsf_ion_L1eEM15",
    "HLT_e15_medium_nogsf_ion_L1eEM15"
    }; //Trigger Names
    int nof_triggers = int(m_trigList.size()*1.0); //Trigger Size
    
    //Reference trigger list
    std::vector<std::string> m_ref_triglist = {
    "L1_EM12", 
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "L1_eEM15",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15"
    }; //Corresponding Reference triggers to compute efficiency for the relevant triggers

    void limit_nof_triggers() {
      if(nof_triggers > 14) {
          nof_triggers = 14;
      }
    }
    
    //Triglist Photons
    std::vector<std::string> m_trigList_Photons= {
      "HLT_g15_loose_ion_L1EM12",
      "HLT_g15_loose_ion_L1eEM15"
    };

    //Reference trigger list
    std::vector<std::string> m_ref_triglist_Photons = {
      "L1_EM12", 
      "L1_eEM15"
    };
    int nof_triggers_Photons = int(m_trigList_Photons.size()*1.0); //Trigger Size
    
    void limit_nof_triggers_Photons() {
      if(nof_triggers_Photons > 4) {
          nof_triggers_Photons = 4;
      }
    }

    //Likelihood Tool Selectors
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //grl tool
    AsgElectronLikelihoodTool *m_electronLooseLHSelector; 
    AsgElectronLikelihoodTool *m_electronMediumLHSelector; 
    AsgElectronLikelihoodTool *m_electronTightLHSelector; 
    AsgPhotonIsEMSelector *m_photonTightIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonLooseIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonMediumIsEMSelector; 


    //Define histograms
    TH1D *Photon_Hists_Pass[4][4][3][3][2];
    TH1D *Photon_Hists_Total[4][4][3][3][2];
    TH1D *Electron_Hists_Pass[14][4][3][3][2];
    TH1D *Electron_Hists_Total[14][4][3][3][2];
    //First []: Maximum Trigger Numbers
    //Second []: 0->All offline electrons, 1->Loose LH Offline, 2->Medium, 3->Tight LH Offline
    //Third []:0->Sum Fcal Et, 1-> Particle Pt, 2->Particle Eta
    //Fourth []:0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta
    //Fifth []:0->All p_t, 1->p_t>20

    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    void Init_Hists();
    void Save_Hists(std::string);
    void SetHists(TH1*,const std::string &,const std::string &);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

    //File Handle
    std::ofstream ns;
  };


#endif