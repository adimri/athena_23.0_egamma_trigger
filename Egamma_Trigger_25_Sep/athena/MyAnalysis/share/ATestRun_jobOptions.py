#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
# testFile = ['/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000001.pool.root.1',
# '/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000036.pool.root.1','/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000054.pool.root.1',
# '/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000126.pool.root.1','/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000129.pool.root.1',
# '/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000001.pool.root.1']

# testFile = ['/eos/user/a/adimri/New_Data/data18_hi/AOD.34759660._000169.pool.root.1'] #Old Reprocessing
# testFile = ['/eos/user/a/adimri/New_Data/Good_ReProcessing_Sep_25/data18_hi/AOD.34942804._001594.pool.root.1']    #Good Reprocessing

# testFile = ['/eos/user/a/adimri/pp_MinBiasData_Run3/data23_5p36TeV/data23_5p36TeV.00461096.physics_MinBias.merge.AOD.f1389_m2203._lb0997._0003.1'] #pp data
testFile = ['/eos/user/a/adimri/PbPb_ExpressStream_Data/data23_hi/data23_hi.00461633.express_express.merge.AOD.x779_m2203._lb0179._0010.1'] #PbPb Run3 data

# testFile = ['/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000001.pool.root.1']

# testFile = ['/eos/user/a/adimri/MC_Data/mc23_13p6TeV/AOD.33839837._010684.pool.root.1']
 
print ("Testfile: ",testFile)

#override next line on command line with: --filesInput=XXX
# jps.AthenaCommonFlags.FilesInput = [testFile[0],testFile[1]]
athenaCommonFlags.FilesInput.set_Value_and_Lock(testFile)


#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration. 
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import addPrivateTool, createAlgorithm, createPublicTool
from AthenaCommon.AppMgr import ToolSvc

alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )   #alg is the object of the algorithm

#Making the Matching Tool
tdt = createPublicTool("Trig::TrigDecisionTool", "TrigDecisionTool")
addPrivateTool(tdt, "ConfigTool", "TrigConf::xAODConfigTool")

#Next Line of Code is specifically for Run3
tdt.NavigationFormat = "TrigComposite"


# tdt.HLTSummary = "HLTNav_Summary_DAODSlimmed"
# tdt.HLTSummary = "HLTNav_Summary_OnlineSlimmed"
tdt.HLTSummary = "HLTNav_Summary_AODSlimmed"
addPrivateTool(alg, "MatchingTool", "Trig::R3MatchingTool")
alg.trigDecisionTool = tdt
alg.MatchingTool.TrigDecisionTool = tdt

#Trying to get Debug messages
# import logging
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)

#Write the properties
# alg.ElectronPtCut = 30000.0
# alg.SampleName = 'Zee'
# alg.JetPtCut = 1000.5

#Output level
#Default is INFO. Can change to VERBOSE/DEBUG/INFO/WARNING/ERROR/FATAL
# alg.OutputLevel=DEBUG

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
# theApp.EvtMax = 10

#For Histograms
# jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:Test1.root"]
# svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

