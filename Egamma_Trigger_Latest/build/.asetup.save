#Release cmake

export LANG="C"
export LC_ALL="C"
export COOL_ORA_ENABLE_ADAPTIVE_OPT="Y"
export ASETUP_PRINTLEVEL="0"
export BINARY_TAG="x86_64-centos7-gcc11-opt"
export CMTCONFIG="x86_64-centos7-gcc11-opt"

if [ -d /tmp/adimri ]; then
   export ASETUP_SYSBIN=`mktemp -d /tmp/adimri/.asetup-sysbin-XXXXXX_$$`
else
   export ASETUP_SYSBIN=`mktemp -d /afs/cern.ch/user/a/adimri/Egamma_Trigger/build/.asetup-sysbin-XXXXXX_$$`
fi
source $AtlasSetup/scripts/sys_exe-alias.sh ''
if [ -n "${MAKEFLAGS:+x}" ]; then
    asetup_flags=`echo ${MAKEFLAGS} | \grep ' -l'`
    if [ -z "${asetup_flags}" ]; then
        export MAKEFLAGS="${MAKEFLAGS} -l10"
    fi
else
    export MAKEFLAGS="-j10 -l10"
fi
source /cvmfs/sft.cern.ch/lcg/releases/gcc/11.2.0-8a51a/x86_64-centos7/setup.sh
export CC=`\env which gcc 2>/dev/null`
[[ -z $CC ]] && unset CC
export CXX=`\env which g++ 2>/dev/null`
[[ -z $CXX ]] && unset CXX
export CUDAHOSTCXX=`\env which g++ 2>/dev/null`
[[ -z $CUDAHOSTCXX ]] && unset CUDAHOSTCXX
export FC=`\env which gfortran 2>/dev/null`
[[ -z $FC ]] && unset FC
export CMAKE_NO_VERBOSE="1"
type lsetup >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
   source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
fi
source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh --quiet "cmake 3.24.3"
if [ -z "${AtlasSetup:+x}" ]; then
    export AtlasSetup="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/V02-02-04/AtlasSetup"
    export AtlasSetupVersion="AtlasSetup-02-02-04"
fi
export FRONTIER_SERVER="(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://ca-proxy-atlas.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"
export ATLAS_POOLCOND_PATH="/cvmfs/atlas-condb.cern.ch/repo/conditions"
export ATLAS_DB_AREA="/cvmfs/atlas.cern.ch/repo/sw/database"
export DBRELEASE_OVERRIDE="current"
export AtlasVersion="23.0.43"
export AtlasProject="Athena"
export AtlasBaseDir="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101"
export ATLAS_RELEASE_BASE="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101"
export AtlasBuildBranch="23.0"
export AtlasArea="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101/Athena/23.0.43"
export AtlasReleaseType="nightly"
export AtlasBuildStamp="2023-08-27T2101"
export SITEROOT="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt"
export ATLAS_RELEASEDATA="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/atlas/offline/ReleaseData"
export LCG_RELEASE_BASE="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/sw/lcg/releases"
export G4PATH="/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/Geant4"
export TDAQ_RELEASE_BASE="/cvmfs/atlas.cern.ch/repo/sw/tdaq"
source /cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101/Athena/23.0.43/InstallArea/x86_64-centos7-gcc11-opt/setup.sh
asetup_status=$?
if [ ${asetup_status} -ne 0 ]; then
    \echo "AtlasSetup(ERROR): sourcing release setup script (/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101/Athena/23.0.43/InstallArea/x86_64-centos7-gcc11-opt/setup.sh) failed"
fi
export TestArea="/afs/cern.ch/user/a/adimri/Egamma_Trigger/build"
alias_sys_exe emacs
echo $LD_LIBRARY_PATH | egrep "LCG_[^/:]*/curl/" >/dev/null
if [ $? -eq 0 ]; then
    alias_sys_exe_envU git
fi
\expr 1 \* 1 + 1 >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo -e '\nMaking workaround-alias for expr on this *OLD* machine'; alias_sys_exe expr
fi
export PATH="${ASETUP_SYSBIN}:${PATH}"

# Check the completeness in the nightly release
for onepath in $(echo $LD_LIBRARY_PATH | tr ":" "\n"); do
   if [[ $onepath == ${AtlasBaseDir}* && ! -d $onepath ]]; then
      echo \!\! FATAL \!\! $onepath does not exist
      return 1
   fi
done

#Release Summary as follows:
#Release base=/cvmfs/atlas-nightlies.cern.ch/repo/sw/23.0_Athena_x86_64-centos7-gcc11-opt/2023-08-27T2101
#Release project=Athena
#Release releaseNum=23.0.43
#Release asconfig=x86_64-centos7-gcc11-opt

# Execute user-specified epilog

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/swConfig/asetup/asetupEpilog.sh
script_status=$?
if [ ${script_status} -ne 0 ]; then
    \echo "AtlasSetup(ERROR): User-specified epilog (source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/swConfig/asetup/asetupEpilog.sh) reported failure (error ${script_status})"
fi
