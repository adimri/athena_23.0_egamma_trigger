
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/afs/cern.ch/user/a/adimri/Egamma_Trigger/athena/MyAnalysis/src/components/MyAnalysis_entries.cxx" "MyAnalysis/CMakeFiles/MyAnalysis.dir/src/components/MyAnalysis_entries.cxx.o" "gcc" "MyAnalysis/CMakeFiles/MyAnalysis.dir/src/components/MyAnalysis_entries.cxx.o.d"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/afs/cern.ch/user/a/adimri/Egamma_Trigger/build/MyAnalysis/CMakeFiles/MyAnalysisLib.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
