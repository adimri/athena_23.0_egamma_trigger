#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>

//Including Header files to run from R3MatchingTool
#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

//The following 3 files donot exist
// #include <xAODTrigger/TrigMatch.h>
// #include <xAODTrigger/TrigMatchContainer.h>
// #include <xAODTrigger/TrigMatchAuxContainer.h>

// #include <xAODJet/JetContainer.h>
// #include <xAODTrigger/TrigDecision.h>
// #include <xAODTrigger/TrigDecisionTool.h>
// #include "StoreGate/ReadDecorHandle.h"
// #include <xAODTrigger/TrigInfo.h>

using namespace std;

  MyxAODAnalysis :: MyxAODAnalysis (const std::string& name,
                                    ISvcLocator *pSvcLocator)
      : EL::AnaAlgorithm (name, pSvcLocator)
      // ,m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool")
      ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
      // ,m_matchTool("Trig::TrigEgammaMatchingTool/TrigEgammaMatchingTool",this)
      ,m_matchtool(this,"Trig::MatchingTool")
      // ,m_tmt("Trig::MatchingTool") //TestMatchingToolAlg.cxx Method
      
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    ANA_MSG_INFO ("Inside  Constructor.");

    declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
                    "Minimum electron pT (in MeV)" );
    declareProperty( "SampleName", m_sampleName = "Unknown",
                    "Descriptive name for the processed sample" );
    declareProperty( "JetPtCut", m_JetPtCut = 20.0,
                    "Minimum Jet pT (in MeV)" );
    
    //For Trigger Tools
    // declareProperty( "TriggerTool", m_trigDecTool);
    declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
    declareProperty("triggers", m_trigList, "trigger selection list");

    //Matching
    declareProperty("MatchingTool", m_matchtool);
    // declareProperty("TrigEgammaMatchingTool",m_matchTool,"This is the match tool already defined in ATLAS framework");
    // declareProperty("TrigMatchingTool",m_matchtool,"This is the match tool already defined in ATLAS framework");
    // declareProperty("MatchingTool",m_tmt);
    

  }

  void MyxAODAnalysis::SetHists(TH1* My_hist,
      const std::string &xaxistitle="None",const std::string &yaxistitle="None"){

        //Here I just set some basic properties of the histogram that I will define.

        My_hist->GetXaxis()->SetTitle(xaxistitle.c_str());
        My_hist->GetYaxis()->SetTitle(yaxistitle.c_str());
      }

  StatusCode MyxAODAnalysis :: initialize ()
  {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_INFO ("In Initialize.");

    // ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 500, 0, 500))); // jet pt [GeV]
    // ANA_CHECK (book (TH1F ("h_electronPt", "h_electronPt", 500, 0, 50))); // jet pt [GeV]
    // ANA_CHECK (book (TH1F ("h_photonPt", "h_photonPt", 1000, 0, 50))); // jet pt [GeV]

    // SetHists(hist("h_electronPt"),strdup("GeV"),strdup("Counts"));
    // SetHists(hist("h_photonPt"),strdup("GeV"),strdup("Counts"));
    // SetHists(hist("h_jetPt"),strdup("GeV"),strdup("Counts"));
    
    //Define the histograms
    Init_Hists();

    //Set the trigger list
    // m_trigList = {"HLT_e15_etcut_ion_L1eEM15"};
    // m_trigList = {"HLT_e13_etcut_nogsf_ion_L1EM10"};
    // m_trigList = {"HLT_e13_etcut_ion"};
    // m_trigList = {"HLT_e26_lhtight_ivarloose_L1EM22VHI"};
    m_trigList = {"HLT_e15_etcut_ion_L1eEM15"};

    //Check if the decision tool is working properly
    // ANA_CHECK(m_scoreTool.retrieve())
    // ANA_CHECK(m_trigDecTool.retrieve());
    ANA_CHECK(trigDecisionTool.retrieve());
    ANA_CHECK(m_matchtool.retrieve());
    // m_tmt.setTypeAndName("Trig::MatchingTool/MyMatchingTool");
    // CHECK(m_tmt.retrieve()); //important to retrieve here, because TrigDecisionTool must be initialized before event loop

    ns.open("/afs/cern.ch/user/a/adimri/Egamma_Trigger/CheckFile_New.txt");
    ns<<".....................Begin....................."<<endl;

    return StatusCode::SUCCESS;
  }

  
  void MyxAODAnalysis::Init_Hists(){
    std::string Ghost_name;
    double pi = TMath::Pi();
    char name[500];
    
    for(int i=0;i<2;i++){
      for(int j=0;j<4;j++){
        for(int k=0;k<3;k++){
          if(i==0) Ghost_name = "EvtPass";
          if(i==1) Ghost_name = "Total_EvtPass";

          if(j==0) Ghost_name += "_SumFcalEt";
          if(j==1) Ghost_name += "_Pt";
          if(j==2) Ghost_name += "_Eta";
          if(j==3) Ghost_name += "_Phi";

          if(k==0) Ghost_name +="Eta_Barrel";
          if(k==1) Ghost_name +="Eta_EndCap";
          if(k==2) Ghost_name +="Eta_All";

          if(j==0){
            sprintf(name,"Photon_%s",Ghost_name.c_str());
            Photon_Hists[i][j][k] = new TH1D(name,name,7000,-0.5,6999.5);
            sprintf(name,"Electron_%s",Ghost_name.c_str());
            Electron_Hists[i][j][k] = new TH1D(name,name,7000,-0.5,6999.5);
          }

          if(j==1){
            sprintf(name,"Photon_%s",Ghost_name.c_str());
            Photon_Hists[i][j][k] = new TH1D(name,name,500,-0.5,499.5);
            sprintf(name,"Electron_%s",Ghost_name.c_str());
            Electron_Hists[i][j][k] = new TH1D(name,name,500,-0.5,499.5);
          }
          
          if(j==2){
            sprintf(name,"Photon_%s",Ghost_name.c_str());
            Photon_Hists[i][j][k] = new TH1D(name,name,1200,-6,6);
            sprintf(name,"Electron_%s",Ghost_name.c_str());
            Electron_Hists[i][j][k] = new TH1D(name,name,1200,-6,6);
          }

          if(j==3){
            sprintf(name,"Photon_%s",Ghost_name.c_str());
            Photon_Hists[i][j][k] = new TH1D(name,name,628,-2*pi,2*pi);
            sprintf(name,"Electron_%s",Ghost_name.c_str());
            Electron_Hists[i][j][k] = new TH1D(name,name,628,-2*pi,2*pi);
          }

        }
      }
    }
    

    

    ANA_MSG_INFO("Initialization Of Histograms completed.");
  }
  
  //**********************************************************************

  // void MyxAODAnalysis::matchEgamma(const std::string& trigger,const xAOD::Egamma *eg){

  //   if (eg) {
  //     if(m_matchTool->match(eg,trigger)){
  //       ANA_MSG_DEBUG("REGTEST:: Method 1 Matched Electron with tool for " << trigger);
  //       m_counterMatch1Bits[trigger]++;
  //     }
  //     else ANA_MSG_DEBUG("REGTEST::Fails method 1 " << trigger);
  // #ifdef XAOD_ANALYSIS
  //     const HLT::TriggerElement *finalFC; 
  //     if(m_matchTool->match(eg,trigger,finalFC)){
  //       ANA_MSG_DEBUG("REGTEST:: Method 2 Matched Electron with tool for " << trigger);
  //       if ( finalFC != NULL ){ 
  //         if ( (m_trigdec->ancestor<xAOD::ElectronContainer>(finalFC)).te() != NULL ){ 
  //           if( (m_trigdec->ancestor<xAOD::ElectronContainer>(finalFC)).te()->getActiveState()){
  //             ANA_MSG_DEBUG("REGTEST::Passed Matching method 2 for " << trigger);
  //             m_counterMatch2Bits[trigger]++;
  //           }
  //           else ANA_MSG_DEBUG("REGTEST::Fails method 2");
  //         }
  //       }
  //     }
  // #endif            
  //     if(m_matchTool->matchHLT(eg,trigger)){
  //       ANA_MSG_DEBUG("REGTEST:: Method 3 Matched Electron with tool for " << trigger);
  //       m_counterMatch3Bits[trigger]++;
  //     }
  //     else ANA_MSG_DEBUG("REGTEST::Fails method 3");
  //     // std::vector<const xAOD::IParticle *> reco;
  //     // reco.push_back(eg);
  //     // if(m_matchTool2->match(reco,trigger)){
  //     //     ANA_MSG_DEBUG("REGTEST::  Generic Matched Electron with tool for " << trigger);
  //     //     m_counterMatch4Bits[trigger]++;
  //     // }
  //     // else ANA_MSG_DEBUG("REGTEST::Fails generic tool");
  //   }
  //   else ANA_MSG_DEBUG("REGTEST: eg pointer null!");
  // }
  
  //**********************************************************************

  
  //**************************************************************************
  /*
  //From R3MatchingTool.cxx
  bool MyxAODAnalysis::matchObjects(
      const xAOD::IParticle *reco,
      const ElementLink<xAOD::IParticleContainer> &onlineLink,
      xAODType::ObjectType onlineType,
      std::map<std::pair<uint32_t, uint32_t>, bool> &cache,
      double scoreThreshold) const
  {
    if (!onlineLink.isValid())
    {
      ATH_MSG_WARNING("Invalid element link!");
      return false;
    }
    std::pair<uint32_t, uint32_t> linkIndices(onlineLink.persKey(), onlineLink.persIndex());
    auto cacheItr = cache.find(linkIndices);
    if (cacheItr == cache.end())
    {
      const xAOD::IParticle *online = *onlineLink;
      ATH_MSG_DEBUG("Match online " << onlineType << " to offline " << reco->type());
      bool match = onlineType == reco->type();
      if (onlineType == xAOD::Type::CaloCluster && (reco->type() == xAOD::Type::Electron || reco->type() == xAOD::Type::Photon))
      {
        // Calo cluster is a special case - some of the egamma chains can return these (the etcut chains)
        // In these cases we need to match this against the caloCluster object contained in electrons or photons
        const xAOD::Egamma *egamma = dynamic_cast<const xAOD::Egamma *>(reco);
        if (!egamma)
          // this should never happen
          throw std::runtime_error("Failed to cast to egamma object");
        const xAOD::IParticle *cluster = egamma->caloCluster();
        if (cluster)
          reco = cluster;
        else
          ATH_MSG_WARNING("Cannot retrieve egamma object's primary calorimeter cluster, will match to the egamma object");
        match = true;
      }
      if (match)
        match = m_scoreTool->score(*online, *reco) < scoreThreshold;
      cacheItr = cache.insert(std::make_pair(linkIndices, match)).first;
    }
    return cacheItr->second;
  }


  const MyxAODAnalysis::chainInfo_t &MyxAODAnalysis::getChainInfo(const std::string &chain) const
  {
    std::lock_guard<std::mutex> guard(m_chainInfoMutex);
    auto cacheItr = m_chainInfoCache.find(chain);
    if (cacheItr == m_chainInfoCache.end())
    {
        chainInfo_t info;
        for (const ChainNameParser::LegInfo &legInfo : ChainNameParser::HLTChainInfo(chain))
        {
            const xAODType::ObjectType type = legInfo.type();
            if (type == xAODType::Other)
            {
                // Use 0 to signal that a leg expects no IParticle features
                info.first.push_back(0);
            }
            else
            {
                info.first.push_back(legInfo.multiplicity);
                for (std::size_t idx = 0; idx < legInfo.multiplicity; ++idx)
                    info.second.push_back(type);
            }
        }
        cacheItr = m_chainInfoCache.insert(std::make_pair(chain, std::move(info))).first;
    }
    return cacheItr->second;
  }


  bool MyxAODAnalysis :: matchR3Tool(
      const std::vector<const xAOD::IParticle *> &recoObjects,
      const std::string &chain,
      double matchThreshold,
      bool rerun) const
  {
    if (recoObjects.size() == 0)
      // If there are no reco objects, the matching is trivially true
      return true;
    // Make the LinkInfo type less verbose
    using IPartLinkInfo_t = TrigCompositeUtils::LinkInfo<xAOD::IParticleContainer>;
    using VecLinkInfo_t = std::vector<IPartLinkInfo_t>;
    // TODO - detect if we're looking at run 3 data.
    // If we are, then setting rerun to true should give a warning as it no
    // longer makes sense for run 3

    // In what follows, the same comparisons between reco and trigger objects will done
    // fairly frequently. As these include DR checks we want to minimise how often we do these
    // Therefore we keep track of any comparisons that we've already done
    // There is one map per input reco object, and then each map entry is the keyed on information
    // extracted from the element link
    std::vector<std::map<std::pair<uint32_t, uint32_t>, bool>> cachedComparisons(recoObjects.size());

    // Note that the user can supply a regex pattern which matches multiple chains
    // We should return true if any individual chain matches
    const Trig::ChainGroup *chainGroup = m_trigDecTool->getChainGroup(chain);
    for (const std::string &chainName : chainGroup->getListOfTriggers())
    {
      const chainInfo_t &chainInfo = getChainInfo(chainName);
      if (!m_trigDecTool->isPassed(chainName, rerun ? TrigDefs::Physics | TrigDefs::allowResurrectedDecision : TrigDefs::Physics))
      {
        ATH_MSG_DEBUG("Chain " << chainName << " did not pass");
        continue;
      }
      ATH_MSG_DEBUG("Chain " << chainName << " passed");
      VecLinkInfo_t features = m_trigDecTool->features<xAOD::IParticleContainer>(chainName);
      // See if we have any that have invalid links. This is a sign that the
      // input file does not contain the required information and should be seen
      // as reason for a job failure
      for (IPartLinkInfo_t &linkInfo : features)
      {
          if (!linkInfo.isValid())
          {
              ATH_MSG_ERROR("Chain " << chainName << " has invalid link info!");
              throw std::runtime_error("Bad link info");
          }
      }
      // Now we have to build up combinations
      // TODO - right now we use a filter that passes everything that isn't pointer-equal.
      // This will probably need to be fixed to something else later - at least the unique RoI filter
      TrigCompositeUtils::Combinations combinations = TrigCompositeUtils::buildCombinations(
        chainName,
        features,
        chainInfo.first,
        TrigCompositeUtils::FilterType::UniqueObjects);
      // Warn once per call if one of the chain groups is too small to match anything
      if (combinations.size() < recoObjects.size())
      {
        ATH_MSG_WARNING(
            "Chain " << chainName << " (matching pattern " << chain << ") has too few objects ("
                     << combinations.size() << ") to match the number of provided reco objects (" << recoObjects.size() << ")");
        continue;
      }
      // Now we iterate through the available combinations
      for (const VecLinkInfo_t &combination : combinations)
      {
        // Prepare the index vector
        std::vector<std::size_t> onlineIndices(combination.size());
        std::iota(onlineIndices.begin(), onlineIndices.end(), 0);
        do
        {
          bool match = true;
          for (std::size_t recoIdx = 0; recoIdx < recoObjects.size(); ++recoIdx)
          {
            std::size_t onlineIdx = onlineIndices[recoIdx];
            if (!matchObjects(
                        recoObjects[recoIdx],
                        combination[onlineIdx].link,
                        chainInfo.second[onlineIdx],
                        cachedComparisons[recoIdx],
                        matchThreshold))
            {
              match = false;
              break;
            }
          }
          if (match)
            return true;
        } while (std::next_permutation(onlineIndices.begin(), onlineIndices.end()));
      }
    }

    // If we reach here we've tried all combinations from all chains in the group and none of them matched
    return false;
  }

  bool MyxAODAnalysis ::matchR3Tool(
      const xAOD::IParticle &recoObject,
      const std::string &chain,
      double matchThreshold,
      bool rerun) const
  {
    std::vector<const xAOD::IParticle *> tmpVec{&recoObject};
    return matchR3Tool(tmpVec, chain, matchThreshold, rerun);
  }
  */

  //**************************************************************************

  //By Hand Execution
  double MyxAODAnalysis::dR(double eta1,double phi1, double eta2, double phi2){
    double deta = fabs(eta1 - eta2);
    double dphi = 0;

    if(fabs(phi1 - phi2) < TMath::Pi()){
      dphi = fabs(phi1 - phi2);
    }
    else{
      dphi = 2*TMath::Pi() - fabs(phi1 - phi2);
    }
    
    return sqrt(deta*deta + dphi*dphi);

  }
  
  StatusCode MyxAODAnalysis :: execute ()
  {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    //ANA_MSG_* (VERBOSE,DEBUG,INFO,WARNING,ERROR,FATAL)
    // ANA_MSG_INFO ("In Execute.");
    

    // ANA_MSG_INFO ("execute(): number of photons = " << photon_info->size());
    // for (const xAOD::Egamma* photon : *photon_info) {
    // hist ("h_photonPt")->Fill (photon->pt() * 0.001); // GeV
    // }

    // ANA_MSG_INFO ("execute(): number of electrons = " << electron_info->size());
    // for (const xAOD::Egamma* electron : *electron_info) {
    // hist ("h_electronPt")->Fill (electron->pt() * 0.001); // GeV
    // }

    // const xAOD::JetContainer* jets = nullptr;
    // ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMPFlowJets"));
    // for (const xAOD::Jet* jet : *jets) {
    //   hist ("h_jetPt")->Fill (jet->pt() * 0.001); // GeV
    // } // end for loop over jets

    // print out run and event number from retrieved object

    // ANA_MSG_INFO ("runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    // ANA_MSG_INFO ("Trying to setup trigger stuff");

    // Loop over triggers and check if any are passed
    // bool passTrig = false;
    
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_e26_lhtight_nod0_ivarloose")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_e60_lhmedium")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_mu26_ivarmedium")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_mu50")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_e140_lhloose")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_e300_etcut")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_mu60_0eta105_msonly")) passTrig = true;
    // if(eventInfo->auxdata< bool >("trigPassed_HLT_e13_etcut_ion")) passTrig = true;

    // If no trigger is passed, skip the event
    // if(!passTrig) ANA_MSG_INFO("Fail: "<<passTrig);
    // if(passTrig) ANA_MSG_INFO ("runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" Pass:"<<passTrig);
    

    // std::string trigname;
    // for(int i=0;i<2;i++){
    //   // auto trigname = m_trigList.at(i);
    //   trigname = m_trigList.at(i);
    //   trigDecision = trigDecisionTool->isPassed(trigname);
    //   if(trigDecision) {
    //     ANA_MSG_INFO ("runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<" trigDecision:"<<trigDecision<<" with Trigger name: "<<trigname);
    //     ANA_MSG_INFO("TrigDecision terminated successfully."<<" eventNumber = " << eventInfo->eventNumber());
    //   }

    //   for(const auto eg : *electron_info){
    //       // if(m_matchTool->match(eg,trigname)) ANA_MSG_INFO("1. Trigname: "<<trigname<<" eventNumber = " << eventInfo->eventNumber());
    //       match(trigname,eg);
    //     }
      
    // }
    
    // if(!trigDecision){
    //   ANA_MSG_INFO("Trigger failed: "<<trigDecision);
    //   }
    // else{
    //   ANA_MSG_INFO("Trigger passed: "<<trigDecision);
    // }

    // auto trigger = 'HLT_g50_loose_L1EM20VH';
    // const TrigInfo info = getTrigInfo(trigger);
    // ANA_MSG_INFO("Test Chain Analysis ============================= " << trigger);

    // if(tdt()->isPassed(trigger)){
    //   ANA_MSG_INFO("Trigger Passed");
    // }
    // else{
    //   ANA_MSG_INFO("Trigger Failed");
    // }
    // Does Not Work for me. I dont know why!
    // Access the TrigDecision object
    // const xAOD::TrigDecision *trigDecision = nullptr;
    // if (!evtStore()->retrieve(trigDecision, "TrigDecisionInfo").isSuccess()) {
    //   ANA_MSG_INFO( "Failed to retrieve TrigDecision object!");
    // }
    // else{
    //   ANA_MSG_INFO( "Got the TrigDecision object!");
    // }


    //Main Code Here
    //Retrieve the eventInfo object from the event store
    const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

    const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
    ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get photon pointer

    const xAOD::PhotonContainer *Online_photon_info = nullptr; //Define online photon pointer
    ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_egamma_Photons"));  //Get photon pointer

    const xAOD::ElectronContainer *electron_info = nullptr; //Define electron pointer
    ANA_CHECK (evtStore()->retrieve (electron_info, "Electrons"));  //Get electron pointer

    const xAOD::ElectronContainer *Online_electron_info = nullptr; //Define Online electron pointer
    ANA_CHECK (evtStore()->retrieve (Online_electron_info, "HLT_egamma_Electrons"));  //Get electron pointer

    // const xAOD::IParticleContainer* electron_info = 0;
    // ANA_CHECK( evtStore()->retrieve( electron_info, "Electrons" ));

    // ANA_MSG_INFO ("runNumber =   " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    std::vector<const xAOD::IParticle*> myParticles;
    /*
    //Force Check By hand
    ns<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"<<endl;
    int idx=0;
    ns<<"Event Number: "<<eventInfo->eventNumber()<<endl;
    string trigname = "HLT_e15_etcut_ion_L1eEM15";
    bool trigDecision = trigDecisionTool->isPassed(trigname);
    if(trigDecision) {
      ns<<"Passes the trigdecision object."<<endl;
      if(Online_electron_info->size()>0){
        for(const auto eg_Online : *Online_electron_info){
          double On_eg_eta = eg_Online->eta(); 
          double On_eg_phi = eg_Online->phi(); 
          if(electron_info->size()>0){
            for(const auto eg_Offine : *electron_info){
              double Off_eg_eta = eg_Offine->eta(); 
              double Off_eg_phi = eg_Offine->phi(); 

              double dist = dR(On_eg_eta,On_eg_phi,Off_eg_eta,Off_eg_phi);

              if(dist <= 0.1){
                ns<<"Match Found for pair #"<<++idx<<endl;
                ns<<"Online (Eta,Phi): ("<<On_eg_eta<<","<<On_eg_phi<<") and Online (Eta,Phi): ("<<Off_eg_eta<<","<<Off_eg_phi<<")"<<endl;
                break;
              }
            }
          }
          else{
            ns<<"No Offline Electrons in the Event. Skipp!";
            break;
          }
        }
      }
      else{
        ns<<"No Online Electrons in the Event. Skipp!!";
      }
    }
    ns<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"<<endl;
    */
    
    /*
    //Using the R3MatchingTool.cxx Method
    bool res(true);
    bool res2(true);
    ns<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"<<endl;
    ns<<"Event Number: "<<eventInfo->eventNumber()<<endl;
    
      ns<<"Inside Electron Info. Size: "<<electron_info->size()<<endl;

      for(uint i = 0; i < electron_info->size(); i++) {
        myParticles.clear();
        myParticles.push_back(electron_info->at(i));
        // ns<<"Old: "<<res<<endl;
        res =  match(myParticles,"HLT_e15_etcut_ion_L1EM12");
        res2 = match(myParticles,"HLT_e15_etcut_ion_L1eEM15");
        // ns<<"New: "<<res<<endl;
        if(res || res2){
          const auto eg = electron_info->at(i);
          double eg_mass = eg->m()*0.001; //GeV
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
          double eg_eta = eg->eta();
          double eg_phi = eg->phi();

          ns<<"Event #: "<<eventInfo->eventNumber()<<"Electron Info-> P_t: "<<eg_pt<<", Eta: "<<eg_eta<<", Phi: "<<eg_phi<<endl;
        }
      }
    
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
    */
    //Using the TestMatchingToolAlg.cxx Method
    /*
    bool res(false);
    ns<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"<<endl;
    ns<<"Event Number: "<<eventInfo->eventNumber()<<endl;
    
      ns<<"Inside Electron Info. Size: "<<electron_info->size()<<endl;

      for(uint i = 0; i < electron_info->size(); i++) {
        myParticles.clear();
        myParticles.push_back(electron_info->at(i));
        ns<<"Old: "<<res<<endl;
        res =  m_tmt->match(myParticles,"HLT_e15_etcut_ion_L1eEM15");
        ns<<"New: "<<res<<endl;
        if(res){
          const auto eg = electron_info->at(i);
          double eg_mass = eg->m()*0.001; //GeV
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
          double eg_eta = eg->eta();
          double eg_phi = eg->phi();

          ns<<"Event #: "<<eventInfo->eventNumber()<<"P_t: "<<eg_pt<<", Eta: "<<eg_eta<<", Phi: "<<eg_phi<<endl;
        }
      }
    
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
    */
    /*
    //For Offline Electrons
    ns<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\"<<endl;
    ns<<"Event Number: "<<eventInfo->eventNumber()<<endl;
    string trigname = m_trigList.at(0);
    bool trigDecision = trigDecisionTool->isPassed(trigname);
    if(trigDecision) ns<<"Passes the trigdecision object."<<endl;

    // bool evt_pass = m_matchtool->isPassed(trigname);
    // if(evt_pass) ns<<"Passes the m_matchtool test."<<endl;
    if(electron_info->size()>0){
      for(const auto eg : *electron_info){
        //Reconstruced Object, Trigger name, dR value used for matching, rerun flag??
        bool pass = true;
        bool pass2 = true;
        // pass = m_matchtool->match({eg}, "HLT_e15_etcut_ion_L1EM12",1,false);
        // pass2 = m_matchtool->match({eg}, "HLT_e15_etcut_ion_L1eEM15",1,false);

        pass = m_matchtool->match({eg}, "HLT_e15_etcut_ion_L1EM12");
        pass2 = m_matchtool->match({eg}, "HLT_e15_etcut_ion_L1eEM15");

        // if(trigDecision){
        //   ns<<"Passes HLT_e15_etcut_ion_L1EM12? "<<pass<<endl;
        //   ns<<"Passes HLT_e15_etcut_ion_L1eEM15? "<<pass2<<endl;
        //   }
        if(pass || pass2){
          double eg_mass = eg->m()*0.001; //GeV
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
          double eg_eta = eg->eta();
          double eg_phi = eg->phi();

          ns<<"Event #: "<<eventInfo->eventNumber()<<"P_t: "<<eg_pt<<", Eta: "<<eg_eta<<", Phi: "<<eg_phi<<endl;

          Dist_Et[0][0]->Fill(eg_et); //Get the Et distribution
          if(eg_pt>=15){
          EtaPhiMaps[0][0]->Fill(eg_eta,eg_phi);  //Get the EtaPhiMap
          }
        }
      }
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
  }  
  else{
    ns<<"No Offline Elecrons in this event."<<endl;
  }
  */
    /*
    //For Offline Photons
    std::string trigger;
    trigger = m_trigList.at(0);
    auto fc = (m_trigDecTool->features(trigger,TrigDefs::alsoDeactivateTEs));

    const auto vec = fc.get<xAOD::ElectronContainer>("",TrigDefs::alsoDeactivateTEs);

    ns<<endl<<"Event info: "<< eventInfo->eventNumber()<<endl;
      for(auto feat : vec){
        ns<<"Inside Vec: "<<endl;
        
        const xAOD::ElectronContainer *cont = feat.cptr();
        int idx=0;
        for(const auto el : *cont){
          double eg_pt = el->pt()*0.001;//GeV
          ns<<"Inside el: "<<idx++<<", pT: "<<eg_pt<<endl;
        }
      }
      for(const auto eg : *Online_electron_info){
        double eg_mass = eg->m()*0.001; //GeV
        double eg_pt = eg->pt()*0.001;//GeV
        double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
        double eg_eta = eg->eta();
        double eg_phi = eg->phi();
      }
    */
    

    /*
    //For getting Sum FcaEt of the event
    const xAOD::HIEventShapeContainer *hies = 0;  
    ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

    double m_sumEt_A = 0;
    double m_sumEt_C = 0;
    for (const xAOD::HIEventShape *ES : *hies) {
      double et = ES->et()*1e-3;
      double eta = ES->etaMin();

      int layer = ES->layer();
      if (layer != 21 && layer != 22 && layer != 23) continue;
      if (eta > 0) {
        m_sumEt_A += et;
      } else {
        m_sumEt_C += et;
      }
    }
    double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
    if(m_sumEt>2000) ANA_MSG_INFO("Fcal Sum_Et = "<<m_sumEt);

    
    //For Offline Electrons
    for(const auto eg : *electron_info){
      double eg_mass = eg->m()*0.001; //GeV
      double eg_pt = eg->pt()*0.001;//GeV
      double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
      double eg_eta = eg->eta();
      double eg_phi = eg->phi();

      Dist_Et[0][0]->Fill(eg_et); //Get the Et distribution
      if(eg_pt>=15){
       EtaPhiMaps[0][0]->Fill(eg_eta,eg_phi);  //Get the EtaPhiMap
      }
    }

    //For Offline Photons
    for(const auto eg : *photon_info){
      double eg_mass = eg->m()*0.001; //GeV
      double eg_pt = eg->pt()*0.001;//GeV
      double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
      double eg_eta = eg->eta();
      double eg_phi = eg->phi();
      
      Dist_Et[0][1]->Fill(eg_et); //Get the Et distribution
      if(eg_pt>=15){
       EtaPhiMaps[0][1]->Fill(eg_eta,eg_phi);  //Get the EtaPhiMap
      }
    }

    
    //For Online Electrons
    for(const auto eg : *Online_electron_info){
      double eg_mass = eg->m()*0.001; //GeV
      double eg_pt = eg->pt()*0.001;//GeV
      double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
      double eg_eta = eg->eta();
      double eg_phi = eg->phi();

      Dist_Et[1][0]->Fill(eg_et); //Get the Et distribution
      if(eg_pt>=15){
       EtaPhiMaps[1][0]->Fill(eg_eta,eg_phi);  //Get the EtaPhiMap
      }
    }

    //For Online Photons
    for(const auto eg : *Online_photon_info){
      double eg_mass = eg->m()*0.001; //GeV
      double eg_pt = eg->pt()*0.001;//GeV
      double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
      double eg_eta = eg->eta();
      double eg_phi = eg->phi();
      
      Dist_Et[1][1]->Fill(eg_et); //Get the Et distribution
      if(eg_pt>=15){
       EtaPhiMaps[1][1]->Fill(eg_eta,eg_phi);  //Get the EtaPhiMap
      }
    }
    */
    
    
    
    //Making Efficiency Plots
    
    //Compute fcalEt of the Event
    const xAOD::HIEventShapeContainer *hies = 0;  
    ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

    double m_sumEt_A = 0;
    double m_sumEt_C = 0;
    for (const xAOD::HIEventShape *ES : *hies) {
      double et = ES->et()*1e-3;
      double eta = ES->etaMin();

      int layer = ES->layer();
      if (layer != 21 && layer != 22 && layer != 23) continue;
      if (eta > 0) {
        m_sumEt_A += et;
      } else {
        m_sumEt_C += et;
      }
    }
    double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
    // if(m_sumEt>2000) ANA_MSG_INFO("Fcal Sum_Et = "<<m_sumEt);

    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";

    for(int trig=0;trig<int(m_trigList.size()*1.0);trig++){//Loop over all triggers in the list
      string trigname = m_trigList.at(0);  //Get The Trigger Name
      bool trigDecision = trigDecisionTool->isPassed(trigname);//Event passed the relevant trigger?

      if(trigDecision){  
        ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the TrigDecision for Trigger name: "<<trigname;
        ns<<endl;
        if(electron_info->size()>0){
          for(const auto eg : *electron_info){  //Loop Over Offline Electrons
            int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one

            double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            double eg_phi = eg->phi();

            if(pass){ //Histograms for electrons that pass
              ns<<"Electron # "<<++idx<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<endl;
              for(int j=0;j<4;j++){
                double Hist_xval;
                if(j==0) Hist_xval = m_sumEt;
                if(j==1) Hist_xval = eg_pt;
                if(j==2) Hist_xval = eg_eta;
                if(j==3) Hist_xval = eg_phi;

                if(fabs(eg_eta)<1.37){//Barrel
                  Electron_Hists[0][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Electron_Hists[0][j][1]->Fill(Hist_xval);
                } 
                
                Electron_Hists[0][j][2]->Fill(Hist_xval);
              }
            } //m_matchtool pass condition ends

            //Histograms showing all electrons in the passes event
            for(int j=0;j<4;j++){
                double Hist_xval;
                if(j==0) Hist_xval = m_sumEt;
                if(j==1) Hist_xval = eg_pt;
                if(j==2) Hist_xval = eg_eta;
                if(j==3) Hist_xval = eg_phi;

                if(fabs(eg_eta)<1.37){//Barrel
                  Electron_Hists[1][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Electron_Hists[1][j][1]->Fill(Hist_xval);
                } 
                
                Electron_Hists[1][j][2]->Fill(Hist_xval);
              }
          }//Offline Electron Loop ends
        }
        else{
          ns<<"This event has no offline electrons."<<endl;
        }
        
        ns<<endl<<endl;
        if(photon_info->size()>0){
          for(const auto eg : *photon_info){  //Loop Over Offline Photons
            int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one

            double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            double eg_phi = eg->phi();

            if(pass){ //Histograms for electrons that pass
              ns<<"Photon # "<<++idx<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<endl;
              for(int j=0;j<4;j++){
                double Hist_xval;
                if(j==0) Hist_xval = m_sumEt;
                if(j==1) Hist_xval = eg_pt;
                if(j==2) Hist_xval = eg_eta;
                if(j==3) Hist_xval = eg_phi;

                if(fabs(eg_eta)<1.37){//Barrel
                  Photon_Hists[0][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Photon_Hists[0][j][1]->Fill(Hist_xval);
                } 
                
                Photon_Hists[0][j][2]->Fill(Hist_xval);
              }
            } //m_matchtool pass condition ends

            //Histograms showing all electrons in the passes event
            for(int j=0;j<4;j++){
                double Hist_xval;
                if(j==0) Hist_xval = m_sumEt;
                if(j==1) Hist_xval = eg_pt;
                if(j==2) Hist_xval = eg_eta;
                if(j==3) Hist_xval = eg_phi;

                if(fabs(eg_eta)<1.37){//Barrel
                  Photon_Hists[1][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Photon_Hists[1][j][1]->Fill(Hist_xval);
                } 
                
                Photon_Hists[1][j][2]->Fill(Hist_xval);
              }
          }//Offline Photon Loop ends
        }
        else{
          ns<<"This event has no offline photons."<<endl;
        }

      }
      else{
        ns<<"Event #: "<<eventInfo->eventNumber()<<" fails the TrigDecision for Trigger name: "<<trigname;
      }
    }

    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";

    return StatusCode::SUCCESS;
  }


  void MyxAODAnalysis::Save_Hists(std::string outfilename){
    std::string Ghost_name;
    char name[500];

    TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");

    //Saving the Efficiency related histograms
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        for(int k=0;k<3;k++){
          if(i==0) Ghost_name = "EvtPass";
          if(i==1) Ghost_name = "Total_EvtPass";

          if(j==0) Ghost_name += "_SumFcalEt";
          if(j==1) Ghost_name += "_Pt";
          if(j==2) Ghost_name += "_Eta";
          if(j==3) Ghost_name += "_Phi";

          if(k==0) Ghost_name +="Eta_Barrel";
          if(k==1) Ghost_name +="Eta_EndCap";
          if(k==2) Ghost_name +="Eta_All";

          sprintf(name,"Photon_%s",Ghost_name.c_str());
          Outfile->WriteObject(Photon_Hists[i][j][k],name);

          sprintf(name,"Electron_%s",Ghost_name.c_str());
          Outfile->WriteObject(Electron_Hists[i][j][k],name);

        }
      }
    }

    

    Outfile->Close();
    ANA_MSG_INFO("All Histograms have been saved.");
  }


  StatusCode MyxAODAnalysis :: finalize ()
  {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.

    ANA_MSG_INFO ("In Finalize.");

    std::string outfilename = "/afs/cern.ch/user/a/adimri/Egamma_Trigger/Output_New.root";
    Save_Hists(outfilename);

    ns<<endl<<"....................End....................."<<endl;
    ns.close();
    return StatusCode::SUCCESS;
  }
