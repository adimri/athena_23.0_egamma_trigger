cmake_minimum_required( VERSION 3.24.3 )
project( CheckHIP LANGUAGES CXX )
list( INSERT CMAKE_MODULE_PATH 0 /cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.54/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules )
enable_language( HIP )
file( WRITE "${CMAKE_CURRENT_BINARY_DIR}/result.cmake"
   "set( CMAKE_HIP_COMPILER \"${CMAKE_HIP_COMPILER}\" )" )