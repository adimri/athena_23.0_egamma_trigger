file(REMOVE_RECURSE
  "genConf/MyAnalysis.confdb2_part"
  "../x86_64-el9-gcc13-opt/include/MyAnalysis"
  "../x86_64-el9-gcc13-opt/jobOptions/MyAnalysis/ATestRun_jobOptions.py"
  "../x86_64-el9-gcc13-opt/lib/libMyAnalysis.confdb"
  "../x86_64-el9-gcc13-opt/python/MyAnalysis/MyAnalysisConf.py"
  "../x86_64-el9-gcc13-opt/python/MyAnalysis/__init__.py"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/ATLAS_PACKAGES_TARGET.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
