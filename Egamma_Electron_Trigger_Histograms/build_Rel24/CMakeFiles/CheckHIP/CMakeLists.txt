cmake_minimum_required( VERSION 3.27.5 )
project( CheckHIP LANGUAGES CXX )
list( INSERT CMAKE_MODULE_PATH 0 /cvmfs/atlas.cern.ch/repo/sw/software/24.0/Athena/24.0.20/InstallArea/x86_64-el9-gcc13-opt/cmake/modules )
enable_language( HIP )
file( WRITE "${CMAKE_CURRENT_BINARY_DIR}/result.cmake"
   "set( CMAKE_HIP_COMPILER \"${CMAKE_HIP_COMPILER}\" )" )