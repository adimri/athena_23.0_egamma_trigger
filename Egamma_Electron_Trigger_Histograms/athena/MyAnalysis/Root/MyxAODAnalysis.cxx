#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackingPrimitives.h" //Particle Hypothesis
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>


#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingToolMT.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

using namespace std;

MyxAODAnalysis :: MyxAODAnalysis (const std::string &name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
    ,m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool")
    ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
    ,m_matchtool(this,"Trig::MatchingTool")
    // ,m_matchTool_MT("Trig::TrigEgammaMatchingToolMT/TrigEgammaMatchingToolMT")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  ANA_MSG_INFO ("Inside  Constructor.");

  //For Trigger Tools
  declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
  declareProperty("Papa_TrigDecisionTool", m_trigDecTool, "The trigger decision tool");
  declareProperty("MatchingTool", m_matchtool); //Matching
  // declareProperty( "MatchingToolMT" , m_matchTool_MT );

}

StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("In Initialize.");
  //Open File for checking purposes
  // ns.open("CheckFile_New.txt");
  // ns<<".....................Begin....................."<<endl;
  
  nevt = 0;

  //Define the histograms
  Init_Hists();
  
  //Initialize the tree
  Init_Tree();

  //Check if the decision tool is working properly
  ANA_CHECK(trigDecisionTool.retrieve());
  ANA_CHECK(m_trigDecTool.retrieve());
  ANA_CHECK(m_matchtool.retrieve());
  // ANA_CHECK(m_matchTool_MT.retrieve());

  // GRL. Good Run List Tool
  m_grl.setTypeAndName("GoodRunsListSelectionTool/myGRLTool");

  //Offline Electron Tool Selectors (from Qipeng)
  std::string confDir = "ElectronPhotonSelectorTools/offline/mc23_20230728_HI/";

  m_Electron_TightIsEMSelector = new AsgElectronLikelihoodTool ("TightLH");;
  ANA_CHECK(m_Electron_TightIsEMSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodTightOfflineConfig2023_HI_Smooth.conf"));
  ANA_CHECK(m_Electron_TightIsEMSelector->initialize());

  m_Electron_LooseIsEMSelector = new AsgElectronLikelihoodTool ("LooseLH");
  ANA_CHECK(m_Electron_LooseIsEMSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2023_HI_Smooth.conf"));
  ANA_CHECK(m_Electron_LooseIsEMSelector->initialize());

  m_Electron_MediumIsEMSelector = new AsgElectronLikelihoodTool ("MediumLH");
  ANA_CHECK(m_Electron_MediumIsEMSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodMediumOfflineConfig2023_HI_Smooth.conf"));
  ANA_CHECK(m_Electron_MediumIsEMSelector->initialize());

  //Offline Photon Tool Selectors (from Qipeng)
  m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
  ANA_CHECK(m_photonTightIsEMSelector->setProperty("WorkingPoint", "TightPhoton"));
  ANA_CHECK(m_photonTightIsEMSelector->initialize());

  m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
  ANA_CHECK(m_photonLooseIsEMSelector->setProperty("WorkingPoint", "LoosePhoton"));
  ANA_CHECK(m_photonLooseIsEMSelector->initialize());

  m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonMediumIsEMSelector" );
  ANA_CHECK(m_photonMediumIsEMSelector->setProperty("WorkingPoint", "MediumPhoton"));
  ANA_CHECK(m_photonMediumIsEMSelector->initialize());

  // m_Electron_TightIsEMSelector = new AsgElectronLikelihoodTool ("TightLH");;
  // ANA_CHECK(m_Electron_TightIsEMSelector->setProperty("WorkingPoint","LooseLHElectron"));
  // ANA_CHECK(m_Electron_TightIsEMSelector->initialize());

  // m_Electron_LooseIsEMSelector = new AsgElectronLikelihoodTool ("LooseLH");
  // ANA_CHECK(m_Electron_LooseIsEMSelector->setProperty("WorkingPoint","MediumLHElectron"));
  // ANA_CHECK(m_Electron_LooseIsEMSelector->initialize());

  // m_Electron_MediumIsEMSelector = new AsgElectronLikelihoodTool ("MediumLH");
  // ANA_CHECK(m_Electron_MediumIsEMSelector->setProperty("WorkingPoint","TightLHElectron"));
  // ANA_CHECK(m_Electron_MediumIsEMSelector->initialize());

  ANA_MSG_INFO ("Initialization Complete.");

  return StatusCode::SUCCESS;
}


void MyxAODAnalysis::Init_Hists(){
  std::string Ghost_name;
  // double pi = TMath::Pi();
  char name[500];

  // sprintf(name,"Hist_Invariant_Mass_Rec_ZBoson_All");
  // Hist_Inv_Mass_All = new TH1D(name,name,2000,-0.05,199.95);

}

void MyxAODAnalysis::Clear_Vectors(){
  //For Electrons
    m_Electron_charge.clear();
    m_Electron_px.clear();
    m_Electron_py.clear();
    m_Electron_pz.clear();
    m_Electron_pT.clear();
    m_Electron_m.clear();
    m_Electron_eta.clear();
    m_Electron_phi.clear();

    m_Electron_Pass_Loose.clear(); //Identification Variable
    m_Electron_Pass_Medium.clear(); //Identification Variable
    m_Electron_Pass_Tight.clear();  //Identification Variable
    m_Electron_topoetcone30.clear();   //Isolation Variable
    // m_Electron_etcone30.clear();       //Isolation Variable
    m_Electron_ptcone30.clear();       //Isolation Variable
    
    //For Trigger L1_EM12
    m_Electron_Pass_L1_EM12.clear();
    m_Electron_All_ClosestdR_EM12.clear();
    m_Electron_Pass_e15_EtCut_ion_L1EM12.clear();
    m_Electron_Pass_e15_lhloose_L1EM12.clear();
    m_Electron_Pass_e15_loose_L1EM12.clear();
    m_Electron_Pass_e15_lhmedium_L1EM12.clear();
    m_Electron_Pass_e15_medium_L1EM12.clear();
    
    //For Trigger L1_eEM15
    m_Electron_Pass_L1_eEM15.clear();
    m_Electron_All_ClosestdR_eEM15.clear();
    m_Electron_Pass_e15_EtCut_ion_L1eEM15.clear();
    m_Electron_Pass_e15_lhloose_L1eEM15.clear();
    m_Electron_Pass_e15_loose_L1eEM15.clear();
    m_Electron_Pass_e15_lhmedium_L1eEM15.clear();
    m_Electron_Pass_e15_medium_L1eEM15.clear();

  //For Photons
    m_Photon_px.clear();
    m_Photon_py.clear();
    m_Photon_pz.clear();
    m_Photon_pT.clear();
    m_Photon_eta.clear();
    m_Photon_phi.clear();

    m_Photon_Pass_Loose.clear(); //Identification Variable
    m_Photon_Pass_Medium.clear(); //Identification Variable
    m_Photon_Pass_Tight.clear();  //Identification Variable
    m_Photon_topoetcone30.clear();   //Isolation Variable
    m_Photon_ptcone30.clear();       //Isolation Variable
    
    //For Trigger L1_EM12
    m_Photon_Pass_L1_EM12.clear();
    m_Photon_All_ClosestdR_EM12.clear();
    m_Photon_Online_L1EM12_pT.clear();
    m_Photon_Online_L1EM12_eta.clear();
    m_Photon_Online_L1EM12_phi.clear();
    m_Photon_Pass_HLT_g18_etcut_ion_L1EM12.clear();
    m_Photon_Pass_HLT_g20_loose_ion_L1EM12.clear();
    
    //For Trigger L1_eEM15
    m_Photon_Pass_L1_eEM15.clear();
    m_Photon_All_ClosestdR_eEM15.clear();
    m_Photon_Online_L1eEM15_pT.clear();
    m_Photon_Online_L1eEM15_eta.clear();
    m_Photon_Online_L1eEM15_phi.clear();
    m_Photon_Pass_HLT_g18_etcut_ion_L1eEM15.clear();
    m_Photon_Pass_HLT_g20_loose_ion_L1eEM15.clear();

    //For Trigger In HLT Legacy
    m_Photon_All_ClosestdR_HLT_IdLegacy.clear();
    m_Photon_Online_HLT_IdLegacy_pT.clear();
    m_Photon_Online_HLT_IdLegacy_eta.clear();
    m_Photon_Online_HLT_IdLegacy_phi.clear();

    //For Trigger In HLT Phase-I
    m_Photon_All_ClosestdR_HLT_IdPhaseI.clear();
    m_Photon_Online_HLT_IdPhaseI_pT.clear();
    m_Photon_Online_HLT_IdPhaseI_eta.clear();
    m_Photon_Online_HLT_IdPhaseI_phi.clear();
}

void MyxAODAnalysis::Init_Tree(){
  /*
  Save_TagAndProbe_Outs = new TTree("Tree_TagAndProbe","Tree_TagAndProbe");
  Save_TagAndProbe_Outs->Branch("run_number",&run_number,"run_number/I");
  Save_TagAndProbe_Outs->Branch("event_number",&event_number,"event_number/L" );
  Save_TagAndProbe_Outs->Branch("Pass_L1_Triggers",&Pass_L1_Triggers,"Pass_L1_Triggers[3]/O" );
  Save_TagAndProbe_Outs->Branch("Pass_EtCut_Triggers",&Pass_EtCut_Triggers,"Pass_EtCut_Triggers[3]/O" );
  Save_TagAndProbe_Outs->Branch("Pass_Id_Triggers",&Pass_Id_Triggers,"Pass_Id_Triggers[12]/O" );
  Save_TagAndProbe_Outs->Branch("Probe_pT",&Probe_pT,"Probe_pT/F");
  Save_TagAndProbe_Outs->Branch("Probe_pz",&Probe_pz,"Probe_pz/F");
  Save_TagAndProbe_Outs->Branch("Probe_m",&Probe_m,"Probe_m/F");
  Save_TagAndProbe_Outs->Branch("Probe_eta",&Probe_eta,"Probe_eta/F");
  Save_TagAndProbe_Outs->Branch("Probe_phi",&Probe_phi,"Probe_phi/F");
  Save_TagAndProbe_Outs->Branch("Probe_index",&Probe_index,"Probe_index/I");
  Save_TagAndProbe_Outs->Branch("Tag_pT",&Tag_pT,"Tag_pT/F");
  Save_TagAndProbe_Outs->Branch("Tag_pz",&Tag_pz,"Tag_pz/F");
  Save_TagAndProbe_Outs->Branch("Tag_m",&Tag_m,"Tag_m/F");
  Save_TagAndProbe_Outs->Branch("Tag_eta",&Tag_eta,"Tag_eta/F");
  Save_TagAndProbe_Outs->Branch("Tag_phi",&Tag_phi,"Tag_phi/F");
  Save_TagAndProbe_Outs->Branch("Tag_index",&Tag_index,"Tag_index/I");
  Save_TagAndProbe_Outs->Branch("Evt_fcalEt",&Evt_fcalEt,"Evt_fcalEt/F");
  */

  Save_Electrons = new TTree("Tree_Egamma","Tree_Egamma");

  //For Electrons
    /*
    Save_Electrons->Branch("run_number",&run_number);
    Save_Electrons->Branch("event_number",&event_number);
    Save_Electrons->Branch("Vector_size",&Vector_size);
    Save_Electrons->Branch("Electron_charge",&m_Electron_charge);
    Save_Electrons->Branch("Electron_px",&m_Electron_px);
    Save_Electrons->Branch("Electron_py",&m_Electron_py);
    Save_Electrons->Branch("Electron_pz",&m_Electron_pz);
    Save_Electrons->Branch("Electron_m",&m_Electron_m);
    Save_Electrons->Branch("Electron_eta",&m_Electron_eta);
    Save_Electrons->Branch("Electron_phi",&m_Electron_phi);
    Save_Electrons->Branch("Electron_Pass_Loose",&m_Electron_Pass_Loose);
    Save_Electrons->Branch("Electron_Pass_Medium",&m_Electron_Pass_Medium);
    Save_Electrons->Branch("Electron_Pass_Tight",&m_Electron_Pass_Tight);
    Save_Electrons->Branch("Electron_topoetcone30",&m_Electron_topoetcone30);
    Save_Electrons->Branch("Electron_ptcone30",&m_Electron_ptcone30);
    Save_Electrons->Branch("Electron_Pass_L1_EM12",&m_Electron_Pass_L1_EM12);
    Save_Electrons->Branch("Electron_ClosestdR_EM12",&m_Electron_All_ClosestdR_EM12);
    Save_Electrons->Branch("Electron_Pass_e15_EtCut_ion_L1EM12",&m_Electron_Pass_e15_EtCut_ion_L1EM12);
    Save_Electrons->Branch("Electron_Pass_e15_lhloose_L1EM12",&m_Electron_Pass_e15_lhloose_L1EM12);
    Save_Electrons->Branch("Electron_Pass_e15_loose_L1EM12",&m_Electron_Pass_e15_loose_L1EM12);
    Save_Electrons->Branch("Electron_Pass_e15_lhmedium_L1EM12",&m_Electron_Pass_e15_lhmedium_L1EM12);
    Save_Electrons->Branch("Electron_Pass_e15_medium_L1EM12",&m_Electron_Pass_e15_medium_L1EM12);
    Save_Electrons->Branch("Electron_Pass_L1_eEM15",&m_Electron_Pass_L1_eEM15);
    Save_Electrons->Branch("Electron_ClosestdR_eEM15",&m_Electron_All_ClosestdR_eEM15);
    Save_Electrons->Branch("Electron_Pass_e15_EtCut_ion_eL1EM15",&m_Electron_Pass_e15_EtCut_ion_L1eEM15);
    Save_Electrons->Branch("Electron_Pass_e15_lhloose_eL1EM15",&m_Electron_Pass_e15_lhloose_L1eEM15);
    Save_Electrons->Branch("Electron_Pass_e15_loose_eL1EM15",&m_Electron_Pass_e15_loose_L1eEM15);
    Save_Electrons->Branch("Electron_Pass_e15_lhmedium_eL1EM15",&m_Electron_Pass_e15_lhmedium_L1eEM15);
    Save_Electrons->Branch("Electron_Pass_e15_medium_eL1EM15",&m_Electron_Pass_e15_medium_L1eEM15);
    */

  //For Photons
    Save_Electrons->Branch("Photon_Vector_size",&Photon_Vector_size);
    Save_Electrons->Branch("Photon_px",&m_Photon_px);
    Save_Electrons->Branch("Photon_py",&m_Photon_py);
    Save_Electrons->Branch("Photon_pz",&m_Photon_pz);
    Save_Electrons->Branch("Photon_pT",&m_Photon_pT);
    Save_Electrons->Branch("Photon_eta",&m_Photon_eta);
    Save_Electrons->Branch("Photon_phi",&m_Photon_phi);
    Save_Electrons->Branch("Photon_Pass_Loose",&m_Photon_Pass_Loose); 
    Save_Electrons->Branch("Photon_Pass_Medium",&m_Photon_Pass_Medium); 
    Save_Electrons->Branch("Photon_Pass_Tight",&m_Photon_Pass_Tight);  
    Save_Electrons->Branch("Photon_topoetcone30",&m_Photon_topoetcone30);   
    Save_Electrons->Branch("Photon_ptcone30",&m_Photon_ptcone30);       
    Save_Electrons->Branch("Photon_Pass_L1_EM12",&m_Photon_Pass_L1_EM12);
    Save_Electrons->Branch("Photon_All_ClosestdR_EM12",&m_Photon_All_ClosestdR_EM12);
    Save_Electrons->Branch("Photon_Online_L1EM12_pT",&m_Photon_Online_L1EM12_pT);
    Save_Electrons->Branch("Photon_Online_L1EM12_eta",&m_Photon_Online_L1EM12_eta);
    Save_Electrons->Branch("Photon_Online_L1EM12_phi",&m_Photon_Online_L1EM12_phi);
    // Save_Electrons->Branch("Photon_Pass_HLT_g18_etcut_ion_L1EM12",&m_Photon_Pass_HLT_g18_etcut_ion_L1EM12);
    Save_Electrons->Branch("Photon_Pass_HLT_g20_loose_ion_L1EM12",&m_Photon_Pass_HLT_g20_loose_ion_L1EM12);
    Save_Electrons->Branch("Photon_Pass_L1_eEM15",&m_Photon_Pass_L1_eEM15);
    Save_Electrons->Branch("Photon_All_ClosestdR_eEM15",&m_Photon_All_ClosestdR_eEM15);
    Save_Electrons->Branch("Photon_Online_L1eEM15_pT",&m_Photon_Online_L1eEM15_pT);
    Save_Electrons->Branch("Photon_Online_L1eEM15_eta",&m_Photon_Online_L1eEM15_eta);
    Save_Electrons->Branch("Photon_Online_L1eEM15_phi",&m_Photon_Online_L1eEM15_phi);
    // Save_Electrons->Branch("Photon_Pass_HLT_g18_etcut_ion_L1eEM15",&m_Photon_Pass_HLT_g18_etcut_ion_L1eEM15);
    Save_Electrons->Branch("Photon_Pass_HLT_g20_loose_ion_L1eEM15",&m_Photon_Pass_HLT_g20_loose_ion_L1eEM15);

    Save_Electrons->Branch("Photon_All_ClosestdR_HLT_IdLegacy",&m_Photon_All_ClosestdR_HLT_IdLegacy);
    Save_Electrons->Branch("Photon_Online_HLT_IdLegacy_pT",&m_Photon_Online_HLT_IdLegacy_pT);
    Save_Electrons->Branch("Photon_Online_HLT_IdLegacy_eta",&m_Photon_Online_HLT_IdLegacy_eta);
    Save_Electrons->Branch("Photon_Online_HLT_IdLegacy_phi",&m_Photon_Online_HLT_IdLegacy_phi);

    Save_Electrons->Branch("Photon_All_ClosestdR_HLT_IdPhaseI",&m_Photon_All_ClosestdR_HLT_IdPhaseI);
    Save_Electrons->Branch("Photon_Online_HLT_IdPhaseI_pT",&m_Photon_Online_HLT_IdPhaseI_pT);
    Save_Electrons->Branch("Photon_Online_HLT_IdPhaseI_eta",&m_Photon_Online_HLT_IdPhaseI_eta);
    Save_Electrons->Branch("Photon_Online_HLT_IdPhaseI_phi",&m_Photon_Online_HLT_IdPhaseI_phi);
    


    Save_Electrons->Branch("Evt_fcalEt",&Evt_fcalEt,"Evt_fcalEt/F");
  
}

void MyxAODAnalysis::Save_Hists(std::string outfilename){
  std::string Ghost_name;
  char name[500];

  TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");
  
  // sprintf(name,"Hist_Invariant_Mass_Rec_ZBoson_All");
  // Outfile->WriteObject(Hist_Inv_Mass_All,name);

  // Outfile->WriteObject(Save_TagAndProbe_Outs,"Tree_TagAndProbe");
  Outfile->WriteObject(Save_Electrons,"Tree_Egamma");

  Outfile->Close();
  ANA_MSG_INFO("All Histograms have been saved.");
}

//Functions to return relevant indices.
int MyxAODAnalysis::Get_FcalEt_Index(double Sum_fcalEt){
  if(Sum_fcalEt>0 && Sum_fcalEt<500) return (0);
  if(Sum_fcalEt>500 && Sum_fcalEt<1000) return (1);
  if(Sum_fcalEt>1000 && Sum_fcalEt<1500) return (2);
  if(Sum_fcalEt>1500 && Sum_fcalEt<2000) return (3);
  if(Sum_fcalEt>2000 && Sum_fcalEt<2500) return (4);
  if(Sum_fcalEt>2500 && Sum_fcalEt<3000) return (5);
  if(Sum_fcalEt>3000 && Sum_fcalEt<3500) return (6);
  if(Sum_fcalEt>3500 && Sum_fcalEt<4000) return (7);
  if(Sum_fcalEt>4000 && Sum_fcalEt<4500) return (8);
  if(Sum_fcalEt>4500 && Sum_fcalEt<5000) return (9);

  return (-1); //To handle cases where Sum_fcalEt>=6TeV
}

int MyxAODAnalysis::Get_Eta_Index(double Eta_Val){
    
  if(fabs(Eta_Val) < 1.37) return 0; //Barrel
  if(fabs(Eta_Val)<2.37 && fabs(Eta_Val)>1.52 ) return 1; //End Caps

  return -1; //Else In the Crack Region or at the extreme edge. All Condition.

}


//Functions to perform the matching

//By hand execution of L1_EM12 matching for Electron
std::pair<bool,double> MyxAODAnalysis::L1_EM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI,
                                       bool match, double dR_Threshold){

  if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ 
      return std::make_pair(false, 10000); //Checks if the EmTauRoI object is correct or not??Ask!!
  }
  //Condition checks if the ROI type is indeed for a Run 2 EM object.
  //Another two possiblities are Run 1 or Tau of Run 2

  const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
  
  bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12")
                                                                 != std::end(thresholdNames));
  if(!EM12ThresholdPassed){
    return std::make_pair(false, 10000);
  }

  //Compute dR
  float eta = EMTauRoI->eta();
  float phi = EMTauRoI->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  } 

  // return (match);
  return std::make_pair(match, dR);
}

//By hand execution of L1_EM15 matching for Electron
bool MyxAODAnalysis::L1_EM15_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI,
                                       bool match, double dR_Threshold){

  if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ 
      return (false); //Checks if the EmTauRoI object is correct or not??Ask!!
  }  
  //Condition checks if the ROI type is indeed for a Run 2 EM object.
  //(Another two possiblities are Run 1 or Tau of Run 2)

  const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
  
  // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));

  bool EM15ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM15") != std::end(thresholdNames));
  if(!EM15ThresholdPassed){
    return (false);
  }

  //Compute dR
  float eta = EMTauRoI->eta();
  float phi = EMTauRoI->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }

  return (match);
}

//By hand execution of L1_eEM15 matching for Electron
std::pair<bool,double> MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15,
                                        bool match, double dR_Threshold){
  //No need for ROI type condition here. 
  //eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
  //That is why no such condition is required and no such function exists.

  if(eEM15->et()*1e-3 < 13){ //eEM15 objects should pass the threshold
    return std::make_pair(false, 10000);
  }

  //eEMRoI does not have threshold names.
  //But it has something called the TOB word or the xTOB word.
  //I don't know if that needs to be matched or not.
  if(!(eEM15->isTOB())) {
    // ns<<"Not a trigger object. (TOB). Continue."<<endl;
    return std::make_pair(false, 10000);
  }
  //Sanity Checks. L1_eEMRoI stores only TOB objects. 
  //This is just the final nail in the coffin to make sure.

  //Compute dR
  float eta = eEM15->eta();
  float phi = eEM15->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  // return (match);
  return std::make_pair(match, dR);
}

std::pair<bool,double> MyxAODAnalysis::L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI,
                                       bool match, double dR_Threshold){

  if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ 
      std::make_pair(false, 10000); //Checks if the EmTauRoI object is correct or not??Ask!!
  }
  //Condition checks if the ROI type is indeed for a Run 2 EM object.
  //Another two possiblities are Run 1 or Tau of Run 2

  const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
  
  bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12")
                                                                 != std::end(thresholdNames));
  if(!EM12ThresholdPassed){
    return std::make_pair(false, 10000);
  }

  //Compute dR
  float eta = EMTauRoI->eta();
  float phi = EMTauRoI->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  } 

  // return (match);
  return std::make_pair(match, dR);
}

std::pair<bool,double> MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15,
                                        bool match, double dR_Threshold){
  //No need for ROI type condition here. 
  //eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
  //That is why no such condition is required and no such function exists.

  if(eEM15->et()*1e-3 < 13){ //eEM15 objects should pass the threshold
    return std::make_pair(false, 10000);
  }

  //eEMRoI does not have threshold names.
  //But it has something called the TOB word or the xTOB word.
  //I don't know if that needs to be matched or not.
  if(!(eEM15->isTOB())) {
    // ns<<"Not a trigger object. (TOB). Continue."<<endl;
    return std::make_pair(false, 10000);
  }
  //Sanity Checks. L1_eEMRoI stores only TOB objects. 
  //This is just the final nail in the coffin to make sure.

  //Compute dR
  float eta = eEM15->eta();
  float phi = eEM15->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  // return (match);
  return std::make_pair(match, dR);
}

//By hand execution of Id Triggers matching for Photon
std::pair<bool,double> MyxAODAnalysis::HLT_TrigMatch(const xAOD::Photon *eg, const xAOD::Egamma *online_eg, bool match, double dR_Threshold){
  //Compute dR
  float eta = online_eg->eta();
  float phi = online_eg->phi();

  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  // return (match);
  return std::make_pair(match, dR);
}

//Check if the electron is a good tag electron.
bool MyxAODAnalysis::IsTagElectron(const xAOD::Electron *eg){
  //These codnitions have been taken from https://arxiv.org/pdf/1612.01456.pdf

  double Et = sqrt( pow(eg->pt(),2) + pow(eg->m(),2) ) *0.001; //Et in GeV
  double eta = eg->eta();

  if(!bool(m_Electron_TightIsEMSelector->accept(eg))) return 0; //Tag electron must pass Tight identification.
  if(Et<25) return 0; //Pass the Et Threshold.
  if(abs(eta)>2.47) return 0; //Pass the correct eta coverage.
  if(abs(eta)>1.37 && abs(eta)<1.52) return 0;  //Should not come from the crack region.

  return 1;
}

bool MyxAODAnalysis::IsPossibleTagElectron(const xAOD::Electron *eg){
  double pT = eg->pt()*0.001; //pT in GeV
  double eta = eg->eta();

  if(pT<10) return 0; //Pass the Et Threshold.
  // if(pT<25) return 0; //Pass the Et Threshold.
  if(abs(eta)>2.47) return 0; //Pass the correct eta coverage.
  if(abs(eta)>1.37 && abs(eta)<1.52) return 0;  //Should not come from the crack region.

  return 1;
}



StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // if(nevt>10) return StatusCode::SUCCESS;

  //Main Code Here
  //Retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

  ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  
  // ns<<"In execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<endl;

  if (!m_grl->passRunLB(*eventInfo)) { //Check if the run passes the good run list tool
    ANA_MSG_DEBUG ("drop event: GRL");
    // ns<<"Dropping Event:GRL->"<<eventInfo->runNumber()<<" : "<<eventInfo->eventNumber()<<endl;
    return StatusCode::SUCCESS;
  }
  
  //Compute fcalEt of the Event
  const xAOD::HIEventShapeContainer *hies = 0;  
  ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

  double m_sumEt_A = 0;
  double m_sumEt_C = 0;
  for (const xAOD::HIEventShape *ES : *hies) {
    double et = ES->et()*1e-3;
    double eta = ES->etaMin();

    int layer = ES->layer();
    if (layer != 21 && layer != 22 && layer != 23) continue;
    if (eta > 0) {
      m_sumEt_A += et;
    } else {
      m_sumEt_C += et;
    }
  }
  double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
  Evt_fcalEt = m_sumEt;

  // int fcalEt_idx = Get_FcalEt_Index(m_sumEt);// fcalEt index is fixed for an event.

  //Getting the relevant containers
  //Get the L1 Photon Triggers
  const xAOD::EmTauRoIContainer *m_EMTauRoIs = nullptr; //Define the L1 particle info pointer
  ANA_CHECK (evtStore()->retrieve(m_EMTauRoIs, "LVL1EmTauRoIs"));  //Get the L1 (legacy) event info pointer  

  const xAOD::eFexEMRoIContainer *m_eEM15 = nullptr; //Define the L1 particle info pointer
  ANA_CHECK (evtStore()->retrieve(m_eEM15, "L1_eEMRoI"));  //Get the L1 (Phase-I) event info pointer

  const xAOD::ElectronContainer* electron_info = 0; //Information for offline electrons
  ANA_CHECK (evtStore()->retrieve (electron_info, "Electrons"));

  const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
  ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get Offline photon pointer

  double Zee_Mass_PDG = 91.8176; //In GeV
  double Zee_Mass_Var = 15; //Acceptable Variance In GeV of reconstructed mass of Z from Z->ee reconstruction.
  // int count=0;

  run_number = eventInfo->runNumber();
  event_number = eventInfo->eventNumber();
  
  /*
  Tag_index = 0;
  for(auto electron_tag : *electron_info){
    if(!IsTagElectron(electron_tag)) continue;  //Ignore if the electron fails the tag electron Cuts.

    TLorentzVector Vec_Tag = electron_tag->p4();
    int charge_tag = electron_tag->charge();

    //Set up the tree varaibles.
    Tag_pz = Vec_Tag.Z()*0.001;            // In GeV
    Tag_pT = electron_tag->pt()*0.001;    // In GeV
    Tag_eta = electron_tag->eta();
    Tag_phi = electron_tag->phi();
    Tag_m = electron_tag->m()*0.001;      //In GeV


    Probe_index=0;
    
    for(auto electron_probe: *electron_info){
      if(!bool(m_Electron_MediumIsEMSelector->accept(electron_probe))) continue; //Probe Electron must pass the Medium Identificaton for HI.

      int charge_probe = electron_probe->charge();
      if(charge_probe*charge_tag != -1) continue;  //Tag and probe are of opposite charges

      TLorentzVector Vec_Probe = electron_probe->p4();

      TLorentzVector Zee_Candidate = Vec_Tag + Vec_Probe; //Possible Z->ee candidate
      double inv_Zee_mass = Zee_Candidate.M()*0.001; //Invariant Mass in GeV
      Hist_Inv_Mass_All->Fill(inv_Zee_mass);
      
      //Tree Variables
      Probe_pz = Vec_Probe.Z()*0.001;            // In GeV
      Probe_m = electron_probe->m()*0.001;      //In GeV
      Probe_pT = electron_probe->pt()*0.001;    // In GeV
      Probe_eta = electron_probe->eta();
      Probe_phi = electron_probe->phi();
      double dR_Threshold = 0.1;


      if(inv_Zee_mass<(Zee_Mass_PDG+Zee_Mass_Var) && inv_Zee_mass>(Zee_Mass_PDG-Zee_Mass_Var)){ //Probe Electron is from Z->ee event
        // Probe_Electrons.push_back(electron_probe);  //Push back into the Probe Electron Vector if it passes the condtion.

        for(int L1_idx=0;L1_idx<nof_L1_Triggers;L1_idx++){

          string Trigname = m_trigList_L1_Electrons.at(L1_idx);
          Pass_L1_Triggers[L1_idx] = false;
          bool Trig_legacy = false;
          bool Trig_PhaseI = false;

          if(Trigname == "L1_EM12") Trig_legacy = true;
          if(Trigname == "L1_EM15") Trig_legacy = true;
          if(Trigname == "L1_eEM15") Trig_PhaseI = true;

          if(Trig_legacy){
            for (const auto EMTauRoI: *m_EMTauRoIs){  //Online Egamma Containers for L1 Legacy Triggers
              bool pass = false;

              if(Trigname=="L1_EM12") pass = L1_EM12_TrigMatch(electron_probe,EMTauRoI,pass, dR_Threshold);
              if(Trigname=="L1_EM15") pass = L1_EM15_TrigMatch(electron_probe,EMTauRoI,pass, dR_Threshold);

              if(pass==true){
                Pass_L1_Triggers[L1_idx] = true;
                break;
              }   
            }
          }//For Legacy Triggers

          if(Trig_PhaseI){
            for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){   //Online Egamma Containers for L1 PhaseI Triggers
              bool pass = false;

              if(Trigname=="L1_eEM15") pass = L1_eEM15_TrigMatch(electron_probe,eEM15,pass, dR_Threshold);

              if(pass==true){
                Pass_L1_Triggers[L1_idx] = true;
                break;
              }   
            }
          }//For Phase I Triggers

        } //Loop for L1 Triggers

        for(int EtCut_idx=0;EtCut_idx<nof_EtCut_Triggers;EtCut_idx++){
          string Trigname = m_trigList_EtCut_Electrons.at(EtCut_idx);

          Pass_EtCut_Triggers[EtCut_idx] = false;
          Pass_EtCut_Triggers[EtCut_idx] = m_matchtool->match({electron_probe}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

        }//Loop for EtCut Triggers

        for(int Id_idx=0;Id_idx<nof_Id_Triggers;Id_idx++){
          string Trigname = m_trigList_Id_Electrons.at(Id_idx);

          Pass_Id_Triggers[Id_idx] = false;
          Pass_Id_Triggers[Id_idx] = m_matchtool->match({electron_probe}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

        }//Loop for Id Triggers
        
        Save_TagAndProbe_Outs->Fill(); //Fill the tree with required properties of Z->ee electrons
        
      }//Probe Electron is from Z->ee event

      Probe_index++;
    }

    Tag_index++;
  }
  */

  
  //Clear the vectors for each event
  Clear_Vectors();
  
  /*
  //Work for Electron Begin
  Vector_size = 0;
  //Now working for the general tree with property Et>25, atleast medium selection 
  for(auto electron: *electron_info){
    if(!IsPossibleTagElectron(electron)) continue;  //Ignore if the electron pT<25 GeV and comes from bad eta coverage.
    // if(!bool(m_Electron_MediumIsEMSelector->accept(electron))) continue; //Electron must atleast pass the Medium Identificaton for HI.
    if(!bool(m_Electron_LooseIsEMSelector->accept(electron))) continue; //Electron must atleast pass the Loose Identificaton for HI.

    // cout<<"Pass"<<endl;

    //Possible Good Electron
    TLorentzVector Vec_Electron = electron->p4();

    //Store the basic electron properties
    m_Electron_charge.push_back(electron->charge()*1.0);
    m_Electron_px.push_back(Vec_Electron.X());
    m_Electron_py.push_back(Vec_Electron.Y());
    m_Electron_pz.push_back(Vec_Electron.Z());
    m_Electron_pT.push_back(electron->pt()*0.001);
    m_Electron_m.push_back(electron->m()*0.001);
    m_Electron_eta.push_back(electron->eta());
    m_Electron_phi.push_back(electron->phi());
    
    bool pass_loose = bool(m_Electron_LooseIsEMSelector->accept(electron));
    bool pass_medium = bool(m_Electron_MediumIsEMSelector->accept(electron));
    bool pass_tight = bool(m_Electron_TightIsEMSelector->accept(electron));

    //Store the Isolation and Identification Variable
    m_Electron_Pass_Loose.push_back(pass_loose); 
    m_Electron_Pass_Medium.push_back(pass_medium); 
    m_Electron_Pass_Tight.push_back(pass_tight);  
    m_Electron_topoetcone30.push_back(electron->isolation(xAOD::Iso::IsolationType::topoetcone30));   
    // m_Electron_etcone30.push_back(electron->isolation(xAOD::Iso::IsolationType::etcone30)); //etcone30 is no longer availaible       
    m_Electron_ptcone30.push_back(electron->isolation(xAOD::Iso::IsolationType::ptcone30));       

    //Now the triggers
    double dR_Threshold = 0.1;
    
    for(int L1_idx=0;L1_idx<nof_L1_Triggers;L1_idx++){

      string Trigname = m_trigList_L1_Electrons.at(L1_idx);
      bool Trig_Pass = false;
      bool Trig_legacy = false;
      bool Trig_PhaseI = false;

      if(Trigname == "L1_EM12") Trig_legacy = true;
      if(Trigname == "L1_EM15") continue;
      if(Trigname == "L1_eEM15") Trig_PhaseI = true;

      std::pair<bool,double> Trig_Property;
      double Closest_dR = 10000; //Initialize

      if(Trig_legacy){
        for (const auto EMTauRoI: *m_EMTauRoIs){  //Online Egamma Containers for L1 Legacy Triggers
          bool pass = false;

          if(Trigname=="L1_EM12") Trig_Property = L1_EM12_TrigMatch(electron,EMTauRoI,pass, dR_Threshold);
          if(Trigname=="L1_EM15") continue;

          pass=Trig_Property.first;

          //Check if we got a closer match to an online object
          if(Trig_Property.second < Closest_dR) Closest_dR = Trig_Property.second;

          if(pass==true){
            Trig_Pass = true;
            break;
          }   
        }
      }//For Legacy Triggers

      if(Trig_PhaseI){
        for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){   //Online Egamma Containers for L1 PhaseI Triggers
          bool pass = false;

          if(Trigname=="L1_eEM15") Trig_Property = L1_eEM15_TrigMatch(electron,eEM15,pass, dR_Threshold);

          pass=Trig_Property.first;

          //Check if we got a closer match to an online object
          if(Trig_Property.second < Closest_dR) Closest_dR = Trig_Property.second;

          if(pass==true){
            Trig_Pass = true;
            break;
          }   
        }
      }//For Phase I Triggers

      if(Trigname == "L1_EM12") {
        m_Electron_Pass_L1_EM12.push_back(Trig_Pass);
        m_Electron_All_ClosestdR_EM12.push_back(Closest_dR);
      }
      if(Trigname == "L1_eEM15") {
        m_Electron_Pass_L1_eEM15.push_back(Trig_Pass);
        m_Electron_All_ClosestdR_eEM15.push_back(Closest_dR);
      }

      // ns<<"Trigname: "<<Trigname<<":: Pass? "<<Trig_Pass<<":: Closest dR = "<<Closest_dR<<endl; //Sanity Check
    } //Loop for L1 Triggers
    
    for(int EtCut_idx=0;EtCut_idx<nof_EtCut_Triggers;EtCut_idx++){
      string Trigname = m_trigList_EtCut_Electrons.at(EtCut_idx);

      if(Trigname == "HLT_e18_etcut_ion_L1EM15") continue;

      bool Trig_Pass = false;
      Trig_Pass = m_matchtool->match({electron}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

      if(Trigname == "HLT_e15_etcut_ion_L1EM12") m_Electron_Pass_e15_EtCut_ion_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_etcut_ion_L1eEM15") m_Electron_Pass_e15_EtCut_ion_L1eEM15.push_back(Trig_Pass);

    }//Loop for EtCut Triggers
    
    for(int Id_idx=0;Id_idx<nof_Id_Triggers;Id_idx++){
      string Trigname = m_trigList_Id_Electrons.at(Id_idx);

      if(Trigname == "HLT_e18_lhloose_nogsf_ion_L1EM15") continue;
      if(Trigname == "HLT_e18_loose_nogsf_ion_L1EM15") continue;
      if(Trigname == "HLT_e18_lhmedium_nogsf_ion_L1EM15") continue;
      if(Trigname == "HLT_e18_medium_nogsf_ion_L1EM15") continue;

      bool Trig_Pass = false;
      Trig_Pass = m_matchtool->match({electron}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

      if(Trigname == "HLT_e15_lhloose_nogsf_ion_L1EM12")  m_Electron_Pass_e15_lhloose_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_loose_nogsf_ion_L1EM12")    m_Electron_Pass_e15_loose_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_lhmedium_nogsf_ion_L1EM12") m_Electron_Pass_e15_lhmedium_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_medium_nogsf_ion_L1EM12")   m_Electron_Pass_e15_medium_L1EM12.push_back(Trig_Pass);

      if(Trigname == "HLT_e15_lhloose_nogsf_ion_L1eEM15")   m_Electron_Pass_e15_lhloose_L1eEM15.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_loose_nogsf_ion_L1eEM15")     m_Electron_Pass_e15_loose_L1eEM15.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_lhmedium_nogsf_ion_L1eEM15")  m_Electron_Pass_e15_lhmedium_L1eEM15.push_back(Trig_Pass);
      if(Trigname == "HLT_e15_medium_nogsf_ion_L1eEM15")    m_Electron_Pass_e15_medium_L1eEM15.push_back(Trig_Pass);

    }//Loop for Id Triggers

  }
  
  Vector_size = m_Electron_charge.size();
  //Work For Electtons is over
  */

  //For Hand Match of Id Triggers
  
  std::vector<const xAOD::Egamma*> online_ph_vec[nof_Id_Triggers_Photon];
  for(int i=0;i<nof_Id_Triggers_Photon;i++) online_ph_vec[i].clear(); //Clear the vectors. Good Habits.
  
  std::string key = "HLT_egamma_Photons";
  
  for(int Id_trig_idx=0;Id_trig_idx<nof_Id_Triggers_Photon;Id_trig_idx++){
    string trigname = m_trigList_Id_Photons.at(Id_trig_idx);
    
    auto vec =  m_trigDecTool->features<xAOD::PhotonContainer>(trigname, TrigDefs::Physics ,key );      

    for( auto &featLinkInfo : vec ){
      if(! featLinkInfo.isValid() ) continue;
      const auto *feat = *(featLinkInfo.link);
      if(!feat) continue;   
      online_ph_vec[Id_trig_idx].push_back(feat);
    }
  }
  

  Photon_Vector_size = 0;

  // ns<<"Photon Vector size: "<<photon_info->size()<<endl;
  for(auto photon: *photon_info){
    // ns<<"Inside Photon Loop."<<endl;
    bool pass_loose = bool(m_photonLooseIsEMSelector->accept(photon));
    bool pass_medium = bool(m_photonMediumIsEMSelector->accept(photon));
    bool pass_tight = bool(m_photonTightIsEMSelector->accept(photon)); 

    if(!pass_tight) continue; //Just want to look at tight photons at the moment
    // if( (photon->pt()*0.001) < 5) continue;  //Photon should atleast have a transverse momentum of 5 GeV

    // ns<<"Passes Both condition at nevt: "<<nevt<<"pT"<<photon->pt()*0.001<<endl;

    TLorentzVector Vec_Photon = photon->p4();

    //Store the basic photon properties
    m_Photon_px.push_back(Vec_Photon.X());
    m_Photon_py.push_back(Vec_Photon.Y());
    m_Photon_pz.push_back(Vec_Photon.Z());
    m_Photon_pT.push_back(photon->pt()*0.001);
    m_Photon_eta.push_back(photon->eta());
    m_Photon_phi.push_back(photon->phi());

    m_Photon_Pass_Loose.push_back(pass_loose); 
    m_Photon_Pass_Medium.push_back(pass_medium); 
    m_Photon_Pass_Tight.push_back(pass_tight);  
    m_Photon_topoetcone30.push_back(photon->isolation(xAOD::Iso::IsolationType::topoetcone30)); 
    m_Photon_ptcone30.push_back(photon->isolation(xAOD::Iso::IsolationType::ptcone30)); 

    //Now the triggers
    double dR_Threshold = 0.1;

    for(int L1_idx=0;L1_idx<nof_L1_Triggers_Photon;L1_idx++){

      string Trigname = m_L1_trigList_Photons.at(L1_idx);
      bool Trig_Pass = false;
      bool Trig_legacy = false;
      bool Trig_PhaseI = false;

      if(Trigname == "L1_EM12") Trig_legacy = true;
      if(Trigname == "L1_eEM15") Trig_PhaseI = true;

      std::pair<bool,double> Trig_Property;
      double Closest_dR = 10000; //Initialize
      float ClosestOnline_pT = -10;
      float ClosestOnline_eta = -10;
      float ClosestOnline_phi = -10;


      if(Trig_legacy){
        for (const auto EMTauRoI: *m_EMTauRoIs){  //Online Egamma Containers for L1 Legacy Triggers
          bool pass = false;

          if(Trigname=="L1_EM12") Trig_Property = L1_EM12_TrigMatch(photon,EMTauRoI,pass, dR_Threshold);

          pass=Trig_Property.first;

          //Check if we got a closer match to an online object
          if(Trig_Property.second < Closest_dR) {
            Closest_dR = Trig_Property.second;
            ClosestOnline_pT = EMTauRoI->eT()*0.001; //In GeV
            ClosestOnline_eta = EMTauRoI->eta();
            ClosestOnline_phi = EMTauRoI->phi();
          }

          if(pass==true){
            Trig_Pass = true;
            break;
          }   
        }
      }//For Legacy Triggers

      if(Trig_PhaseI){
        for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){   //Online Egamma Containers for L1 PhaseI Triggers
          bool pass = false;

          if(Trigname=="L1_eEM15") Trig_Property = L1_eEM15_TrigMatch(photon,eEM15,pass, dR_Threshold);

          pass=Trig_Property.first;

          //Check if we got a closer match to an online object
          if(Trig_Property.second < Closest_dR) {
            Closest_dR = Trig_Property.second;
            ClosestOnline_pT = eEM15->et()*0.001; //In GeV
            ClosestOnline_eta = eEM15->eta();
            ClosestOnline_phi = eEM15->phi();
          }

          if(pass==true){
            Trig_Pass = true;
            break;
          }   
        }
      }//For Phase I Triggers

      if(Trigname == "L1_EM12") {
        m_Photon_Pass_L1_EM12.push_back(Trig_Pass);
        m_Photon_All_ClosestdR_EM12.push_back(Closest_dR);
        m_Photon_Online_L1EM12_pT.push_back(ClosestOnline_pT);
        m_Photon_Online_L1EM12_eta.push_back(ClosestOnline_eta);
        m_Photon_Online_L1EM12_phi.push_back(ClosestOnline_phi);
      }
      if(Trigname == "L1_eEM15") {
        m_Photon_Pass_L1_eEM15.push_back(Trig_Pass);
        m_Photon_All_ClosestdR_eEM15.push_back(Closest_dR);
        m_Photon_Online_L1eEM15_pT.push_back(ClosestOnline_pT);
        m_Photon_Online_L1eEM15_eta.push_back(ClosestOnline_eta);
        m_Photon_Online_L1eEM15_phi.push_back(ClosestOnline_phi);
      }

      // ns<<"Trigname: "<<Trigname<<":: Pass? "<<Trig_Pass<<":: Closest dR = "<<Closest_dR<<endl; //Sanity Check
    } //Loop for L1 Triggers

    /*
    for(int EtCut_idx=0;EtCut_idx<nof_EtCut_Triggers_Photon;EtCut_idx++){
      string Trigname = m_trigList_EtCut_Photons.at(EtCut_idx);

      bool Trig_Pass = false;
      Trig_Pass = m_matchtool->match({photon}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

      if(Trigname == "HLT_g18_etcut_ion_L1EM12") m_Photon_Pass_HLT_g18_etcut_ion_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_g18_etcut_ion_L1eEM15") m_Photon_Pass_HLT_g18_etcut_ion_L1eEM15.push_back(Trig_Pass);

    }//Loop for EtCut Triggers
    */

    for(int Id_idx=0;Id_idx<nof_Id_Triggers_Photon;Id_idx++){
      string Trigname = m_trigList_Id_Photons.at(Id_idx);

      bool Trig_Pass = false;
      Trig_Pass = m_matchtool->match({photon}, Trigname, dR_Threshold); //Check if the offline object matches with any online one;

      if(Trigname == "HLT_g20_loose_ion_L1EM12")  m_Photon_Pass_HLT_g20_loose_ion_L1EM12.push_back(Trig_Pass);
      if(Trigname == "HLT_g20_loose_ion_L1eEM15")   m_Photon_Pass_HLT_g20_loose_ion_L1eEM15.push_back(Trig_Pass);

    }//Loop for Id Triggers
    
    //Hand Match Loop for Id Triggers
    for(int Id_idx=0;Id_idx<nof_Id_Triggers_Photon;Id_idx++){
      string Trigname = m_trigList_Id_Photons.at(Id_idx);

      std::pair<bool,double> Trig_Property;
      double Closest_dR = 10000; //Initialize
      float ClosestOnline_pT = -10;
      float ClosestOnline_eta = -10;
      float ClosestOnline_phi = -10;

      for(int i=0;i<int(1*online_ph_vec[Id_idx].size());i++){
        const xAOD::Egamma *online_eg =  online_ph_vec[Id_idx].at(i);

        bool pass = false;
        Trig_Property = HLT_TrigMatch(photon,online_eg,pass, dR_Threshold);

        //Check if we got a closer match to an online object
        if(Trig_Property.second < Closest_dR) {
          Closest_dR = Trig_Property.second;
          ClosestOnline_pT = online_eg->pt()*0.001; //In GeV
          ClosestOnline_eta = online_eg->eta();
          ClosestOnline_phi = online_eg->phi();
        }
      }

      if(Trigname == "HLT_g20_loose_ion_L1EM12"){
        m_Photon_All_ClosestdR_HLT_IdLegacy.push_back(Closest_dR);
        m_Photon_Online_HLT_IdLegacy_pT.push_back(ClosestOnline_pT);
        m_Photon_Online_HLT_IdLegacy_eta.push_back(ClosestOnline_eta);
        m_Photon_Online_HLT_IdLegacy_phi.push_back(ClosestOnline_phi);
      }

      if(Trigname == "HLT_g20_loose_ion_L1eEM15"){
        m_Photon_All_ClosestdR_HLT_IdPhaseI.push_back(Closest_dR);
        m_Photon_Online_HLT_IdPhaseI_pT.push_back(ClosestOnline_pT);
        m_Photon_Online_HLT_IdPhaseI_eta.push_back(ClosestOnline_eta);
        m_Photon_Online_HLT_IdPhaseI_phi.push_back(ClosestOnline_phi);
      }
    }
    
  }
  
  Photon_Vector_size = m_Photon_pT.size();
  //Work For Photons is over

  //Fill the tree at the end of each event
  Save_Electrons->Fill();

  // ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
  // ns<<endl;

  nevt++;

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  ANA_MSG_INFO ("In Finalize.");
  
  std::string outfilename = "Output_New.root";
  Save_Hists(outfilename);

  // ns<<endl<<"....................End....................."<<endl;
  // ns.close();
  return StatusCode::SUCCESS;
}
