#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags
# testFile = ['/eos/user/a/adimri/Data_Shower_Shapes/Data_2018/data18_hi/data18_hi.00365512.physics_HardProbes.merge.AOD.f1021_m2037._lb0072._0001.1']
# testFile = ['/eos/user/a/adimri/Data_Shower_Shapes/Data_2023/data23_hi/data23_hi.00462549.physics_HardProbes.merge.AOD.f1399_m2209._lb0300._0001.1']
testFile = ['/eos/user/a/adimri/Data_Shower_Shapes/Data_2023/data23_hi/data23_hi.00462995.physics_HardProbes.merge.AOD.f1408_m2212._lb0142._0001.1']
# testFile = ['/eos/user/a/adimri/Data_Shower_Shapes/Data_ReProcessing/data23_hi/data23_hi.00463124.physics_HardProbes.merge.AOD.f1408_m2212._lb0749._0001.1']
# testFile = ['/eos/user/a/adimri/Data_Shower_Shapes/Data_ReProcessing/data23_hi/AOD.40128242._003014.pool.root.1']

# testFile = ['/eos/user/a/adimri/Data22/data23_hi/AOD.37845209._000001.pool.root.1']
print ("Testfile: ",testFile)

#override next line on command line with: --filesInput=XXX
# jps.AthenaCommonFlags.FilesInput = [testFile[0],testFile[1]]
athenaCommonFlags.FilesInput.set_Value_and_Lock(testFile)


#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration. 
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import addPrivateTool, createAlgorithm, createPublicTool
from AthenaCommon.AppMgr import ToolSvc
from TrigConfxAOD.TrigConfxAODConf import TrigConf__xAODConfigTool

alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )   #alg is the object of the algorithm

# ToolSvc += TrigConf__xAODConfigTool('ConfigTool')
ToolSvc += CfgMgr.GoodRunsListSelectionTool("myGRLTool",GoodRunsListVec=["./data23_hi.periodAllYear_DetStatus-v113-pro31-08_MERGED_PHYS_HeavyIonP_All_Good.xml"])

#Making the Matching Tool
tdt = createPublicTool("Trig::TrigDecisionTool", "TrigDecisionTool")
addPrivateTool(tdt, "ConfigTool", "TrigConf::xAODConfigTool")

#Next Line of Code is specifically for Run3
tdt.NavigationFormat = "TrigComposite"


# tdt.HLTSummary = "HLTNav_Summary_DAODSlimmed"
# tdt.HLTSummary = "HLTNav_Summary_OnlineSlimmed"
tdt.HLTSummary = "HLTNav_Summary_AODSlimmed"
addPrivateTool(alg, "MatchingTool", "Trig::R3MatchingTool")
alg.trigDecisionTool = tdt
alg.MatchingTool.TrigDecisionTool = tdt

#Write the properties
# alg.ElectronPtCut = 30000.0
# alg.SampleName = 'Zee'
# alg.JetPtCut = 1000.5

#Output level
#Default is INFO. Can change to VERBOSE/DEBUG/INFO/WARNING/ERROR/FATAL
# alg.OutputLevel=DEBUG

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
# theApp.EvtMax = 10

#For Histograms
# jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:Test1.root"]
# svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

