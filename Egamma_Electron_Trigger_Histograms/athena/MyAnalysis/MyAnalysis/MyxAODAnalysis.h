#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingToolMT.h"
#include <AsgTools/ToolHandle.h> 

//Taken from Qipeng
#include "TriggerMatchingTool/R3MatchingTool.h"
// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> // GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 

//Some Basic Global Variables
const int nof_L1_Triggers = 3;
const int nof_EtCut_Triggers = 3;
const int nof_Id_Triggers = 12;

const int nof_L1_Triggers_Photon = 2;
const int nof_EtCut_Triggers_Photon = 2;
const int nof_Id_Triggers_Photon = 2;


const int max_electron_types = 3; //Loose, Medium and Tight
const int max_fcalEt_bins = 11; //0.25,0.75,1.25, ... , 4.75. And All.
const int max_Eta_bins = 3; //Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

const int nof_xaxes = 3; //pT, fcalEt, Eta.
const int max_pT_bins = 3; //pT bins. pT>20, pT>35, All pT.
const double pT_bin_Vals[3] = {20,35,0};

class MyxAODAnalysis : public EL::AnaAlgorithm
{
  private:


  public:
  std::ofstream ns; //File Handle

  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  
  //My Functions
  void Init_Hists();
  void Init_Tree();
  void Save_Hists(std::string);

  //Likelihood Tool Selectors
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //grl tool
  
  //For Electrons
  AsgElectronLikelihoodTool *m_Electron_TightIsEMSelector; 
  AsgElectronLikelihoodTool *m_Electron_LooseIsEMSelector; 
  AsgElectronLikelihoodTool *m_Electron_MediumIsEMSelector; 

  //For Photons
  AsgPhotonIsEMSelector *m_photonTightIsEMSelector; 
  AsgPhotonIsEMSelector *m_photonLooseIsEMSelector; 
  AsgPhotonIsEMSelector *m_photonMediumIsEMSelector; 

  //Trigger Decision Tools
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool; //Using directly the Trigdecision Tool
  ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool; //Trig Decision Tool
  ToolHandle<Trig::IMatchingTool> m_matchtool;  //Matching 
  // ToolHandle<TrigEgammaMatchingToolMT> m_matchTool_MT;
  // const ToolHandle<TrigEgammaMatchingToolMT>& match() const {return m_matchTool_New;}

  //L1 wrt MinBias
  std::vector<std::string> m_trigList_L1_Electrons = {"L1_EM12","L1_EM15","L1_eEM15"};
  //EtCut wrt L1
  std::vector<std::string> m_trigList_EtCut_Electrons= {
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e18_etcut_ion_L1EM15",
    "HLT_e15_etcut_ion_L1eEM15"
  };

  //HLT wrt EtCut
  std::vector<std::string> m_trigList_Id_Electrons= {
    "HLT_e15_lhloose_nogsf_ion_L1EM12",
    "HLT_e15_loose_nogsf_ion_L1EM12",
    "HLT_e15_lhmedium_nogsf_ion_L1EM12",
    "HLT_e15_medium_nogsf_ion_L1EM12",
    "HLT_e18_lhloose_nogsf_ion_L1EM15",
    "HLT_e18_loose_nogsf_ion_L1EM15",
    "HLT_e18_lhmedium_nogsf_ion_L1EM15",
    "HLT_e18_medium_nogsf_ion_L1EM15",
    "HLT_e15_lhloose_nogsf_ion_L1eEM15",
    "HLT_e15_loose_nogsf_ion_L1eEM15",
    "HLT_e15_lhmedium_nogsf_ion_L1eEM15",
    "HLT_e15_medium_nogsf_ion_L1eEM15"
  };
  
  //For Photons
  std::vector<std::string> m_L1_trigList_Photons = {"L1_EM12", "L1_eEM15"};
  std::vector<std::string> m_trigList_EtCut_Photons= {
      "HLT_g18_etcut_ion_L1EM12",
      "HLT_g18_etcut_ion_L1eEM15"
    };
  std::vector<std::string> m_trigList_Id_Photons= {
    "HLT_g20_loose_ion_L1EM12",
    "HLT_g20_loose_ion_L1eEM15"
  };

  //Following will be the tree variables for Tag And Probe Tree
  int run_number;                                             //run number
  long long int event_number;                                 //event number

  //For Electrons
    bool Pass_L1_Triggers[nof_L1_Triggers];                     //Pass Info for L1 Triggers (Probe Electron Passes or fails)
    bool Pass_EtCut_Triggers[nof_EtCut_Triggers];               //Pass Info for EtCut Triggers (Probe Electron Passes or fails)
    bool Pass_Id_Triggers[nof_Id_Triggers];                     //Pass Info for Id Triggers (Probe Electron Passes or fails)
    float Probe_pT, Probe_pz, Probe_m, Probe_eta, Probe_phi;    //Probe Electron Info
    float Tag_pT, Tag_pz, Tag_m, Tag_eta, Tag_phi;              //Tag Electron Info
    int Tag_index, Probe_index;                                 //Container index of Tag and Probe electrons respectively 
    float Evt_fcalEt;                                           //Sum fcal Et for the event


    //Following will be the tree variables for Save Egamma
    int Vector_size;
    
    //Electron Properties
    std::vector<float> m_Electron_charge;
    std::vector<float> m_Electron_px;
    std::vector<float> m_Electron_py;
    std::vector<float> m_Electron_pz;
    std::vector<float> m_Electron_pT;
    std::vector<float> m_Electron_m;
    std::vector<float> m_Electron_eta;
    std::vector<float> m_Electron_phi;

    std::vector<bool>   m_Electron_Pass_Loose; //Identification Variable
    std::vector<bool>   m_Electron_Pass_Medium; //Identification Variable
    std::vector<bool>   m_Electron_Pass_Tight;  //Identification Variable
    std::vector<float> m_Electron_topoetcone30;   //Isolation Variable
    // std::vector<float> m_Electron_etcone30;       //Isolation Variable. No longer available
    std::vector<float> m_Electron_ptcone30;       //Isolation Variable
    
    //For Trigger L1_EM12
    std::vector<bool>   m_Electron_Pass_L1_EM12;
    std::vector<double>   m_Electron_All_ClosestdR_EM12;
    std::vector<bool>   m_Electron_Pass_e15_EtCut_ion_L1EM12;
    std::vector<bool>   m_Electron_Pass_e15_lhloose_L1EM12;
    std::vector<bool>   m_Electron_Pass_e15_loose_L1EM12;
    std::vector<bool>   m_Electron_Pass_e15_lhmedium_L1EM12;
    std::vector<bool>   m_Electron_Pass_e15_medium_L1EM12;
    
    //For Trigger L1_eEM15
    std::vector<bool>   m_Electron_Pass_L1_eEM15;
    std::vector<double>   m_Electron_All_ClosestdR_eEM15;
    std::vector<bool>   m_Electron_Pass_e15_EtCut_ion_L1eEM15;
    std::vector<bool>   m_Electron_Pass_e15_lhloose_L1eEM15;
    std::vector<bool>   m_Electron_Pass_e15_loose_L1eEM15;
    std::vector<bool>   m_Electron_Pass_e15_lhmedium_L1eEM15;
    std::vector<bool>   m_Electron_Pass_e15_medium_L1eEM15;

    //Following will be the Photon tree variables for Save Egamma 
    int Photon_Vector_size;
    
    //Photon Properties
    std::vector<float> m_Photon_px;
    std::vector<float> m_Photon_py;
    std::vector<float> m_Photon_pz;
    std::vector<float> m_Photon_pT;
    std::vector<float> m_Photon_eta;
    std::vector<float> m_Photon_phi;

    std::vector<bool>   m_Photon_Pass_Loose; //Identification Variable
    std::vector<bool>   m_Photon_Pass_Medium; //Identification Variable
    std::vector<bool>   m_Photon_Pass_Tight;  //Identification Variable
    std::vector<float> m_Photon_topoetcone30;   //Isolation Variable
    // std::vector<float> m_Electron_etcone30;       //Isolation Variable. No longer available
    std::vector<float> m_Photon_ptcone30;       //Isolation Variable
    
    //For Trigger L1_EM12
    std::vector<bool>   m_Photon_Pass_L1_EM12;
    std::vector<double>   m_Photon_All_ClosestdR_EM12;
    std::vector<float> m_Photon_Online_L1EM12_pT;
    std::vector<float> m_Photon_Online_L1EM12_eta;
    std::vector<float> m_Photon_Online_L1EM12_phi;
    std::vector<bool>   m_Photon_Pass_HLT_g18_etcut_ion_L1EM12;
    std::vector<bool>   m_Photon_Pass_HLT_g20_loose_ion_L1EM12;
    
    //For Trigger L1_eEM15
    std::vector<bool>   m_Photon_Pass_L1_eEM15;
    std::vector<double>   m_Photon_All_ClosestdR_eEM15;
    std::vector<float> m_Photon_Online_L1eEM15_pT;
    std::vector<float> m_Photon_Online_L1eEM15_eta;
    std::vector<float> m_Photon_Online_L1eEM15_phi;
    std::vector<bool>   m_Photon_Pass_HLT_g18_etcut_ion_L1eEM15;
    std::vector<bool>   m_Photon_Pass_HLT_g20_loose_ion_L1eEM15;

    //For Trigger In HLT Legacy
    std::vector<double>   m_Photon_All_ClosestdR_HLT_IdLegacy;
    std::vector<float> m_Photon_Online_HLT_IdLegacy_pT;
    std::vector<float> m_Photon_Online_HLT_IdLegacy_eta;
    std::vector<float> m_Photon_Online_HLT_IdLegacy_phi;

    //For Trigger In HLT Phase-I
    std::vector<double>   m_Photon_All_ClosestdR_HLT_IdPhaseI;
    std::vector<float> m_Photon_Online_HLT_IdPhaseI_pT;
    std::vector<float> m_Photon_Online_HLT_IdPhaseI_eta;
    std::vector<float> m_Photon_Online_HLT_IdPhaseI_phi;

  //Global Variable
  int nevt;

  //Function to get relevant indices
  int Get_FcalEt_Index(double Sum_fcalEt);
  int Get_Eta_Index(double Eta_Val);

  //Function  to perform the matching by hand
  void Clear_Vectors();
  bool IsPossibleTagElectron(const xAOD::Electron *eg);
  bool IsTagElectron(const xAOD::Electron *eg);
  std::pair<bool,double> L1_EM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM12 matching
  bool L1_EM15_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM15 matching
  std::pair<bool,double> L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM15 matching

  std::pair<bool,double> L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM12 matching
  std::pair<bool,double> L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM15 matching

  std::pair<bool,double> HLT_TrigMatch(const xAOD::Photon *eg, const xAOD::Egamma *online_eg, bool match=false, double dR_Threshold=0.1);  //By hand execution of ID Triggers matching

  //Extra Histograms
  // TH1D *Hist_Inv_Mass_All;

  //Tree that stores everything
  TTree *Save_Electrons;
  // TTree *Save_TagAndProbe_Outs;
};
#endif