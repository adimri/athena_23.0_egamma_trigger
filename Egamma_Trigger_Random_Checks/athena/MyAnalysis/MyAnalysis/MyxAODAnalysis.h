#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <AsgTools/ToolHandle.h> 

//Taken from Qipeng
#include "TriggerMatchingTool/R3MatchingTool.h"
// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> // GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 


  class MyxAODAnalysis : public EL::AnaAlgorithm
  {
    private:

    double m_electronPtCut; // Electron pT cut
    std::string m_sampleName; // Sample name
    double m_JetPtCut; //Jet pT Cut
  

    public:
    unsigned int trigDecision = 0; //< Trigger decision

    ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool;
    ToolHandle<Trig::IMatchingTool> m_matchtool;
    
    //Do by hand
    double dR(double eta1,double phi1, double eta2, double phi2); //dR calculation by hand
    bool L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match=false); //By hand execution of L1_EM15 matching
    bool L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match=false);  //By hand execution of L1_EM15 matching
    bool L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching
    bool L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching

    std::string Set_Name_Shower_Shape(int j, int k, int l, int m);

    std::vector<std::string> m_MinBias_trigList = {
    "HLT_mb_sptrk_pc_L1ZDC_A_C_VTE50",
    "HLT_noalg_L1TE600p0ETA49",
    "HLT_noalg_L1TE50_VTE600p0ETA49"};
    //Min Bias TrigList

    int nof_MinBias_triggers = int(m_MinBias_trigList.size()*1.0); //L1 Reference Trigger Size
    
    
    //Likelihood Tool Selectors
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //grl tool
    AsgElectronLikelihoodTool *m_electronLooseLHSelector; 
    AsgElectronLikelihoodTool *m_electronMediumLHSelector; 
    AsgElectronLikelihoodTool *m_electronTightLHSelector; 
    AsgPhotonIsEMSelector *m_photonTightIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonLooseIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonMediumIsEMSelector; 

    TH1D *Electron_Dists[4][2][3];
    TH1D *Photon_Dists[4][2][3];
    //First []: Type of offline object
    //Second []: pt, eta of offline object
    //Third []: Eta Covergae. |eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

    //SumFcal_Et Dist for centrality selection
    TH1D *Sum_Fcal_Et_Dist;
    
    

    
    
    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    void Init_Hists();
    void Save_Hists(std::string);
    void SetHists(TH1*,const std::string &,const std::string &);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    
    //File Handle
    std::ofstream ns;
  };


#endif