#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackingPrimitives.h" //Particle Hypothesis
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>


#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

using namespace std;

  MyxAODAnalysis :: MyxAODAnalysis (const std::string &name,
                                    ISvcLocator *pSvcLocator)
      : EL::AnaAlgorithm (name, pSvcLocator)
      ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
      ,m_matchtool(this,"Trig::MatchingTool")
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    ANA_MSG_INFO ("Inside  Constructor.");

    declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
                    "Minimum electron pT (in MeV)" );
    declareProperty( "SampleName", m_sampleName = "Unknown",
                    "Descriptive name for the processed sample" );
    declareProperty( "JetPtCut", m_JetPtCut = 20.0,
                    "Minimum Jet pT (in MeV)" );
    
    //For Trigger Tools
    declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
    // declareProperty("triggers", m_trigList, "trigger selection list");

    //Matching
    declareProperty("MatchingTool", m_matchtool);
    

  }

  void MyxAODAnalysis::SetHists(TH1* My_hist,
      const std::string &xaxistitle="None",const std::string &yaxistitle="None"){

        //Here I just set some basic properties of the histogram that I will define.

        My_hist->GetXaxis()->SetTitle(xaxistitle.c_str());
        My_hist->GetYaxis()->SetTitle(yaxistitle.c_str());
      }

  StatusCode MyxAODAnalysis :: initialize ()
  {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_INFO ("In Initialize.");
    //Open File for checking purposes
    ns.open("CheckFile_New.txt");
    ns<<".....................Begin....................."<<endl;

    
    //Define the histograms
    Init_Hists();

    //Check if the decision tool is working properly
    ANA_CHECK(trigDecisionTool.retrieve());
    ANA_CHECK(m_matchtool.retrieve());
    
    // GRL. Good Run List Tool
    m_grl.setTypeAndName("GoodRunsListSelectionTool/myGRLTool");

    //Offline Electron Selector Tools (from Qipeng)
    std::string confDir = "ElectronPhotonSelectorTools/offline/mc23_20230728_HI/";
    m_electronLooseLHSelector = new AsgElectronLikelihoodTool ("LooseLH");;
    //ANA_CHECK(m_electronLooseLHSelector->setProperty("WorkingPoint", "LooseLHElectron"));
    ANA_CHECK(m_electronLooseLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronLooseLHSelector->initialize());

    m_electronMediumLHSelector = new AsgElectronLikelihoodTool ("MediumLH");;
    //ANA_CHECK(m_electronMediumLHSelector->setProperty("WorkingPoint", "MediumLHElectron"));
    ANA_CHECK(m_electronMediumLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodMediumOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronMediumLHSelector->initialize());

    m_electronTightLHSelector = new AsgElectronLikelihoodTool ("TightLH");;
    //ANA_CHECK(m_electronTightLHSelector->setProperty("WorkingPoint", "TightLHElectron"));
    ANA_CHECK(m_electronTightLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodTightOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronTightLHSelector->initialize());

    //Offline Photon Tool Selectors (from Qipeng)
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    ANA_CHECK(m_photonTightIsEMSelector->setProperty("WorkingPoint", "TightPhoton"));
    ANA_CHECK(m_photonTightIsEMSelector->initialize());

    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
    ANA_CHECK(m_photonLooseIsEMSelector->setProperty("WorkingPoint", "LoosePhoton"));
    ANA_CHECK(m_photonLooseIsEMSelector->initialize());

    m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonMediumIsEMSelector" );
    ANA_CHECK(m_photonMediumIsEMSelector->setProperty("WorkingPoint", "MediumPhoton"));
    ANA_CHECK(m_photonMediumIsEMSelector->initialize());


    return StatusCode::SUCCESS;
  }

  std::string MyxAODAnalysis::Set_Name_Shower_Shape(int j, int k, int l, int m){
    string Ghost_name;
    
    if(j==0) Ghost_name+="_Rhad1";
    if(j==1) Ghost_name+="_Rhad";
    if(j==2) Ghost_name+="_Reta";
    if(j==3) Ghost_name+="_Rphi";
    if(j==4) Ghost_name+="_weta1";
    if(j==5) Ghost_name+="_weta2";
    if(j==6) Ghost_name+="_wtots1";
    if(j==7) Ghost_name+="_f1";
    if(j==8) Ghost_name+="_f3";
    if(j==9) Ghost_name+="_fracs1";
    if(j==10) Ghost_name+="_DeltaE";
    if(j==11) Ghost_name+="_Eratio";
    if(j==12) Ghost_name+="_e277";

    if(k==0) Ghost_name  += "_All";
    if(k==1) Ghost_name  += "_Loose";
    if(k==2) Ghost_name  += "_Medium";
    if(k==3) Ghost_name  += "_Tight";
    
    if(l==0) Ghost_name  += "_All_Eta";
    if(l==1) Ghost_name  += "_Eta_Barrel";
    if(l==2) Ghost_name  += "_Eta_End_Caps";

    if(m==0) Ghost_name  += "_All_pt";
    if(m==1) Ghost_name  += "_pt_geq_20GeV";

    return Ghost_name;

  }

  void MyxAODAnalysis::Init_Hists(){
    std::string Ghost_name;
    double pi = TMath::Pi();
    char name[500];
    
    for(int i=0;i<4;i++){
      for(int j=0;j<2;j++){
        for(int k=0;k<3;k++){
          Ghost_name =  "All_Trigs";

          if(i==0) Ghost_name += "_All_Off";
          if(i==1) Ghost_name += "_Loose_LH_Off";
          if(i==2) Ghost_name += "_Medium_LH_Off";
          if(i==3) Ghost_name += "_Tight_LH_Off";

          if(j==0) Ghost_name += "_Pt";
          if(j==1) Ghost_name += "_Eta";

          if(k==0) Ghost_name +="_Eta_Barrel";
          if(k==1) Ghost_name +="_Eta_EndCap";
          if(k==2) Ghost_name +="_Eta_All";

          if(j==0){
              sprintf(name,"Electron_%s",Ghost_name.c_str());
              Electron_Dists[i][j][k] = new TH1D(name,name,500,-0.5,499.5);

              sprintf(name,"Photon_%s",Ghost_name.c_str());
              Photon_Dists[i][j][k] = new TH1D(name,name,500,-0.5,499.5);
            }
            
            if(j==1){
              sprintf(name,"Electron_%s",Ghost_name.c_str());
              Electron_Dists[i][j][k] = new TH1D(name,name,1200,-6,6);

              sprintf(name,"Photon_%s",Ghost_name.c_str());
              Photon_Dists[i][j][k] = new TH1D(name,name,1200,-6,6);
            }

        }
      }
    }
    
    sprintf(name,"Sum_Fcal_Et_Dist");
    Sum_Fcal_Et_Dist = new TH1D(name,name,7000,-0.5,6999.5);
    
    ANA_MSG_INFO("Initialization Of Histograms completed.");
  }
  

  //By Hand Execution of dR calculation
  double MyxAODAnalysis::dR(double eta1,double phi1, double eta2, double phi2){
    double deta = fabs(eta1 - eta2);
    double dphi = 0;

    if(fabs(phi1 - phi2) < TMath::Pi()){
      dphi = fabs(phi1 - phi2);
    }
    else{
      dphi = 2*TMath::Pi() - fabs(phi1 - phi2);
    }
    
    return sqrt(deta*deta + dphi*dphi);

  }
  
  //By hand execution of L1_eEM15 matching for Electron
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()*1e-3<13){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_eEM15 matching for Photon
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()*1e-3 < 13){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_EM12 matching for Electron
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  //By hand execution of L1_EM12 matching for Photon
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  StatusCode MyxAODAnalysis :: execute ()
  {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    //Main Code Here
    //Retrieve the eventInfo object from the event store
    const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

    ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    
    // if (!m_grl->passRunLB(*eventInfo)) { //Check if the run passes the good run list tool
    //   ANA_MSG_DEBUG ("drop event: GRL");
    //   ns<<"Dropping Event:GRL->"<<eventInfo->runNumber()<<" : "<<eventInfo->eventNumber();
    //   return StatusCode::SUCCESS;
    // }

    const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
    ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get photon pointer

    const xAOD::PhotonContainer *Online_photon_info = nullptr; //Define online photon pointer
    ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_egamma_Photons"));  //Get photon pointer

    const xAOD::ElectronContainer *electron_info = nullptr; //Define electron pointer
    ANA_CHECK (evtStore()->retrieve (electron_info, "Electrons"));  //Get electron pointer

    const xAOD::ElectronContainer *Online_electron_info = nullptr; //Define Online electron pointer
    ANA_CHECK (evtStore()->retrieve (Online_electron_info, "HLT_egamma_Electrons"));  //Get electron pointer


    std::vector<const xAOD::IParticle*> myParticles;
    
    
    //Making Efficiency Plots
    
    //Compute fcalEt of the Event
    const xAOD::HIEventShapeContainer *hies = 0;  
    ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

    double m_sumEt_A = 0;
    double m_sumEt_C = 0;
    for (const xAOD::HIEventShape *ES : *hies) {
      double et = ES->et()*1e-3;
      double eta = ES->etaMin();

      int layer = ES->layer();
      if (layer != 21 && layer != 22 && layer != 23) continue;
      if (eta > 0) {
        m_sumEt_A += et;
      } else {
        m_sumEt_C += et;
      }
    }
    double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
    // if(m_sumEt>2000) ANA_MSG_INFO("Fcal Sum_Et = "<<m_sumEt);

    
    

    const xAOD::EmTauRoIContainer *m_EMTauRoIs = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_EMTauRoIs, "LVL1EmTauRoIs"));  //Get the event info pointer  

    const xAOD::eFexEMRoIContainer *m_eEM15 = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_eEM15, "L1_eEMRoI"));  //Get the event info pointer

    bool pass[3];
    ns<<endl;
    for(int MB_trig=0;MB_trig<3;MB_trig++){
      string trigname = m_MinBias_trigList.at(MB_trig);
      pass[MB_trig] = trigDecisionTool->isPassed(trigname);
      ns<<"MinBias Trig: "<<trigname<<", Passed? "<<pass[MB_trig]<<endl;
    }

    if(pass[0]==true || pass[1]==true || pass[2]==true){

      bool off_elec_type[4]; //Category of Offline Electron. All,Loose,Medium,Tight
      if(electron_info->size()>0){
        for(const auto eg : *electron_info){  //Loop Over Offline Electrons
          
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();

          off_elec_type[0] = true; 
          off_elec_type[1] = bool(m_electronLooseLHSelector->accept(eg));
          off_elec_type[2] = bool(m_electronMediumLHSelector->accept(eg));
          off_elec_type[3] = bool(m_electronTightLHSelector->accept(eg));

          for(int electron_type=0;electron_type<4;electron_type++){ 
            if(!off_elec_type[electron_type]) continue;
              for(int j=0;j<2;j++){
                double Hist_xval;
                if(j==0) Hist_xval = eg_pt;
                if(j==1) Hist_xval = eg_eta;

                if(fabs(eg_eta)<1.37){//Barrel
                  Electron_Dists[electron_type][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Electron_Dists[electron_type][j][1]->Fill(Hist_xval);
                } 
                  
                Electron_Dists[electron_type][j][2]->Fill(Hist_xval);

              }//xaxis loop ends
          }//electron type loop ends

        }//Offline electron loop ends
      }//electron size Condition ends
      else{
        // ns<<"No Electrons in the event."
      }

      bool off_photon_type[4]; //Category of Offline Photon. All,Loose,Medium,Tight
      if(photon_info->size()>0){
        for(const auto eg : *photon_info){  //Loop Over Offline Photons
          
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();

          off_photon_type[0] = true;
          off_photon_type[1] = bool(m_photonLooseIsEMSelector->accept(eg));
          off_photon_type[2] = bool(m_photonMediumIsEMSelector->accept(eg));
          off_photon_type[3] = bool(m_photonTightIsEMSelector->accept(eg));

          for(int photon_type=0;photon_type<4;photon_type++){ 
            if(!off_photon_type[photon_type]) continue;
              for(int j=0;j<2;j++){
                double Hist_xval;
                if(j==0) Hist_xval = eg_pt;
                if(j==1) Hist_xval = eg_eta;

                if(fabs(eg_eta)<1.37){//Barrel
                  Photon_Dists[photon_type][j][0]->Fill(Hist_xval);
                } 
              
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52){//End-Caps
                  Photon_Dists[photon_type][j][1]->Fill(Hist_xval);
                } 
                  
                Photon_Dists[photon_type][j][2]->Fill(Hist_xval);

              }//xaxis loop ends
          }//Photon type loop ends

        }//Offline Photon loop ends
      }//Photon size Condition ends
      else{
        // ns<<"No Photona in the event."
      }
      Sum_Fcal_Et_Dist->Fill(m_sumEt);
    }//MB Trigger Pass Check Ends

    

    
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";

    return StatusCode::SUCCESS;
  }


  void MyxAODAnalysis::Save_Hists(std::string outfilename){
    std::string Ghost_name;
    char name[500];

    TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");

 
    for(int i=0;i<4;i++){
      for(int j=0;j<2;j++){
        for(int k=0;k<3;k++){
          Ghost_name =  "All_Trigs";

          if(i==0) Ghost_name += "_All_Off";
          if(i==1) Ghost_name += "_Loose_LH_Off";
          if(i==2) Ghost_name += "_Medium_LH_Off";
          if(i==3) Ghost_name += "_Tight_LH_Off";

          if(j==0) Ghost_name += "_Pt";
          if(j==1) Ghost_name += "_Eta";

          if(k==0) Ghost_name +="_Eta_Barrel";
          if(k==1) Ghost_name +="_Eta_EndCap";
          if(k==2) Ghost_name +="_Eta_All";

          
          sprintf(name,"Electron_%s",Ghost_name.c_str());
          Outfile->WriteObject(Electron_Dists[i][j][k],name);

          sprintf(name,"Photon_%s",Ghost_name.c_str());
          Outfile->WriteObject(Photon_Dists[i][j][k],name);

        }
      }
    }
    
    sprintf(name,"Sum_Fcal_Et_Dist");
    Outfile->WriteObject(Sum_Fcal_Et_Dist,name);

    Outfile->Close();
    ANA_MSG_INFO("All Histograms have been saved.");
  }


  StatusCode MyxAODAnalysis :: finalize ()
  {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.

    ANA_MSG_INFO ("In Finalize.");

    // std::string outfilename = "/afs/cern.ch/user/a/adimri/Egamma_Trigger/Output_New.root";
    std::string outfilename = "Output_New.root";
    Save_Hists(outfilename);

    ns<<endl<<"....................End....................."<<endl;
    ns.close();
    return StatusCode::SUCCESS;
  }
