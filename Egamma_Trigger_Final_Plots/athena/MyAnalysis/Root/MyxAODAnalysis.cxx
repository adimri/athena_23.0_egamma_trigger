#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackingPrimitives.h" //Particle Hypothesis
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>


#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

using namespace std;

  MyxAODAnalysis :: MyxAODAnalysis (const std::string &name,
                                    ISvcLocator *pSvcLocator)
      : EL::AnaAlgorithm (name, pSvcLocator)
      ,m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool")
      ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
      ,m_matchtool(this,"Trig::MatchingTool")
  {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.

    ANA_MSG_INFO ("Inside  Constructor.");

    declareProperty( "ElectronPtCut", m_electronPtCut = 25000.0,
                    "Minimum electron pT (in MeV)" );
    declareProperty( "SampleName", m_sampleName = "Unknown",
                    "Descriptive name for the processed sample" );
    declareProperty( "JetPtCut", m_JetPtCut = 20.0,
                    "Minimum Jet pT (in MeV)" );
    
    //For Trigger Tools
    declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
    declareProperty("Papa_TrigDecisionTool", m_trigDecTool, "The trigger decision tool");
    // declareProperty("triggers", m_trigList, "trigger selection list");

    //Matching
    declareProperty("MatchingTool", m_matchtool);
    

  }

  void MyxAODAnalysis::SetHists(TH1* My_hist,
      const std::string &xaxistitle="None",const std::string &yaxistitle="None"){

        //Here I just set some basic properties of the histogram that I will define.

        My_hist->GetXaxis()->SetTitle(xaxistitle.c_str());
        My_hist->GetYaxis()->SetTitle(yaxistitle.c_str());
      }

  StatusCode MyxAODAnalysis :: initialize ()
  {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    ANA_MSG_INFO ("In Initialize.");
    //Open File for checking purposes
    ns.open("CheckFile_New.txt");
    ns<<".....................Begin....................."<<endl;

    limit_nof_triggers();

    //Define the histograms
    Init_Hists();

    //Check if the decision tool is working properly
    ANA_CHECK(trigDecisionTool.retrieve());
    ANA_CHECK(m_trigDecTool.retrieve());
    ANA_CHECK(m_matchtool.retrieve());
    
    // GRL. Good Run List Tool
    m_grl.setTypeAndName("GoodRunsListSelectionTool/myGRLTool");

    //Offline Electron Selector Tools (from Qipeng)
    std::string confDir = "ElectronPhotonSelectorTools/offline/mc23_20230728_HI/";
    m_electronLooseLHSelector = new AsgElectronLikelihoodTool ("LooseLH");;
    //ANA_CHECK(m_electronLooseLHSelector->setProperty("WorkingPoint", "LooseLHElectron"));
    ANA_CHECK(m_electronLooseLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodLooseOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronLooseLHSelector->initialize());

    m_electronMediumLHSelector = new AsgElectronLikelihoodTool ("MediumLH");;
    //ANA_CHECK(m_electronMediumLHSelector->setProperty("WorkingPoint", "MediumLHElectron"));
    ANA_CHECK(m_electronMediumLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodMediumOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronMediumLHSelector->initialize());

    m_electronTightLHSelector = new AsgElectronLikelihoodTool ("TightLH");;
    //ANA_CHECK(m_electronTightLHSelector->setProperty("WorkingPoint", "TightLHElectron"));
    ANA_CHECK(m_electronTightLHSelector->setProperty("ConfigFile",confDir+"ElectronLikelihoodTightOfflineConfig2023_HI_Smooth.conf"));
    ANA_CHECK(m_electronTightLHSelector->initialize());

    //Offline Photon Tool Selectors (from Qipeng)
    m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
    ANA_CHECK(m_photonTightIsEMSelector->setProperty("WorkingPoint", "TightPhoton"));
    ANA_CHECK(m_photonTightIsEMSelector->initialize());

    m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
    ANA_CHECK(m_photonLooseIsEMSelector->setProperty("WorkingPoint", "LoosePhoton"));
    ANA_CHECK(m_photonLooseIsEMSelector->initialize());

    m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonMediumIsEMSelector" );
    ANA_CHECK(m_photonMediumIsEMSelector->setProperty("WorkingPoint", "MediumPhoton"));
    ANA_CHECK(m_photonMediumIsEMSelector->initialize());


    return StatusCode::SUCCESS;
  }

  void MyxAODAnalysis::Init_Hists(){
    std::string Ghost_name;
    // double pi = TMath::Pi();
    char name[500];
    /*
    //For Efficiency of L1 wrt MinBias
    for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<3;i++){
          for(int j=0;j<6;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_L1_trigList.at(trig_count)+"_" + m_L1_Ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-1";
              if(j==1) Ghost_name += "FcalEt_1-2";
              if(j==2) Ghost_name += "FcalEt_2-3";
              if(j==3) Ghost_name += "FcalEt_3-4";
              if(j==4) Ghost_name += "FcalEt_4-6";
              if(j==5) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
              Photon_Evt_L1[trig_count][ref_trig_count][i][j][k] = new TH1D(name,name,500,-0.5,499.5);

              //For saving the denominator
              sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
              Photon_Total_L1[trig_count][ref_trig_count][i][j][k] = new TH1D(name,name,500,-0.5,499.5);
            }
          }
        }
      }
    }
    */
    //For Efficiency of EtCut Triggers wrt corresponding L1 Triggers
    for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Trigs_Photons;EtCut_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx)+"_" + m_Ref_trigList_EtCut_Photons.at(EtCut_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Photon_EtCut_Evt_%s",Ghost_name.c_str());
            Photon_Evt_EtCut[EtCut_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

            //For saving the denominator
            sprintf(name,"Photon_EtCut_Total_%s",Ghost_name.c_str());
            Photon_Total_EtCut[EtCut_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

          }
        }
      }   
    }
    
    
    //For Efficiency of Id Triggers wrt correspnding EtCut Triggers
    for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Trigs_Photons;Id_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
            Photon_Evt_Id[Id_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

            //For saving the denominator
            sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
            Photon_Total_Id[Id_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

          }
        }
      }   
    }

    //For EtaPhiMaps of Id Triggers
    for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Trigs_Photons;Id_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"EtaPhiMap_Photon_Id_Evt_%s",Ghost_name.c_str());
            EtaPhiMap_Photon_Evt_Id[Id_trig_idx][i][j][k] = new TH2D(name,name,300,-3,3,320,-3.2,3.2);

          }
        }
      }   
    }
    
    //For 2D Correlation Plots between Offline And Online Photons
    for(int Corel_trig_idx=0; Corel_trig_idx<nof_Corel_Trigs_Photons;Corel_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Corel_Photons.at(Corel_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Corel_Pt_Photon_Evt_%s",Ghost_name.c_str());
            Corel_Photon_Off_On_Pt[Corel_trig_idx][i][j][k] = new TH2D(name,name,150,-0.5,149.5,150,-0.5,149.5);

          }
        }
      }   
    }
    

    ANA_MSG_INFO("Initialization Of Histograms completed.");
  }
  

  //By Hand Execution of dR calculation
  double MyxAODAnalysis::dR(double eta1,double phi1, double eta2, double phi2){
    double deta = fabs(eta1 - eta2);
    double dphi = 0;

    if(fabs(phi1 - phi2) < TMath::Pi()){
      dphi = fabs(phi1 - phi2);
    }
    else{
      dphi = 2*TMath::Pi() - fabs(phi1 - phi2);
    }
    
    return sqrt(deta*deta + dphi*dphi);

  }
  
  //By hand execution of L1_eEM15 matching for Electron
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()*1e-3<13){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_eEM15 matching for Photon
  bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM15->et()*1e-3 < 13){ //eEM15 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM15->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM15->eta();
    float phi = eEM15->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_EM12 matching for Electron
  bool MyxAODAnalysis::L1_EM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  //By hand execution of L1_EM12 matching for Photon
  bool MyxAODAnalysis::L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  //By hand execution of L1_eEM12 matching for Electron
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM12, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM12->et()*1e-3<10){ //eEM12 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM12->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM12->eta();
    float phi = eEM12->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }

  //By hand execution of L1_eEM12 matching for Photon
  bool MyxAODAnalysis::L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM12, bool match){
    //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
    //That is why no such condition is required and no such function exists.

    if(eEM12->et()*1e-3 < 10){ //eEM12 objects should pass the threshold
      return(false);
    }

    //eEMRoI does not have threshold names.
    //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
    if(!(eEM12->isTOB())) {
      ns<<"Not a trigger object. (TOB). Continue."<<endl;
      return (false);
    }
    //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

    //Compute dR
    float eta = eEM12->eta();
    float phi = eEM12->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }  

    return (match);
  }


  //By hand execution of L1_EM10 matching for Electron
  bool MyxAODAnalysis::L1_EM10_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM10") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  //By hand execution of L1_EM10 matching for Photon
  bool MyxAODAnalysis::L1_EM10_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match){

    if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
        return (false);
    } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

    const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
    
    // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
    bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM10") != std::end(thresholdNames));
    if(!EM12ThresholdPassed){
      return (false);
    }

    //Compute dR
    float eta = EMTauRoI->eta();
    float phi = EMTauRoI->phi();
    const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

    if (dR < 0.1){  //If dR passes the condition the matching has occured. 
      match = true; //Change Value to true
    }

    return (match);
  }

  StatusCode MyxAODAnalysis :: execute ()
  {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    //Main Code Here
    //Retrieve the eventInfo object from the event store
    const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
    ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

    ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
    
    if (!m_grl->passRunLB(*eventInfo)) { //Check if the run passes the good run list tool
      ANA_MSG_DEBUG ("drop event: GRL");
      ns<<"Dropping Event:GRL->"<<eventInfo->runNumber()<<" : "<<eventInfo->eventNumber()<<endl;
      return StatusCode::SUCCESS;
    }

    const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
    ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get photon pointer

    const xAOD::PhotonContainer *Online_photon_info = nullptr; //Define online photon pointer
    ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_egamma_Photons"));  //Get photon pointer

    const xAOD::ElectronContainer *electron_info = nullptr; //Define electron pointer
    ANA_CHECK (evtStore()->retrieve (electron_info, "Electrons"));  //Get electron pointer

    const xAOD::ElectronContainer *Online_electron_info = nullptr; //Define Online electron pointer
    ANA_CHECK (evtStore()->retrieve (Online_electron_info, "HLT_egamma_Electrons"));  //Get electron pointer


    std::vector<const xAOD::IParticle*> myParticles;
    
    
    //Making Efficiency Plots
    
    //Compute fcalEt of the Event
    const xAOD::HIEventShapeContainer *hies = 0;  
    ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

    double m_sumEt_A = 0;
    double m_sumEt_C = 0;
    for (const xAOD::HIEventShape *ES : *hies) {
      double et = ES->et()*1e-3;
      double eta = ES->etaMin();

      int layer = ES->layer();
      if (layer != 21 && layer != 22 && layer != 23) continue;
      if (eta > 0) {
        m_sumEt_A += et;
      } else {
        m_sumEt_C += et;
      }
    }
    double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
    // if(m_sumEt>2000) ANA_MSG_INFO("Fcal Sum_Et = "<<m_sumEt);
    

    const xAOD::EmTauRoIContainer *m_EMTauRoIs = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_EMTauRoIs, "LVL1EmTauRoIs"));  //Get the event info pointer  

    const xAOD::eFexEMRoIContainer *m_eEM15 = nullptr; //Define the L1 particle info pointer
    ANA_CHECK (evtStore()->retrieve(m_eEM15, "L1_eEMRoI"));  //Get the event info pointer

    const int max_photon_types =3;
    /*
    //Code Below Calculates Efficiency for L1 triggers wrt MinBias Triggers
    for(int L1_ref_trig = 0; L1_ref_trig<nof_L1_ref_triggers;L1_ref_trig++){
      string ref_trigname = m_L1_Ref_trigList.at(L1_ref_trig);
      bool ref_trigDecision = trigDecisionTool->isPassed(ref_trigname);

      if(ref_trigDecision){
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for   name: "<<ref_trigname;
        ns<<endl;
        for(int L1_trig=0;L1_trig<nof_L1_triggers;L1_trig++){
          string trigname = m_L1_trigList.at(L1_trig);

          //Photon Work Begins
          bool off_photon_type[max_photon_types]; //Category of Offline Photons. All,Loose,Medium,Tight
          if(photon_info->size()>0){
            for(const auto eg : *photon_info){  //Loop Over Offline Photons
              bool pass = false;
              
              if(trigname=="L1_EM10"){ //By Hand Matching of L1_EM12
                for (const auto EMTauRoI: *m_EMTauRoIs){ 
                  pass = L1_EM10_TrigMatch(eg,EMTauRoI,pass);
                  if(pass) {
                    // ns<<"EM10 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                    break;       
                  }
                }
              }

              if(trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
                for (const auto EMTauRoI: *m_EMTauRoIs){ 
                  pass = L1_EM12_TrigMatch(eg,EMTauRoI,pass);
                  if(pass) {
                    // ns<<"EM12 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                    break;       
                  }
                }
              }

              if(trigname=="L1_eEM12"){  //By Hand Matching of L1_eEM15
                for (const xAOD::eFexEMRoI *eEM12: *m_eEM15){ 
                  pass = L1_eEM12_TrigMatch(eg,eEM12,pass);
                  if(pass) {
                    // ns<<"EM12 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<eEM12->eta()<<", "<<eEM12->phi()<<")"<<endl;
                    break;
                  }
                }
              } 

              if(trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
                for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                  pass = L1_eEM15_TrigMatch(eg,eEM15,pass);
                  if(pass) {
                    // ns<<"EM15 Photon Matched? "<<pass<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                    //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                    break;
                  }
                }
              } 

              // double eg_mass = eg->m()*0.001; //GeV
              double eg_pt = eg->pt()*0.001;//GeV
              // double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
              double eg_eta = eg->eta();
              // double eg_phi = eg->phi();
              
              off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));

              if(pass){
                // ns<<"Photon matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<". Matches Trigger: "<<trigname<<" and MinBias Reference Trigger: "<<ref_trigname<<endl;

                for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;

                  int eta_idx=-1;
                  int fcal_idx=-1;

                  //Fix eta_idx
                  if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                  if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                  //Fix fcalEt idx
                  if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                  if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                  if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                  if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                  if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                  if(eta_idx>-1 && fcal_idx>-1) {
                    Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][eta_idx]->Fill(eg_pt); //Particular fcal + Particular Eta
                    Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                    Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta                      
                  }
                  if(fcal_idx==-1 && eta_idx>-1)  Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                  if(fcal_idx>-1 && eta_idx==-1)  Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                  Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 
                  
                }//photon type loop ends
              }//pass condition ends

              //Histograms showing all photons that passes the MinBias reference trigger
              for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                if(!off_photon_type[photon_type]) continue;

                int eta_idx=-1;
                int fcal_idx=-1;

                //Fix eta_idx
                if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                //Fix fcalEt idx
                if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                if(eta_idx>-1 && fcal_idx>-1) {
                  Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][eta_idx]->Fill(eg_pt); //Particular fcal + Particular Eta
                  Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                  Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta                   
                }
                if(fcal_idx==-1 && eta_idx>-1)  Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                if(fcal_idx>-1 && eta_idx==-1)  Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                Photon_Total_L1[L1_trig][L1_ref_trig][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 
                
              }//photon type loop ends for reference filling
                
            }// Offline Photon Loop Ends 
          }//Photon Size>0 condition ends
          else{
            // ns<<"No photons in this event. "<<endl;
          }

          //Photon Work Ends
        }//L1 trig loop ends
      }//TrigDecision condition ends
      else{
          // ns<<"Event(Electron+Photon) Fails the MinBias trigger: "<<ref_trigname<<endl;
        }
    }//L1_ref trig loop ends
    //Work finished for calculating Efficiency for L1 triggers wrt MinBias Triggers
    */

    //Calculating Efficiencies for EtCut Triggers wrt to L1 triggers
    for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Trigs_Photons;EtCut_trig_idx++){
      string ref_trigname = m_Ref_trigList_EtCut_Photons.at(EtCut_trig_idx); //Get Reference Trigger Name
      string trigname = m_trigList_EtCut_Photons.at(EtCut_trig_idx);  //Get The Trigger Name
      bool trigDecision = trigDecisionTool->isPassed(ref_trigname);//Event passed the relevant trigger?

      if(trigDecision){ 
        ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        ns<<endl;
        
        //Work for Photon Begins
        bool off_photon_type[max_photon_types];
        
        if(photon_info->size()>0){
          for(const auto eg : *photon_info){  //Loop Over Offline Photons
            // int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one
            bool pass_ref = false; //Check if the same offline object matches with reference trigger
            
            //Initializing pass ref
            if(ref_trigname=="L1_EM10"){ //By Hand Matching of L1_EM12
              for (const auto EMTauRoI: *m_EMTauRoIs){ 
                pass_ref = L1_EM10_TrigMatch(eg,EMTauRoI,pass_ref);
                if(pass_ref) {
                  // ns<<"EM10 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                  break;       
                }
              }
            }
            else if(ref_trigname=="L1_EM12"){ //By Hand Matching of L1_EM12
              for (const auto EMTauRoI: *m_EMTauRoIs){ 
                pass_ref = L1_EM12_TrigMatch(eg,EMTauRoI,pass_ref);
                if(pass_ref) {
                  // ns<<"EM12 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<EMTauRoI->eta()<<", "<<EMTauRoI->phi()<<")."<<endl;
                  break;       
                }
              }
            }
            else if(ref_trigname=="L1_eEM12"){  //By Hand Matching of L1_eEM15
              for (const xAOD::eFexEMRoI *eEM12: *m_eEM15){ 
                pass_ref = L1_eEM12_TrigMatch(eg,eEM12,pass_ref);
                if(pass_ref) {
                  // ns<<"eEM12 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<eEM12->eta()<<", "<<eEM12->phi()<<")"<<endl;
                  break;
                }
              }
            }
            else if(ref_trigname=="L1_eEM15"){  //By Hand Matching of L1_eEM15
              for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
                pass_ref = L1_eEM15_TrigMatch(eg,eEM15,pass_ref);
                if(pass_ref) {
                  // ns<<"eEM15 Photon Matched? "<<pass_ref<<". Offline (Eta,Phi) = ("<<eg->eta()<<", "<<eg->phi()<<"). Online (Eta,Phi) = ("
                  //         <<eEM15->eta()<<", "<<eEM15->phi()<<")"<<endl;
                  break;
                }
              }
            }
            else {  //Matching the remaining reference triggers using matchtool. This function cannot match L1 triggers for some reason.
              pass_ref=m_matchtool->match({eg},ref_trigname);
            }

            //Photon Properties
            // double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            // double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            // double eg_phi = eg->phi();
            
            off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            
            // ns<<"Off_Photon_Type = ("<<off_photon_type[0]<<","<<off_photon_type[1]<<","<<off_photon_type[2]<<")"<<endl;
            // ns<<"(Pass_Ref,Pass) = ("<<pass_ref<<","<<pass<<")"<<endl;

            if(pass_ref){
              if(pass){ //Histograms for photons that pass
                ns<<"Photon "<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<endl;
                for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;
                  
                  int eta_idx=-1;
                  int fcal_idx=-1;

                  //Fix eta_idx
                  if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                  if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                  //Fix fcalEt idx
                  if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                  if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                  if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                  if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                  if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                  if(eta_idx>-1 && fcal_idx>-1) {
                    Photon_Evt_EtCut[EtCut_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(eg_pt); //Particular fcal + Particular Eta
                    Photon_Evt_EtCut[EtCut_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta                  
                    Photon_Evt_EtCut[EtCut_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta                  
                  }
                  if(fcal_idx==-1 && eta_idx>-1)  Photon_Evt_EtCut[EtCut_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                  if(fcal_idx>-1 && eta_idx==-1)  Photon_Evt_EtCut[EtCut_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                  Photon_Evt_EtCut[EtCut_trig_idx][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 

                }//Photon type loop ends
              } //m_matchtool pass condition ends

              //Histograms showing all photons in the passes event
              for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                if(!off_photon_type[photon_type]) continue;
                
                int eta_idx=-1;
                int fcal_idx=-1;

                //Fix eta_idx
                if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                //Fix fcalEt idx
                if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                if(eta_idx>-1 && fcal_idx>-1) {
                  Photon_Total_EtCut[EtCut_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(eg_pt); //Particular fcal + Particular Eta
                  Photon_Total_EtCut[EtCut_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta                  
                  Photon_Total_EtCut[EtCut_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta
                }
                if(fcal_idx==-1 && eta_idx>-1)  Photon_Total_EtCut[EtCut_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                if(fcal_idx>-1 && eta_idx==-1)  Photon_Total_EtCut[EtCut_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                Photon_Total_EtCut[EtCut_trig_idx][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 

              }//Photon type loop ends for Only reference photons
            }//Pass_ref condtion ends
          }//Offline Photon Loop ends
        }//Offline Photon Size Condition
        else{
          // ns<<"This event has no offline photons."<<endl;
        }
        //Photon Work Ends
        
      }//TrigDecision condition.
      else{
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" fails the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        // ns<<endl;
      }
    }//EtCut_trig_idx loop ends
    //Calculating efficiencies for EtCut wrt L1 triggers
    
    
    //Calculating Efficiencies for Id Triggers wrt to EtCut triggers
    for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Trigs_Photons;Id_trig_idx++){
      string ref_trigname = m_Ref_trigList_Id_Photons.at(Id_trig_idx); //Get Reference Trigger Name
      string trigname = m_trigList_Id_Photons.at(Id_trig_idx);  //Get The Trigger Name
      bool trigDecision = trigDecisionTool->isPassed(ref_trigname);//Event passed the relevant trigger?

      if(trigDecision){ 
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" passes the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        ns<<endl;
        
        //Work for Photon Begins
        bool off_photon_type[max_photon_types];
        
        if(photon_info->size()>0){
          for(const auto eg : *photon_info){  //Loop Over Offline Photons
            // int idx=0;
            bool pass=m_matchtool->match({eg}, trigname); //Check if the offline object matches with any online one
            bool pass_ref = m_matchtool->match({eg},ref_trigname);; //Check if the same offline object matches with reference trigger

            //Photon Properties
            double eg_mass = eg->m()*0.001; //GeV
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_et = TMath::Sqrt(TMath::Power(eg_mass,2)+TMath::Power(eg_pt,2));//GeV
            double eg_eta = eg->eta();
            double eg_phi = eg->phi();
            
            off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            
            // ns<<"Off_Photon_Type = ("<<off_photon_type[0]<<","<<off_photon_type[1]<<","<<off_photon_type[2]<<")"<<endl;
            // ns<<"(Pass_Ref,Pass) = ("<<pass_ref<<","<<pass<<")"<<endl;

            if(pass_ref){
              if(pass){ //Histograms for photons that pass
                // ns<<"Photon "<<" matches! Pt: "<<eg_pt<<", eta: "<<eg_eta<<endl;
                for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                  if(!off_photon_type[photon_type]) continue;
                  
                  int eta_idx=-1;
                  int fcal_idx=-1;

                  //Fix eta_idx
                  if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                  if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                  //Fix fcalEt idx
                  if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                  if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                  if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                  if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                  if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                  if(eta_idx>-1 && fcal_idx>-1) {
                    Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(eg_pt);  //Particular fcal + Particular Eta
                    Photon_Evt_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt); //All fcal + Particular eta
                    Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta 
                  } 
                  if(fcal_idx==-1 && eta_idx>-1)  Photon_Evt_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                  if(fcal_idx>-1 && eta_idx==-1)  Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                  Photon_Evt_Id[Id_trig_idx][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 

                  //For EtaPhiMaps Of Passed Id Photons
                  if(eta_idx>-1 && fcal_idx>-1) {
                    EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(eg_eta,eg_phi); //Particular fcal + Particular Eta
                    EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_eta,eg_phi);  //All fcal + Particular eta
                    EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_eta,eg_phi); //Particular fcal + All eta  
                  } 
                  if(fcal_idx==-1 && eta_idx>-1)  EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_eta,eg_phi);  //All fcal + Particular eta
                  if(fcal_idx>-1 && eta_idx==-1)  EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_eta,eg_phi); //Particular fcal + All eta  

                  EtaPhiMap_Photon_Evt_Id[Id_trig_idx][photon_type][5][2]->Fill(eg_eta,eg_phi); //All fcalEt + All eta 

                }//Photon type loop ends
              } //m_matchtool pass condition ends

              //Histograms showing all photons in the passes event
              for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                if(!off_photon_type[photon_type]) continue;
                
                int eta_idx=-1;
                int fcal_idx=-1;

                //Fix eta_idx
                if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                //Fix fcalEt idx
                if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                if(eta_idx>-1 && fcal_idx>-1) {
                  Photon_Total_Id[Id_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(eg_pt); //Particular fcal + Particular Eta
                  Photon_Total_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                  Photon_Total_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta 
                }
                if(fcal_idx==-1 && eta_idx>-1)  Photon_Total_Id[Id_trig_idx][photon_type][5][eta_idx]->Fill(eg_pt);  //All fcal + Particular eta
                if(fcal_idx>-1 && eta_idx==-1)  Photon_Total_Id[Id_trig_idx][photon_type][fcal_idx][2]->Fill(eg_pt); //Particular fcal + All eta  

                Photon_Total_Id[Id_trig_idx][photon_type][5][2]->Fill(eg_pt); //All fcalEt + All eta 

              }//Photon type loop ends for Only reference photons
            }//Pass_ref condtion ends
          }//Offline Photon Loop ends
        }//Offline Photon Size Condition
        else{
          // ns<<"This event has no offline photons."<<endl;
        }
        //Photon Work Ends
        
      }//TrigDecision condition.
      else{
        // ns<<"Event #: "<<eventInfo->eventNumber()<<" fails the trigDecisionTool->isPassed(ref_trigname) condition for Reference Trigger name: "<<ref_trigname;
        // ns<<endl;
      }

    }//Id_trig_idx loop ends
    
    //Below lies the code to find the onlien object matching to offline object
    for(int Corel_trig_idx=0; Corel_trig_idx<nof_Corel_Trigs_Photons;Corel_trig_idx++){
      string trigname_2DCorel = m_trigList_Corel_Photons.at(Corel_trig_idx);
      
      //Work for photon begins
      bool off_photon_type[max_photon_types];
      if(photon_info->size()>0){
        for(const auto eg : *photon_info){  //Loop Over Offline Electrons
          bool pass=m_matchtool->match({eg}, trigname_2DCorel);
          
          double offline_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();
          // double eg_phi = eg->phi();

          off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
          off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
          off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));          
          
          if(pass){
            // ns<<"Photon Passes. pt = " << eg->pt() << " eta = " << eg->eta() << " phi = " << eg->phi()<<endl;
            
            //Lines Below check if the event passes the trig chain
            const std::string &chain = trigname_2DCorel;
            const Trig::ChainGroup *chainGroup = m_trigDecTool->getChainGroup(chain);
            for (const std::string &chainName : chainGroup->getListOfTriggers()){

              if (!m_trigDecTool->isPassed(chainName, false ? TrigDefs::Physics | TrigDefs::allowResurrectedDecision : TrigDefs::Physics)){
                // ns<<"Chain " << chainName << " did not pass."<<endl;
                continue;
              }

              // ns<<"Chain " << chainName << " passed"<<endl;

              //Lines Below find the corresonding online objects and check for matching
              using IPartLinkInfo_t = TrigCompositeUtils::LinkInfo<xAOD::IParticleContainer>;
              using VecLinkInfo_t = std::vector<IPartLinkInfo_t>;
              VecLinkInfo_t features = m_trigDecTool->features<xAOD::IParticleContainer>(chainName);
              for (IPartLinkInfo_t &linkInfo : features){
                if (!linkInfo.isValid()){
                  ATH_MSG_ERROR("Chain " << chainName << " has invalid link info!");
                  // throw std::runtime_error("Bad link info");
                  continue;
                }
                const xAOD::IParticle *online = *(linkInfo.link);
                // ns<<"Online obj pt = " << online->pt() << " eta = " << online->eta() << " phi = " << online->phi()<<endl;

                const float dR = xAOD::P4Helpers::deltaR(*eg, online->eta(), online->phi(), false);
                double online_pt = online->pt()*0.001;//GeV;

                //Fill Up the correlation plots if matching occurs (dR<=0.1).
                if(dR<=0.1){
                  for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                    if(!off_photon_type[photon_type]) continue;
                    
                    int eta_idx=-1;
                    int fcal_idx=-1;

                    //Fix eta_idx
                    if(fabs(eg_eta)<1.37) eta_idx = 0; //Barrel 
                    if(fabs(eg_eta)<2.37 && fabs(eg_eta)>1.52)  eta_idx = 1; //End-Caps

                    //Fix fcalEt idx
                    if(m_sumEt>=0 && m_sumEt<=1000) fcal_idx=0;
                    if(m_sumEt>1000 && m_sumEt<=2000) fcal_idx=1;
                    if(m_sumEt>2000 && m_sumEt<=3000) fcal_idx=2;
                    if(m_sumEt>3000 && m_sumEt<=4000) fcal_idx=3;
                    if(m_sumEt>4000 && m_sumEt<=6000) fcal_idx=4;

                    if(eta_idx>-1 && fcal_idx>-1) Corel_Photon_Off_On_Pt[Corel_trig_idx][photon_type][fcal_idx][eta_idx]->Fill(offline_pt,online_pt); //Particular fcal + Particular Eta
                    if(fcal_idx==-1 && eta_idx>-1)  Corel_Photon_Off_On_Pt[Corel_trig_idx][photon_type][5][eta_idx]->Fill(offline_pt,online_pt);  //All fcal + Particular eta
                    if(fcal_idx>-1 && eta_idx==-1)  Corel_Photon_Off_On_Pt[Corel_trig_idx][photon_type][fcal_idx][2]->Fill(offline_pt,online_pt); //Particular fcal + All eta  

                    Corel_Photon_Off_On_Pt[Corel_trig_idx][photon_type][5][2]->Fill(offline_pt,online_pt); //All fcalEt + All eta 

                  }//Photon type loop ends for Only reference photons
                }
              }
            }//Checking for online objects loop ends 
            ns<<endl<<endl;
          }//Pass Condition Ends
        }//Offline photon loop ends
      }//Photon size condition
      else{
        // ns<<"No Photon in this event."<<endl;
      }
    }//Corel_trig_idx loop ends
    
    ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";

    return StatusCode::SUCCESS;
  }


  void MyxAODAnalysis::Save_Hists(std::string outfilename){
    std::string Ghost_name;
    char name[500];

    TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");
    /*
    //For Efficiency of L1 wrt MinBias
    for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
      for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
        for(int i=0;i<3;i++){
          for(int j=0;j<6;j++){
            for(int k=0;k<3;k++){
              Ghost_name =  m_L1_trigList.at(trig_count)+"_"+m_L1_Ref_trigList.at(ref_trig_count);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-1";
              if(j==1) Ghost_name += "FcalEt_1-2";
              if(j==2) Ghost_name += "FcalEt_2-3";
              if(j==3) Ghost_name += "FcalEt_3-4";
              if(j==4) Ghost_name += "FcalEt_4-6";
              if(j==5) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Evt_L1[trig_count][ref_trig_count][i][j][k],name);

              //For saving the denominator
              sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Total_L1[trig_count][ref_trig_count][i][j][k],name);
            }
          }
        }
      }
    }
    */
    //For Efficiency of EtCut Triggers wrt corresponding L1 Triggers
    for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Trigs_Photons;EtCut_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx)+"_" + m_Ref_trigList_EtCut_Photons.at(EtCut_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Photon_EtCut_Evt_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Evt_EtCut[EtCut_trig_idx][i][j][k],name);

            //For saving the denominator
            sprintf(name,"Photon_EtCut_Total_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Total_EtCut[EtCut_trig_idx][i][j][k],name);

          }
        }
      }   
    }
    
    
    //For Efficiency of Id Triggers wrt correspnding EtCut Triggers
    for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Trigs_Photons;Id_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Evt_Id[Id_trig_idx][i][j][k],name);

            //For saving the denominator
            sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Total_Id[Id_trig_idx][i][j][k],name);

          }
        }
      }   
    }

    //For EtaPhiMaps of Id Triggers
    for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Trigs_Photons;Id_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"EtaPhiMap_Photon_Id_Evt_%s",Ghost_name.c_str());
            Outfile->WriteObject(EtaPhiMap_Photon_Evt_Id[Id_trig_idx][i][j][k],name);

          }
        }
      }   
    }
    
    //For 2D Correlation Plots between Offline And Online Photons
    for(int Corel_trig_idx=0; Corel_trig_idx<nof_Corel_Trigs_Photons;Corel_trig_idx++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name = m_trigList_Corel_Photons.at(Corel_trig_idx);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";
            
            sprintf(name,"Corel_Pt_Photon_Evt_%s",Ghost_name.c_str());
            Outfile->WriteObject(Corel_Photon_Off_On_Pt[Corel_trig_idx][i][j][k],name);

          }
        }
      }   
    }

    Outfile->Close();
    ANA_MSG_INFO("All Histograms have been saved.");
  }
  

  StatusCode MyxAODAnalysis :: finalize ()
  {
    // This method is the mirror image of initialize(), meaning it gets
    // called after the last event has been processed on the worker node
    // and allows you to finish up any objects you created in
    // initialize() before they are written to disk.  This is actually
    // fairly rare, since this happens separately for each worker node.
    // Most of the time you want to do your post-processing on the
    // submission node after all your histogram outputs have been
    // merged.

    ANA_MSG_INFO ("In Finalize.");

    // std::string outfilename = "/afs/cern.ch/user/a/adimri/Egamma_Trigger/Output_New.root";
    std::string outfilename = "Output_New.root";
    Save_Hists(outfilename);
    
    int trash = 1;
    
    ns<<endl<<"....................End....................."<<endl;
    ns.close();
    return StatusCode::SUCCESS;
  }
