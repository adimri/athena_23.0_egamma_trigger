#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <AsgTools/ToolHandle.h> 

//Taken from Qipeng
#include "TriggerMatchingTool/R3MatchingTool.h"
// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> // GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 


  class MyxAODAnalysis : public EL::AnaAlgorithm
  {
    private:

    double m_electronPtCut; // Electron pT cut
    std::string m_sampleName; // Sample name
    double m_JetPtCut; //Jet pT Cut
  

    public:
    unsigned int trigDecision = 0; //< Trigger decision

    ToolHandle<Trig::TrigDecisionTool> m_trigDecTool; //Using directly the Trigdecision Tool
    ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool; //Using Trigdecision Tool Using the interface file
    ToolHandle<Trig::IMatchingTool> m_matchtool;  //Using matchtool from the Interface file
    
    //Do by hand
    double dR(double eta1,double phi1, double eta2, double phi2); //dR calculation by hand
    bool L1_eEM15_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM15, bool match=false); //By hand execution of L1_EM15 matching
    bool L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match=false);  //By hand execution of L1_EM15 matching
    bool L1_eEM12_TrigMatch(const xAOD::Electron *eg, const xAOD::eFexEMRoI *eEM12, bool match=false); //By hand execution of L1_EM15 matching
    bool L1_eEM12_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM12, bool match=false);  //By hand execution of L1_EM15 matching
    bool L1_EM12_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching
    bool L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM12 matching
    bool L1_EM10_TrigMatch(const xAOD::Electron *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM10 matching
    bool L1_EM10_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false);  //By hand execution of L1_EM10 matching


    
    
    
    //Likelihood Tool Selectors
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //grl tool
    AsgElectronLikelihoodTool *m_electronLooseLHSelector; 
    AsgElectronLikelihoodTool *m_electronMediumLHSelector; 
    AsgElectronLikelihoodTool *m_electronTightLHSelector; 
    AsgPhotonIsEMSelector *m_photonTightIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonLooseIsEMSelector; 
    AsgPhotonIsEMSelector *m_photonMediumIsEMSelector; 
    
    std::vector<std::string> m_L1_trigList = { "L1_EM10", "L1_EM12", "L1_eEM12", "L1_eEM15"};
    int nof_L1_triggers = int(m_L1_trigList.size()*1.0); //L1 Trigger Size

    std::vector<std::string> m_L1_Ref_trigList = {
    "HLT_mb_sptrk_pc_L1ZDC_A_C_VTE50",
    "HLT_noalg_L1TE600p0ETA49",
    "HLT_noalg_L1TE50_VTE600p0ETA49"
    }; //3 reference for each the L1 triggers
    int nof_L1_ref_triggers = int(m_L1_Ref_trigList.size()*1.0); //L1 Reference Trigger Size

    TH1D *Photon_Evt_L1[4][3][3][6][3];
    TH1D *Photon_Total_L1[4][3][3][6][3];
    //First [] is for the L1 triggers
    //Second [] is for the MinBias Triggers wrt to which we need to compute efficiency
    //Third [] is for loose,medium and tight electrons
    //Fourth [] is for Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
    //Fifth [] is fot Eta Coverage: Barrel, Eta, All


    //Triglist for Etcut Triggers
    std::vector<std::string> m_trigList_EtCut_Photons= {
      "HLT_g13_etcut_ion_L1EM10",
      "HLT_g18_etcut_ion_L1EM10",

      "HLT_g15_etcut_ion_L1EM12",
      "HLT_g18_etcut_ion_L1EM12",
      "HLT_g20_etcut_ion_L1EM12",

      "HLT_g13_etcut_ion_L1eEM12",
      "HLT_g18_etcut_ion_L1eEM12",
      
      "HLT_g15_etcut_ion_L1eEM15",
      "HLT_g18_etcut_ion_L1eEM15",
      "HLT_g20_etcut_ion_L1eEM15"
    };

    std::vector<std::string> m_Ref_trigList_EtCut_Photons= {
      "L1_EM10",
      "L1_EM10",

      "L1_EM12",
      "L1_EM12",
      "L1_EM12",

      "L1_eEM12",
      "L1_eEM12",

      "L1_eEM15",
      "L1_eEM15",
      "L1_eEM15" 
    };

    int nof_EtCut_Trigs_Photons = int(m_trigList_EtCut_Photons.size()*1.0); //Trigger Size
    
    TH1D *Photon_Evt_EtCut[10][3][6][3];
    TH1D *Photon_Total_EtCut[10][3][6][3];
    //First []: Nof of Triggers.
    //Second []: Loose, Medium, Tight Photons
    //Third []: Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
    //Fourth []: Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

    //Triglist Photons
    std::vector<std::string> m_trigList_Id_Photons= {
      "HLT_g15_loose_ion_L1EM10",
      "HLT_g15_loose_ion_L1EM12",
      "HLT_g20_loose_ion_L1EM12",

      "HLT_g15_loose_ion_L1eEM12",
      "HLT_g15_loose_ion_L1eEM15",
      "HLT_g20_loose_ion_L1eEM15"
    };

    //Reference Triglist Photons
    std::vector<std::string> m_Ref_trigList_Id_Photons= {
      "HLT_g13_etcut_ion_L1EM10",
      "HLT_g15_etcut_ion_L1EM12",
      "HLT_g20_etcut_ion_L1EM12",

      "HLT_g13_etcut_ion_L1eEM12",
      "HLT_g15_etcut_ion_L1eEM15",
      "HLT_g20_etcut_ion_L1eEM15"
    };
    
    int nof_Id_Trigs_Photons = int(m_trigList_Id_Photons.size()*1.0); //Trigger Size

    TH1D *Photon_Evt_Id[6][3][6][3];
    TH1D *Photon_Total_Id[6][3][6][3];
    TH2D *EtaPhiMap_Photon_Evt_Id[6][3][6][3];
    //First []: Nof of Triggers.
    //Second []: Loose, Medium, Tight Photons
    //Third []: Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
    //Fourth []: Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta
    
    //Triglist Photons
    std::vector<std::string> m_trigList_Corel_Photons= {
      "HLT_g15_loose_ion_L1EM12",
      "HLT_g20_loose_ion_L1EM12",

      "HLT_g15_loose_ion_L1eEM15",
      "HLT_g20_loose_ion_L1eEM15"
    };

    int nof_Corel_Trigs_Photons = int(m_trigList_Corel_Photons.size()*1.0); //Trigger Size
    TH2D *Corel_Photon_Off_On_Pt[4][3][6][3];
    //First []: Nof of Triggers.
    //Second []: Loose, Medium, Tight Photons
    //Third []: Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
    //Fourth []: Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

    void limit_nof_triggers() {
      if(nof_L1_triggers > 2) nof_L1_triggers = 4;
      if(nof_EtCut_Trigs_Photons > 10)  nof_EtCut_Trigs_Photons  =10;
      if(nof_Id_Trigs_Photons > 6) nof_Id_Trigs_Photons = 6;
      if(nof_Corel_Trigs_Photons > 4) nof_Corel_Trigs_Photons = 4;
    }

    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    void Init_Hists();
    void Save_Hists(std::string);
    void SetHists(TH1*,const std::string &,const std::string &);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;
    
    //File Handle
    std::ofstream ns;
  };


#endif