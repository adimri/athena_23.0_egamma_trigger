# This file will be configured to contain variables for CPack. These variables
# should be set in the CMake list file of the project before CPack module is
# included. The list of available CPACK_xxx variables and their associated
# documentation may be obtained using
#  cpack --help-variable-list
#
# Some variables are common to all generators (e.g. CPACK_PACKAGE_NAME)
# and some are specific to a generator
# (e.g. CPACK_NSIS_EXTRA_INSTALL_COMMANDS). The generator specific variables
# usually begin with CPACK_<GENNAME>_xxxx.


set(CPACK_BUILD_SOURCE_DIRS "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/athena/Projects/WorkDir;/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build")
set(CPACK_CMAKE_GENERATOR "Unix Makefiles")
set(CPACK_COMPONENTS_ALL "Debug;Main;Unspecified")
set(CPACK_COMPONENT_UNSPECIFIED_HIDDEN "TRUE")
set(CPACK_COMPONENT_UNSPECIFIED_REQUIRED "TRUE")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_FILE "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.24.3/Linux-x86_64/share/cmake-3.24/Templates/CPack.GenericDescription.txt")
set(CPACK_DEFAULT_PACKAGE_DESCRIPTION_SUMMARY "WorkDir built using CMake")
set(CPACK_DMG_SLA_USE_RESOURCE_FILE_LICENSE "ON")
set(CPACK_GENERATOR "RPM")
set(CPACK_IGNORE_FILES "/CVS/;/\\.svn/;/\\.bzr/;/\\.hg/;/\\.git/;\\.swp\$;\\.#;/#")
set(CPACK_INSTALLED_DIRECTORIES "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/athena/Projects/WorkDir;/")
set(CPACK_INSTALL_CMAKE_PROJECTS "")
set(CPACK_INSTALL_COMMANDS "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CMakeFiles/cpack_install.sh")
set(CPACK_INSTALL_PREFIX "usr/WorkDir/23.0.50/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_INSTALL_SCRIPT "/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.50/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules/scripts/cpack_install.cmake")
set(CPACK_MODULE_PATH "/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.50/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules;/cvmfs/atlas.cern.ch/repo/sw/software/23.0/AthenaExternals/23.0.50/InstallArea/x86_64-centos7-gcc11-opt/cmake/modules;/cvmfs/atlas.cern.ch/repo/sw/software/23.0/AthenaExternals/23.0.50/InstallArea/x86_64-centos7-gcc11-opt/lib64/cmake/VecCore")
set(CPACK_NSIS_DISPLAY_NAME "WorkDir/23.0.50/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_NSIS_INSTALLER_ICON_CODE "")
set(CPACK_NSIS_INSTALLER_MUI_ICON_CODE "")
set(CPACK_NSIS_INSTALL_ROOT "$PROGRAMFILES")
set(CPACK_NSIS_PACKAGE_NAME "WorkDir/23.0.50/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_NSIS_UNINSTALL_NAME "Uninstall")
set(CPACK_OUTPUT_CONFIG_FILE "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CPackConfig.cmake")
set(CPACK_PACKAGE_CONTACT "atlas-sw-core@cern.ch")
set(CPACK_PACKAGE_DEFAULT_LOCATION "/usr")
set(CPACK_PACKAGE_DESCRIPTION "WorkDir - 23.0.50")
set(CPACK_PACKAGE_DESCRIPTION_FILE "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CMakeFiles/README.txt")
set(CPACK_PACKAGE_DESCRIPTION_SUMMARY "WorkDir - 23.0.50")
set(CPACK_PACKAGE_FILE_NAME "WorkDir-23.0.50-Source")
set(CPACK_PACKAGE_INSTALL_DIRECTORY "WorkDir/23.0.50/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_PACKAGE_INSTALL_REGISTRY_KEY "WorkDir/23.0.50/InstallArea/x86_64-centos7-gcc11-opt")
set(CPACK_PACKAGE_NAME "WorkDir")
set(CPACK_PACKAGE_RELOCATABLE "true")
set(CPACK_PACKAGE_VENDOR "ATLAS Collaboration")
set(CPACK_PACKAGE_VERSION "23.0.50")
set(CPACK_PACKAGE_VERSION_MAJOR "23")
set(CPACK_PACKAGE_VERSION_MINOR "0")
set(CPACK_PACKAGE_VERSION_PATCH "50")
set(CPACK_PROJECT_CONFIG_FILE "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CMakeFiles/CPackOptions.cmake")
set(CPACK_RESOURCE_FILE_LICENSE "/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.50/InstallArea/x86_64-centos7-gcc11-opt/LICENSE.txt")
set(CPACK_RESOURCE_FILE_README "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CMakeFiles/README.txt")
set(CPACK_RESOURCE_FILE_WELCOME "/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/Cmake/3.24.3/Linux-x86_64/share/cmake-3.24/Templates/CPack.GenericWelcome.txt")
set(CPACK_RPM_PACKAGE_ARCHITECTURE "noarch")
set(CPACK_RPM_PACKAGE_AUTOREQ " no")
set(CPACK_RPM_PACKAGE_AUTOREQPROV " no")
set(CPACK_RPM_PACKAGE_GROUP "ATLAS Software")
set(CPACK_RPM_PACKAGE_LICENSE "Apache License Version 2.0")
set(CPACK_RPM_PACKAGE_NAME "WorkDir_23.0.50_x86_64-centos7-gcc11-opt")
set(CPACK_RPM_PACKAGE_PROVIDES "/bin/sh")
set(CPACK_RPM_PACKAGE_REQUIRES "Athena_23.0.50_x86_64-centos7-gcc11-opt, LCG_102b_ATLAS_11_jsonmcpp_3.10.5_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_freetype_2.10.0_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_Qt5_5.15.2_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_gtest_1.11.0_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_expat_2.2.6_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_zlib_1.2.11_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_XercesC_3.2.3_x86_64_centos7_gcc11_opt, ATLAS_Geant4Data_geant4.10.6.patch03.atlasmt07, LCG_102b_ATLAS_11_Python_3.9.12_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_future_0.17.1_x86_64_centos7_gcc11_opt, tdaq-common-10-00-00_x86_64-centos7-gcc11-opt, LCG_102b_ATLAS_11_tbb_2020_U2_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_cppgsl_3.1.0_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_AIDA_3.2.1_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_HepPDT_2.06.01_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_CppUnit_1.14.0_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_libunwind_1.3.1_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_fmt_7.1.3_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_graphviz_2.40.1_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_doxygen_1.8.18_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_ROOT_6.26.08_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_rangev3_0.11.0_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_clhep_2.4.5.1_x86_64_centos7_gcc11_opt, LCG_102b_ATLAS_11_Boost_1.78.0_x86_64_centos7_gcc11_opt")
set(CPACK_RPM_PACKAGE_SOURCES "ON")
set(CPACK_RPM_PACKAGE_VERSION "23.0.50")
set(CPACK_RPM_SPEC_MORE_DEFINE "
%global __os_install_post %{nil}
%define _unpackaged_files_terminate_build 0
%define _binaries_in_noarch_packages_terminate_build 0
%define _source_payload w2.xzdio
%define _binary_payload w2.xzdio
%undefine __brp_mangle_shebangs")
set(CPACK_SET_DESTDIR "OFF")
set(CPACK_SOURCE_GENERATOR "RPM")
set(CPACK_SOURCE_IGNORE_FILES "/CVS/;/\\.svn/;/\\.bzr/;/\\.hg/;/\\.git/;\\.swp\$;\\.#;/#")
set(CPACK_SOURCE_INSTALLED_DIRECTORIES "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/athena/Projects/WorkDir;/")
set(CPACK_SOURCE_OUTPUT_CONFIG_FILE "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CPackSourceConfig.cmake")
set(CPACK_SOURCE_PACKAGE_FILE_NAME "WorkDir-23.0.50-Source")
set(CPACK_SOURCE_TOPLEVEL_TAG "Linux-Source")
set(CPACK_STRIP_FILES "")
set(CPACK_SYSTEM_NAME "Linux")
set(CPACK_THREADS "1")
set(CPACK_TOPLEVEL_TAG "Linux-Source")
set(CPACK_WIX_SIZEOF_VOID_P "8")

if(NOT CPACK_PROPERTIES_FILE)
  set(CPACK_PROPERTIES_FILE "/afs/cern.ch/user/a/adimri/Egamma_Trigger_Final_Plots/build/CPackProperties.cmake")
endif()

if(EXISTS ${CPACK_PROPERTIES_FILE})
  include(${CPACK_PROPERTIES_FILE})
endif()
