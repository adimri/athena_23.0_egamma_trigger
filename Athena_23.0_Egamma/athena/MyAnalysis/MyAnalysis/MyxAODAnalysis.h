#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include <AsgTools/ToolHandle.h> 

#include "TriggerMatchingTool/R3MatchingTool.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 


  class MyxAODAnalysis : public EL::AnaAlgorithm
  {
    private:
    
    // Configuration, and any other types of variables go here.
    //float m_cutValue;
    //TTree *m_myTree;
    //TH1 *m_myHist;

    double m_electronPtCut; // Electron pT cut
    std::string m_sampleName; // Sample name
    double m_JetPtCut; //Jet pT Cut
  

    public:
    unsigned int trigDecision = 0; //< Trigger decision

    ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool;
    ToolHandle<Trig::IMatchingTool> m_matchtool;

    std::vector<std::string> m_trigList;

    //Do by hand
    double dR(double eta1,double phi1, double eta2, double phi2);

    // ToolHandle<Trig::ITrigEgammaMatchingTool> m_matchTool; //Matching
    // void matchEgamma(const std::string&, const xAOD::Egamma *); //Function doing the match

    //Stuff Below is from R3 MAtchingTool
    // Keep a cache of the interpreted chain information
    /*
    using multInfo_t = std::vector<std::size_t>;
    using typeInfo_t = std::vector<xAODType::ObjectType>;
    using chainInfo_t = std::pair<multInfo_t, typeInfo_t>;
    mutable std::map<std::string, chainInfo_t> m_chainInfoCache ATLAS_THREAD_SAFE;
    mutable std::mutex m_chainInfoMutex ATLAS_THREAD_SAFE;
    ToolHandle<Trig::TrigDecisionTool> m_trigDecTool; //Papa of ITrigDecisionTool
    ToolHandle<Trig::IMatchScoringTool> m_scoreTool{this, "ScoringTool", "Trig::DRScoringTool","Tool to score pairs of particles"};
    const chainInfo_t &getChainInfo(const std::string &chain) const;
    bool matchObjects(const xAOD::IParticle *reco,const ElementLink<xAOD::IParticleContainer> &onlineLink,xAODType::ObjectType onlineType,std::map<std::pair<uint32_t, uint32_t>, bool> &cache,double drThreshold) const;
    bool matchR3Tool(const std::vector<const xAOD::IParticle *> &recoObjects,const std::string &chain,double matchThreshold,bool rerun) const; //Function that does the matching
    bool matchR3Tool(const xAOD::IParticle &recoObject,const std::string &chain,double matchThreshold,bool rerun) const;
    */

    // ToolHandle<Trig::ITrigEgammaMatchingTool> m_matchTool; //Matching
    // ToolHandle<Trig::IMatchingTool> m_tmt;

    // ToolHandle<Trig::MatchingTool> m_matchTool; //Matching
    // ToolHandle<Trig::TrigDecisionTool> trigDecisionTool;

    // std::map<std::string,int> m_counterMatch1Bits; //Some matching count stuff required 
    // std::map<std::string,int> m_counterMatch2Bits;
    // std::map<std::string,int> m_counterMatch3Bits;
    // std::map<std::string,int> m_counterMatch4Bits;

    //Define histograms
    TH1D *Photon_Hists[2][4][3];
    TH1D *Electron_Hists[2][4][3];
    //First []: 0->Event Passed, 1->Stores Total for calculating efficiency for event passed
    //Second []:0->Sum Fcal Et, 1-> Particle Pt, 2->Particle Eta, 3->Particle Phi
    //Third []:0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

    // TH2D *EtaPhiMaps[2][2]; //x is eta, y is phi
    // TH1D *Dist_Et[2][2];  
    //First []: 0->Offline, 1->Online. Second []: 0->Electron, 1->Photon

    // this is a standard algorithm constructor
    MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);
    void Init_Hists();
    void Save_Hists(std::string);
    void SetHists(TH1*,const std::string &,const std::string &);

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

    //File Handle
    std::ofstream ns;
  };


#endif