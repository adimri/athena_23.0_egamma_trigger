#See: https://twiki.cern.ch/twiki/bin/viewauth/AtlasComputing/SoftwareTutorialxAODAnalysisInCMake for more details about anything here

#testFile = os.getenv("ALRB_TutorialData") + '/mc21_13p6TeV.601229.PhPy8EG_A14_ttbar_hdamp258p75_SingleLep.deriv.DAOD_PHYS.e8357_s3802_r13508_p5057/DAOD_PHYS.28625583._000007.pool.root.1'

#testFile = '/afs/cern.ch/user/a/adimri/DAOD_PHYS.28625583._000007.pool.root'

from AthenaCommon.AthenaCommonFlags import athenaCommonFlags

# testFile = ['/afs/cern.ch/user/a/adimri/data18_hi/data18_hi.00367384.physics_HardProbes.merge.AOD.f1030_m2048._lb0142._0004.1',
#             '/afs/cern.ch/user/a/adimri/data18_hi/data18_hi/data18_hi.00367384.physics_HardProbes.merge.AOD.f1030_m2048._lb0083._0001.1'] 

# testFile = ['/eos/user/a/adimri/data22_13p6TeV/AOD.31185268._000024.pool.root.1',
# '/eos/user/a/adimri/data22_13p6TeV/AOD.31185268._000026.pool.root.1','/eos/user/a/adimri/data22_13p6TeV/AOD.31185268._000054.pool.root.1',
# '/eos/user/a/adimri/data22_13p6TeV/AOD.31185268._000098.pool.root.1','/eos/user/a/adimri/data22_13p6TeV/AOD.31185268._000115.pool.root.1']

testFile = ['/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000001.pool.root.1',
'/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000036.pool.root.1','/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000054.pool.root.1',
'/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000126.pool.root.1','/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000129.pool.root.1',
'/eos/user/a/adimri/New_Data/data22_hi/AOD.34062077._000001.pool.root.1']

# testFile = ['/eos/user/a/adimri/New_Data/data22_hi/AOD.33769034._000001.pool.root.1']

# testFile = ['/eos/user/a/adimri/MC_Data/mc23_13p6TeV/AOD.33839837._010684.pool.root.1']
 
print ("Testfile: ",testFile)

#override next line on command line with: --filesInput=XXX
# jps.AthenaCommonFlags.FilesInput = [testFile[0],testFile[1]]
athenaCommonFlags.FilesInput.set_Value_and_Lock(testFile)


#Specify AccessMode (read mode) ... ClassAccess is good default for xAOD
jps.AthenaCommonFlags.AccessMode = "ClassAccess" 


# Create the algorithm's configuration. 
from AnaAlgorithm.DualUseConfig import createAlgorithm
from AnaAlgorithm.DualUseConfig import addPrivateTool, createAlgorithm, createPublicTool
from AthenaCommon.AppMgr import ToolSvc

alg = createAlgorithm ( 'MyxAODAnalysis', 'AnalysisAlg' )   #alg is the object of the algorithm

#Sequence from TestMatchingToolAlg.cxx Method
# athenaCommonFlags.EvtMax=1
# include ("RecExCommon/RecExCommon_topOptions.py")
# ToolSvc.TrigDecisionTool.TrigDecisionKey='xTrigDecision'
# ToolSvc += CfgMgr.Trig__MatchingTool("MyMatchingTool",OutputLevel=DEBUG)

# include("TriggerTest/TriggerTestCommon.py")

#Making the Matching Tool
tdt = createPublicTool("Trig::TrigDecisionTool", "TrigDecisionTool")
addPrivateTool(tdt, "ConfigTool", "TrigConf::xAODConfigTool")
# #Next Line of Code is specifically for Run3
tdt.NavigationFormat = "TrigComposite"
# tdt.NavigationFormat = "TriggerElement"

# tdt.HLTSummary = "HLTNav_Summary_DAODSlimmed"
# tdt.HLTSummary = "HLTNav_Summary_OnlineSlimmed"
tdt.HLTSummary = "HLTNav_Summary_AODSlimmed"

# ToolSvc += CfgMgr.Trig__MatchingTool("MatchingTool",OutputLevel=DEBUG)
# ToolSvc += Trig__R3MatchingTool('MatchingTool')
# matching_tool = addPrivateTool(alg, "MatchingTool", "Trig::R3MatchingTool")
addPrivateTool(alg, "MatchingTool", "Trig::R3MatchingTool")
# matching_tool.TrigDecisionTool = tdt
# matching_tool = tdt
# addPrivateTool(alg, "MatchingTool.ScoringTool", "Trig::DRScoringTool")
alg.trigDecisionTool = tdt
alg.MatchingTool.TrigDecisionTool = tdt

# from TrigEgammaMatchingTool.TrigEgammaMatchingToolConf import Trig__TrigEgammaMatchingTool
# matchtool = Trig__TrigEgammaMatchingTool("MatchingTool")
# from AthenaCommon.AppMgr import ToolSvc
# ToolSvc += matchtool
# alg.TrigEgammaMatchingTool = matchtool

# set up trigger config service
# from TriggerJobOpts.TriggerConfigGetter import TriggerConfigGetter
# cfg =  TriggerConfigGetter()
# from TrigDecisionTool.TrigDecisionToolConf import Trig__TrigDecisionTool
# tdt = Trig__TrigDecisionTool("TrigDecisionTool")
# ToolSvc += tdt 

#Trying to get Debug messages
# import logging
# logging.basicConfig()
# logging.getLogger().setLevel(logging.DEBUG)

#Write the properties
# alg.ElectronPtCut = 30000.0
# alg.SampleName = 'Zee'
# alg.JetPtCut = 1000.5

#Output level
#Default is INFO. Can change to VERBOSE/DEBUG/INFO/WARNING/ERROR/FATAL
# alg.OutputLevel=DEBUG

# later on we'll add some configuration options for our algorithm that go here

# Set data type
# dataType = "data" # "mc" or "data"

#Trigger List
# triggerChains = [
#     'HLT_e26_lhtight_nod0_ivarloose',
#     'HLT_e60_lhmedium',
#     'HLT_mu26_ivarmedium',
#     'HLT_mu50',
#     'HLT_e140_lhloose',
#     'HLT_e300_etcut',
#     'HLT_mu60_0eta105_msonly',
#     'HLT_e13_etcut_ion'
# ]

# Include and set up the trigger analysis sequence:
# from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
# triggerSequence = makeTriggerAnalysisSequence( dataType, triggerChains=triggerChains )

# Add the trigger analysis sequence to the job:
# athAlgSeq += triggerSequence

# Add our algorithm to the main alg sequence
athAlgSeq += alg

# limit the number of events (for testing purposes)
# theApp.EvtMax = 10

#For Histograms
# jps.AthenaCommonFlags.HistOutputs = ["ANALYSIS:Test1.root"]
# svcMgr.THistSvc.MaxFileSize=-1 #speeds up jobs that output lots of histograms

# optional include for reducing printout from athena
include("AthAnalysisBaseComps/SuppressLogging.py")

