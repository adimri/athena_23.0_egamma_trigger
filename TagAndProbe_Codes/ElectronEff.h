#ifndef ElectronEff_h
#define ElectronEff_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include "string.h"
#include "TH1.h"
#include "TH2.h"
#include "TProfile.h"
#include "TGraph.h"

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

//Some Global Variables
const int NFile_max=100;
const int nof_L1_Triggers = 2;
const int nof_EtCut_Triggers = 2;
const int nof_Id_Triggers = 8;


const int max_electron_types = 3; //Loose, Medium and Tight
const int max_fcalEt_bins = 11; //0.25,0.75,1.25, ... , 4.75. And All.
const int max_Eta_bins = 3; //Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

const int nof_xaxes = 3; //pT, fcalEt, Eta.
const int max_pT_bins = 3; //pT bins. pT>20, pT>35, All pT.
const double pT_bin_Vals[3] = {20,35,0};


class ElectronEff{
    private:

    // Declaration of leaf types
    Int_t           run_number;
    Long64_t        event_number;
    vector<float>   *Electron_charge;
    vector<float>   *Electron_px;   //MeV
    vector<float>   *Electron_py;   //MeV
    vector<float>   *Electron_pz;   //MeV
    vector<float>   *Electron_m;    //GeV
    vector<float>   *Electron_eta;
    vector<float>   *Electron_phi;
    vector<bool>    *Electron_Pass_Medium;
    vector<bool>    *Electron_Pass_Tight;
    vector<float>   *Electron_topoetcone30;
    vector<float>   *Electron_ptcone30;
    vector<bool>    *Electron_Pass_L1_EM12;
    vector<double>  *Electron_ClosestdR_EM12;
    vector<bool>    *Electron_Pass_e15_EtCut_ion_L1EM12;
    vector<bool>    *Electron_Pass_e15_lhloose_L1EM12;
    vector<bool>    *Electron_Pass_e15_loose_L1EM12;
    vector<bool>    *Electron_Pass_e15_lhmedium_L1EM12;
    vector<bool>    *Electron_Pass_e15_medium_L1EM12;
    vector<bool>    *Electron_Pass_L1_eEM15;
    vector<double>  *Electron_ClosestdR_eEM15;
    vector<bool>    *Electron_Pass_e15_EtCut_ion_eL1EM15;
    vector<bool>    *Electron_Pass_e15_lhloose_eL1EM15;
    vector<bool>    *Electron_Pass_e15_loose_eL1EM15;
    vector<bool>    *Electron_Pass_e15_lhmedium_eL1EM15;
    vector<bool>    *Electron_Pass_e15_medium_eL1EM15;
    Float_t         Evt_fcalEt;

    // List of branches
    TBranch        *b_run_number;   //!
    TBranch        *b_event_number;   //!
    TBranch        *b_Electron_charge;   //!
    TBranch        *b_Electron_px;   //!
    TBranch        *b_Electron_py;   //!
    TBranch        *b_Electron_pz;   //!
    TBranch        *b_Electron_m;   //!
    TBranch        *b_Electron_eta;   //!
    TBranch        *b_Electron_phi;   //!
    TBranch        *b_Electron_Pass_Medium;   //!
    TBranch        *b_Electron_Pass_Tight;   //!
    TBranch        *b_Electron_topoetcone30;   //!
    TBranch        *b_Electron_ptcone30;   //!
    TBranch        *b_Electron_Pass_L1_EM12;   //!
    TBranch        *b_Electron_ClosestdR_EM12;   //!
    TBranch        *b_Electron_Pass_e15_EtCut_ion_L1EM12;   //!
    TBranch        *b_Electron_Pass_e15_lhloose_L1EM12;   //!
    TBranch        *b_Electron_Pass_e15_loose_L1EM12;   //!
    TBranch        *b_Electron_Pass_e15_lhmedium_L1EM12;   //!
    TBranch        *b_Electron_Pass_e15_medium_L1EM12;   //!
    TBranch        *b_Electron_Pass_L1_eEM15;   //!
    TBranch        *b_Electron_ClosestdR_eEM15;   //!
    TBranch        *b_Electron_Pass_e15_EtCut_ion_eL1EM15;   //!
    TBranch        *b_Electron_Pass_e15_lhloose_eL1EM15;   //!
    TBranch        *b_Electron_Pass_e15_loose_eL1EM15;   //!
    TBranch        *b_Electron_Pass_e15_lhmedium_eL1EM15;   //!
    TBranch        *b_Electron_Pass_e15_medium_eL1EM15;   //!
    TBranch        *b_Evt_fcalEt;   //!
    public:

    //Class Variables
    TChain *Event; //TChain variable to merge all the trees from the different files

    std::vector<std::string> m_trigList_L1_Electrons = { "L1_EM12", "L1_eEM15"};
    std::vector<std::string> m_trigList_EtCut_Electrons = { "HLT_e15_etcut_ion_L1EM12", "HLT_e15_etcut_ion_L1eEM15"};

    //Trigger List
     //L1 wrt MinBias
    std::vector<std::string> m_Ref_trigList_Id_L1_Electrons = {
        "L1_EM12",
        "L1_EM12",
        "L1_EM12",
        "L1_EM12",
        "L1_eEM15",
        "L1_eEM15",
        "L1_eEM15",
        "L1_eEM15"
    };

    //EtCut wrt L1
    std::vector<std::string> m_Ref_trigList_Id_EtCut_Electrons= {
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1EM12",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15",
    "HLT_e15_etcut_ion_L1eEM15"
    };

    //HLT wrt EtCut
    std::vector<std::string> m_trigList_Id_Electrons= {
    "HLT_e15_lhloose_nogsf_ion_L1EM12",
    "HLT_e15_loose_nogsf_ion_L1EM12",
    "HLT_e15_lhmedium_nogsf_ion_L1EM12",
    "HLT_e15_medium_nogsf_ion_L1EM12",
    "HLT_e15_lhloose_nogsf_ion_L1eEM15",
    "HLT_e15_loose_nogsf_ion_L1eEM15",
    "HLT_e15_lhmedium_nogsf_ion_L1eEM15",
    "HLT_e15_medium_nogsf_ion_L1eEM15"
    };


    //Class Functions
    void Init_Tree(string Infiles_Loc);                                     //Initialize the fchain variable
    
    void Init_Hists();                                                      //Initialize all the TProfiles+histograms that will be filled in the analysis
    void Write_Hists(string outfilename);                                   //Write the histograms
    void Ana();                                                             //Main Function for the analysis. Put 0 to make use of all events
    void Execute_ElectronEff(string Infiles_Loc,string outfilename);        //Executes the Maxro ElectronEff.C

    //Support Functions to Ana()
    int Get_Eta_Index(double Eta_Val);
    int Get_FcalEt_Index(double Sum_fcalEt);
    bool IsPossibleTagElectron(float pT, float eta);

    //Relevant_Histograms
    TH1D *Electron_Evt_Id_EtCut[nof_Id_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    TH1D *Electron_Total_Id_EtCut[nof_Id_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];

    TH1D *Electron_Evt_Id_L1[nof_Id_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    TH1D *Electron_Total_Id_L1[nof_Id_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];

    TH1D *Electron_Recon_Total_EtCut[nof_EtCut_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    TH1D *Electron_Recon_Total_L1[nof_L1_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    TH1D *Electron_Recon_Total_Id[nof_Id_Triggers][max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    TH1D *Electron_Recon_Total[max_fcalEt_bins][max_Eta_bins][nof_xaxes];
    
    TH1D *AllElectron_Threshold[2];

    // Extra Histograms
    TH1D *Hist_Inv_Mass_All;
    TH1D *Hist_Inv_Z;
};

#endif