#include <AsgMessaging/MessageCheck.h>
#include <MyAnalysis/MyxAODAnalysis.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "xAODHIEvent/HIEventShapeContainer.h"
#include "FourMomUtils/xAODP4Helpers.h"
#include <xAODTracking/TrackParticleContainer.h>
#include "xAODTracking/TrackingPrimitives.h" //Particle Hypothesis
#include <string.h>
#include "TMath.h"
#include "TFile.h"
#include <stdio.h>


#include "xAODBase/IParticleContainer.h"
#include "TrigCompositeUtils/Combinations.h"
#include "TrigCompositeUtils/ChainNameParser.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingToolMT.h"
#include "xAODEgamma/Egamma.h"
#include <numeric>
#include <algorithm>

using namespace std;

MyxAODAnalysis :: MyxAODAnalysis (const std::string &name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
    ,m_trigDecTool("Trig::TrigDecisionTool/TrigDecisionTool")
    ,trigDecisionTool ("Trig::TrigDecisionTool/TrigDecisionTool")
    ,m_matchtool(this,"Trig::MatchingTool")
    // ,m_matchTool_MT("Trig::TrigEgammaMatchingToolMT/TrigEgammaMatchingToolMT")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  ANA_MSG_INFO ("Inside  Constructor.");

  //For Trigger Tools
  declareProperty ("trigDecisionTool", trigDecisionTool, "the trigger decision tool");
  declareProperty("Papa_TrigDecisionTool", m_trigDecTool, "The trigger decision tool");
  declareProperty("MatchingTool", m_matchtool); //Matching
  // declareProperty( "MatchingToolMT" , m_matchTool_MT );

}

StatusCode MyxAODAnalysis :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO ("In Initialize.");
  //Open File for checking purposes
  // ns.open("CheckFile_New.txt");
  // ns<<".....................Begin....................."<<endl;
  
  //Define the histograms
  Init_Hists();

  //Check if the decision tool is working properly
  ANA_CHECK(trigDecisionTool.retrieve());
  ANA_CHECK(m_trigDecTool.retrieve());
  ANA_CHECK(m_matchtool.retrieve());
  // ANA_CHECK(m_matchTool_MT.retrieve());

  // GRL. Good Run List Tool
  m_grl.setTypeAndName("GoodRunsListSelectionTool/myGRLTool");

  //Offline Photon Tool Selectors (from Qipeng)
  m_photonTightIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonTightIsEMSelector" );
  ANA_CHECK(m_photonTightIsEMSelector->setProperty("WorkingPoint", "TightPhoton"));
  ANA_CHECK(m_photonTightIsEMSelector->initialize());

  m_photonLooseIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonLooseIsEMSelector" );
  ANA_CHECK(m_photonLooseIsEMSelector->setProperty("WorkingPoint", "LoosePhoton"));
  ANA_CHECK(m_photonLooseIsEMSelector->initialize());

  m_photonMediumIsEMSelector = new AsgPhotonIsEMSelector ( "PhotonMediumIsEMSelector" );
  ANA_CHECK(m_photonMediumIsEMSelector->setProperty("WorkingPoint", "MediumPhoton"));
  ANA_CHECK(m_photonMediumIsEMSelector->initialize());

  return StatusCode::SUCCESS;
}


void MyxAODAnalysis::Init_Hists(){
  std::string Ghost_name;
  // double pi = TMath::Pi();
  char name[500];

  /*
  //For Efficiency of L1 wrt MinBias
  for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
    for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_L1_trigList.at(trig_count)+"_" + m_L1_Ref_trigList.at(ref_trig_count);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
            Photon_Evt_L1[trig_count][ref_trig_count][i][j][k] = new TH1D(name,name,500,-0.5,499.5);

            //For saving the denominator
            sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
            Photon_Total_L1[trig_count][ref_trig_count][i][j][k] = new TH1D(name,name,500,-0.5,499.5);
          }
        }
      }
    }
  }
  

  //For Efficiency of EtCut Triggers wrt corresponding L1 Triggers
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Trigs_Photons;EtCut_trig_idx++){
    for(int i=0;i<3;i++){
      for(int j=0;j<6;j++){
        for(int k=0;k<3;k++){
          Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx)+"_" + m_Ref_trigList_EtCut_Photons.at(EtCut_trig_idx);
          if(i==0) Ghost_name += "_Loose_LH_Off";
          if(i==1) Ghost_name += "_Medium_LH_Off";
          if(i==2) Ghost_name += "_Tight_LH_Off";

          if(j==0) Ghost_name += "FcalEt_0-1";
          if(j==1) Ghost_name += "FcalEt_1-2";
          if(j==2) Ghost_name += "FcalEt_2-3";
          if(j==3) Ghost_name += "FcalEt_3-4";
          if(j==4) Ghost_name += "FcalEt_4-6";
          if(j==5) Ghost_name += "FcalEt_All";

          if(k==0) Ghost_name +="_Eta_Barrel";
          if(k==1) Ghost_name +="_Eta_EndCap";
          if(k==2) Ghost_name +="_Eta_All";
          
          sprintf(name,"Photon_EtCut_Evt_%s",Ghost_name.c_str());
          Photon_Evt_EtCut[EtCut_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

          //For saving the denominator
          sprintf(name,"Photon_EtCut_Total_%s",Ghost_name.c_str());
          Photon_Total_EtCut[EtCut_trig_idx][i][j][k] = new TH1D(name,name,300,-0.5,299.5);

        }
      }
    }   
  }
  */
  
  //For Efficiency of Id Triggers wrt correspnding EtCut Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<max_photon_types;i++){
      for(int j=0;j<max_fcalEt_bins;j++){
        for(int k=0;k<max_Eta_bins;k++){
          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            for(int xaxes_idx=0;xaxes_idx<nof_xaxes;xaxes_idx++){

              Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-0.5";
              if(j==1) Ghost_name += "FcalEt_0.5-1";
              if(j==2) Ghost_name += "FcalEt_1-1.5";
              if(j==3) Ghost_name += "FcalEt_1.5-2";
              if(j==4) Ghost_name += "FcalEt_2-2.5";
              if(j==5) Ghost_name += "FcalEt_2.5-3";
              if(j==6) Ghost_name += "FcalEt_3-3.5";
              if(j==7) Ghost_name += "FcalEt_3.5-4";
              if(j==8) Ghost_name += "FcalEt_4-4.5";
              if(j==9) Ghost_name += "FcalEt_4.5-5";
              if(j==10) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";
              
              if(pT_idx==0) Ghost_name += "_pT_geq_20";
              if(pT_idx==1) Ghost_name += "_pT_geq_35";
              if(pT_idx==2) Ghost_name += "_pT_All";

              if(xaxes_idx==0) Ghost_name += "_Axis_pT";
              if(xaxes_idx==1) Ghost_name += "_Axis_fcalEt";
              if(xaxes_idx==2) Ghost_name += "_Axis_Eta";

              sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
              if(xaxes_idx==0)  Photon_Evt_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,300,-0.5,299.5);
              if(xaxes_idx==1)  Photon_Evt_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,12,-500,5500);
              if(xaxes_idx==2)  Photon_Evt_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,52,-2.6,2.6);

              //For saving the denominator
              sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
              if(xaxes_idx==0)  Photon_Total_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,300,-0.5,299.5);
              if(xaxes_idx==1)  Photon_Total_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,12,-500,5500);
              if(xaxes_idx==2)  Photon_Total_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,52,-2.6,2.6);
            }
          }
        }
      }
    }   
  }

  //For Efficiency of Id Triggers wrt correspnding L1 Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<max_photon_types;i++){
      for(int j=0;j<max_fcalEt_bins;j++){
        for(int k=0;k<max_Eta_bins;k++){
          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            for(int xaxes_idx=0;xaxes_idx<nof_xaxes;xaxes_idx++){

              Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_L1_Photons.at(Id_trig_idx);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-0.5";
              if(j==1) Ghost_name += "FcalEt_0.5-1";
              if(j==2) Ghost_name += "FcalEt_1-1.5";
              if(j==3) Ghost_name += "FcalEt_1.5-2";
              if(j==4) Ghost_name += "FcalEt_2-2.5";
              if(j==5) Ghost_name += "FcalEt_2.5-3";
              if(j==6) Ghost_name += "FcalEt_3-3.5";
              if(j==7) Ghost_name += "FcalEt_3.5-4";
              if(j==8) Ghost_name += "FcalEt_4-4.5";
              if(j==9) Ghost_name += "FcalEt_4.5-5";
              if(j==10) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";

              if(pT_idx==0) Ghost_name += "_pT_geq_20";
              if(pT_idx==1) Ghost_name += "_pT_geq_35";
              if(pT_idx==2) Ghost_name += "_pT_All";

              if(xaxes_idx==0) Ghost_name += "_Axis_pT";
              if(xaxes_idx==1) Ghost_name += "_Axis_fcalEt";
              if(xaxes_idx==2) Ghost_name += "_Axis_Eta";

              sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
              if(xaxes_idx==0)  Photon_Evt_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,300,-0.5,299.5);
              if(xaxes_idx==1)  Photon_Evt_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,12,-500,5500);
              if(xaxes_idx==2)  Photon_Evt_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,52,-2.6,2.6);

              //For saving the denominator
              sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
              if(xaxes_idx==0)  Photon_Total_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,300,-0.5,299.5);
              if(xaxes_idx==1)  Photon_Total_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,12,-500,5500);
              if(xaxes_idx==2)  Photon_Total_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx] = new TH1D(name,name,52,-2.6,2.6);
            }
          }
        }
      }
    }   
  }
  
  /*
  //For Bad Photons (which have pT mismatch between offline and online objects)
  //L1
  for(int trig_count=0;trig_count<nof_L1_Triggers;trig_count++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_L1_trigList.at(trig_count) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0){
          Ghost_name+="_EtaPhi";
          sprintf(name,"Photon_L1_%s",Ghost_name.c_str());
          Photon_L1_Bad[trig_count][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
        }

        if(i==1){
          Ghost_name+="_On_Off_pT";
          sprintf(name,"Photon_L1_%s",Ghost_name.c_str());
          Photon_L1_Bad[trig_count][i][j] = new TH2D(name,name,20,-0.5,19.5,150,-0.5,149.5);
        }

      }
    }
  }

  //EtCut
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Triggers;EtCut_trig_idx++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_trigList_EtCut_Photons.at(EtCut_trig_idx) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0){
          Ghost_name+="_EtaPhi";
          sprintf(name,"Photon_EtCut_%s",Ghost_name.c_str());
          Photon_EtCut_Bad[EtCut_trig_idx][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
        }

        if(i==1){
          Ghost_name+="_On_Off_pT";
          sprintf(name,"Photon_EtCut_%s",Ghost_name.c_str());
          Photon_EtCut_Bad[EtCut_trig_idx][i][j] = new TH2D(name,name,20,-0.5,19.5,150,-0.5,149.5);
        }

      }
    }
  }

  //Id Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_trigList_Id_Photons.at(Id_trig_idx) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0){
          Ghost_name+="_EtaPhi";
          sprintf(name,"Photon_Id_%s",Ghost_name.c_str());
          Photon_Id_Bad[Id_trig_idx][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
        }

        if(i==1){
          Ghost_name+="_On_Off_pT";
          sprintf(name,"Photon_Id_%s",Ghost_name.c_str());
          Photon_Id_Bad[Id_trig_idx][i][j] = new TH2D(name,name,20,-0.5,19.5,150,-0.5,149.5);
        }

      }
    }
  }
  */

  /*
  //For Checking the properties of the Egamma online and offline objects.
  //For Offline Only
  for(int i=0;i<max_photon_types;i++){
    if(i==0) Ghost_name += "Loose_LH_Off";
    if(i==1) Ghost_name += "Medium_LH_Off";
    if(i==2) Ghost_name += "Tight_LH_Off";

    sprintf(name,"Photon_Off_All_pT_%s",Ghost_name.c_str());
    Hist_pT_All[i] = new TH1D(name,name,250,-0.5,249.5);

    sprintf(name,"Photon_Off_All_EtaPhiMap_%s",Ghost_name.c_str());
    EtaPhiMap_All[i] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
  }

  //For L1
  for(int L1_trig_idx=0;L1_trig_idx<nof_L1_Triggers;L1_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_L1_trigList.at(L1_trig_idx).c_str(),Plot_Name.c_str());
      Hist_pT_Online_L1[L1_trig_idx][j] = new TH1D(name,name,250,-0.5,249.5);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_L1_trigList.at(L1_trig_idx).c_str(),Plot_Name.c_str());
      EtaPhiMap_Online_L1[L1_trig_idx][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_L1_trigList.at(L1_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<2;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Hist_pT_Matched_L1[L1_trig_idx][i][j] = new TH1D(name,name,250,-0.5,249.5);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        EtaPhiMap_Matched_L1[L1_trig_idx][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_L1_trigList.at(L1_trig_idx).c_str());
    DeltaR_L1[L1_trig_idx] = new TH1D(name,name,200,-0.005,1.995);
  }

  //For EtCut
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Triggers;EtCut_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str(),Plot_Name.c_str());
      Hist_pT_Online_EtCut[EtCut_trig_idx][j] = new TH1D(name,name,250,-0.5,249.5);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str(),Plot_Name.c_str());
      EtaPhiMap_Online_EtCut[EtCut_trig_idx][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<3;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";
        if(j==2) Plot_Name+="_Direct_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Hist_pT_Matched_EtCut[EtCut_trig_idx][i][j] = new TH1D(name,name,250,-0.5,249.5);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        EtaPhiMap_Matched_EtCut[EtCut_trig_idx][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str());
    DeltaR_EtCut[EtCut_trig_idx] = new TH1D(name,name,200,-0.005,1.995);
  }

  //For Id Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str(),Plot_Name.c_str());
      Hist_pT_Online_Id[Id_trig_idx][j] = new TH1D(name,name,250,-0.5,249.5);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str(),Plot_Name.c_str());
      EtaPhiMap_Online_Id[Id_trig_idx][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<3;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";
        if(j==2) Plot_Name+="_Direct_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Hist_pT_Matched_Id[Id_trig_idx][i][j] = new TH1D(name,name,250,-0.5,249.5);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        EtaPhiMap_Matched_Id[Id_trig_idx][i][j] = new TH2D(name,name,50,-2.5,2.5,64,-3.2,3.2);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str());
    DeltaR_Id[Id_trig_idx] = new TH1D(name,name,200,-0.005,1.995);
  }
  */

}

void MyxAODAnalysis::Save_Hists(std::string outfilename){
  std::string Ghost_name;
  char name[500];

  TFile *Outfile = TFile::Open(outfilename.c_str(),"RECREATE");
  
  /*
  //For Efficiency of L1 wrt MinBias
  for(int trig_count=0;trig_count<nof_L1_triggers;trig_count++){
    for(int ref_trig_count=0;ref_trig_count<nof_L1_ref_triggers;ref_trig_count++){
      for(int i=0;i<3;i++){
        for(int j=0;j<6;j++){
          for(int k=0;k<3;k++){
            Ghost_name =  m_L1_trigList.at(trig_count)+"_"+m_L1_Ref_trigList.at(ref_trig_count);
            if(i==0) Ghost_name += "_Loose_LH_Off";
            if(i==1) Ghost_name += "_Medium_LH_Off";
            if(i==2) Ghost_name += "_Tight_LH_Off";

            if(j==0) Ghost_name += "FcalEt_0-1";
            if(j==1) Ghost_name += "FcalEt_1-2";
            if(j==2) Ghost_name += "FcalEt_2-3";
            if(j==3) Ghost_name += "FcalEt_3-4";
            if(j==4) Ghost_name += "FcalEt_4-6";
            if(j==5) Ghost_name += "FcalEt_All";

            if(k==0) Ghost_name +="_Eta_Barrel";
            if(k==1) Ghost_name +="_Eta_EndCap";
            if(k==2) Ghost_name +="_Eta_All";

            sprintf(name,"Photon_L1_Evt_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Evt_L1[trig_count][ref_trig_count][i][j][k],name);

            //For saving the denominator
            sprintf(name,"Photon_L1_Total_%s",Ghost_name.c_str());
            Outfile->WriteObject(Photon_Total_L1[trig_count][ref_trig_count][i][j][k],name);
          }
        }
      }
    }
  }
  
  
  //For Efficiency of EtCut Triggers wrt corresponding L1 Triggers
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Trigs_Photons;EtCut_trig_idx++){
    for(int i=0;i<3;i++){
      for(int j=0;j<6;j++){
        for(int k=0;k<3;k++){
          Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx)+"_" + m_Ref_trigList_EtCut_Photons.at(EtCut_trig_idx);
          if(i==0) Ghost_name += "_Loose_LH_Off";
          if(i==1) Ghost_name += "_Medium_LH_Off";
          if(i==2) Ghost_name += "_Tight_LH_Off";

          if(j==0) Ghost_name += "FcalEt_0-1";
          if(j==1) Ghost_name += "FcalEt_1-2";
          if(j==2) Ghost_name += "FcalEt_2-3";
          if(j==3) Ghost_name += "FcalEt_3-4";
          if(j==4) Ghost_name += "FcalEt_4-6";
          if(j==5) Ghost_name += "FcalEt_All";

          if(k==0) Ghost_name +="_Eta_Barrel";
          if(k==1) Ghost_name +="_Eta_EndCap";
          if(k==2) Ghost_name +="_Eta_All";
          
          sprintf(name,"Photon_EtCut_Evt_%s",Ghost_name.c_str());
          Outfile->WriteObject(Photon_Evt_EtCut[EtCut_trig_idx][i][j][k],name);

          //For saving the denominator
          sprintf(name,"Photon_EtCut_Total_%s",Ghost_name.c_str());
          Outfile->WriteObject(Photon_Total_EtCut[EtCut_trig_idx][i][j][k],name);

        }
      }
    }   
  }
  */
  
  //For Efficiency of Id Triggers wrt correspnding EtCut Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<max_photon_types;i++){
      for(int j=0;j<max_fcalEt_bins;j++){
        for(int k=0;k<max_Eta_bins;k++){
          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            for(int xaxes_idx=0;xaxes_idx<nof_xaxes;xaxes_idx++){

              Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_Photons.at(Id_trig_idx);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-0.5";
              if(j==1) Ghost_name += "FcalEt_0.5-1";
              if(j==2) Ghost_name += "FcalEt_1-1.5";
              if(j==3) Ghost_name += "FcalEt_1.5-2";
              if(j==4) Ghost_name += "FcalEt_2-2.5";
              if(j==5) Ghost_name += "FcalEt_2.5-3";
              if(j==6) Ghost_name += "FcalEt_3-3.5";
              if(j==7) Ghost_name += "FcalEt_3.5-4";
              if(j==8) Ghost_name += "FcalEt_4-4.5";
              if(j==9) Ghost_name += "FcalEt_4.5-5";
              if(j==10) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";
              
              if(pT_idx==0) Ghost_name += "_pT_geq_20";
              if(pT_idx==1) Ghost_name += "_pT_geq_35";
              if(pT_idx==2) Ghost_name += "_pT_All";

              if(xaxes_idx==0) Ghost_name += "_Axis_pT";
              if(xaxes_idx==1) Ghost_name += "_Axis_fcalEt";
              if(xaxes_idx==2) Ghost_name += "_Axis_Eta";

              sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Evt_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx],name);

              //For saving the denominator
              sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Total_Id[Id_trig_idx][i][j][k][pT_idx][xaxes_idx],name);
            }
          }
        }
      }
    }   
  }

  //For Efficiency of Id Triggers wrt correspnding L1 Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<max_photon_types;i++){
      for(int j=0;j<max_fcalEt_bins;j++){
        for(int k=0;k<max_Eta_bins;k++){
          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            for(int xaxes_idx=0;xaxes_idx<nof_xaxes;xaxes_idx++){

              Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx)+"_" + m_Ref_trigList_Id_L1_Photons.at(Id_trig_idx);
              if(i==0) Ghost_name += "_Loose_LH_Off";
              if(i==1) Ghost_name += "_Medium_LH_Off";
              if(i==2) Ghost_name += "_Tight_LH_Off";

              if(j==0) Ghost_name += "FcalEt_0-0.5";
              if(j==1) Ghost_name += "FcalEt_0.5-1";
              if(j==2) Ghost_name += "FcalEt_1-1.5";
              if(j==3) Ghost_name += "FcalEt_1.5-2";
              if(j==4) Ghost_name += "FcalEt_2-2.5";
              if(j==5) Ghost_name += "FcalEt_2.5-3";
              if(j==6) Ghost_name += "FcalEt_3-3.5";
              if(j==7) Ghost_name += "FcalEt_3.5-4";
              if(j==8) Ghost_name += "FcalEt_4-4.5";
              if(j==9) Ghost_name += "FcalEt_4.5-5";
              if(j==10) Ghost_name += "FcalEt_All";

              if(k==0) Ghost_name +="_Eta_Barrel";
              if(k==1) Ghost_name +="_Eta_EndCap";
              if(k==2) Ghost_name +="_Eta_All";
              
              if(pT_idx==0) Ghost_name += "_pT_geq_20";
              if(pT_idx==1) Ghost_name += "_pT_geq_35";
              if(pT_idx==2) Ghost_name += "_pT_All";

              if(xaxes_idx==0) Ghost_name += "_Axis_pT";
              if(xaxes_idx==1) Ghost_name += "_Axis_fcalEt";
              if(xaxes_idx==2) Ghost_name += "_Axis_Eta";

              sprintf(name,"Photon_Id_Evt_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Evt_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx],name);

              //For saving the denominator
              sprintf(name,"Photon_Id_Total_%s",Ghost_name.c_str());
              Outfile->WriteObject(Photon_Total_Id_L1[Id_trig_idx][i][j][k][pT_idx][xaxes_idx],name);
            }
          }
        }
      }
    }   
  }
  
  /*
  //For Bad Photons (which have pT mismatch between offline and online objects)
  //L1
  for(int trig_count=0;trig_count<nof_L1_Triggers;trig_count++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_L1_trigList.at(trig_count) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0)  Ghost_name+="_EtaPhi";
        if(i==1)  Ghost_name+="_On_Off_pT";

        sprintf(name,"Photon_L1_%s",Ghost_name.c_str());

        Outfile->WriteObject(Photon_L1_Bad[trig_count][i][j],name);

      }
    }
  }

  //EtCut
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Triggers;EtCut_trig_idx++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_trigList_EtCut_Photons.at(EtCut_trig_idx) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0)  Ghost_name+="_EtaPhi";
        if(i==1)  Ghost_name+="_On_Off_pT";

        sprintf(name,"Photon_EtCut_%s",Ghost_name.c_str());

        Outfile->WriteObject(Photon_EtCut_Bad[EtCut_trig_idx][i][j],name);

      }
    }
  }

  //Id Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int i=0;i<2;i++){
      for(int j=0;j<3;j++){
        Ghost_name =  "Bad_"+ m_trigList_Id_Photons.at(Id_trig_idx) +"_";

        if(j==0) Ghost_name += "_Loose_LH_Off";
        if(j==1) Ghost_name += "_Medium_LH_Off";
        if(j==2) Ghost_name += "_Tight_LH_Off";

        if(i==0)  Ghost_name+="_EtaPhi";
        if(i==1)  Ghost_name+="_On_Off_pT";

        sprintf(name,"Photon_Id_%s",Ghost_name.c_str());

        Outfile->WriteObject(Photon_Id_Bad[Id_trig_idx][i][j],name);

      }
    }
  }
  */
  /*
  //For Checking the properties of the Egamma online and offline objects.
  //For Offline Only
  for(int i=0;i<max_photon_types;i++){
    if(i==0) Ghost_name += "Loose_LH_Off";
    if(i==1) Ghost_name += "Medium_LH_Off";
    if(i==2) Ghost_name += "Tight_LH_Off";

    sprintf(name,"Photon_Off_All_pT_%s",Ghost_name.c_str());
    Outfile->WriteObject(Hist_pT_All[i],name);

    sprintf(name,"Photon_Off_All_EtaPhiMap_%s",Ghost_name.c_str());
    Outfile->WriteObject(EtaPhiMap_All[i],name);
  }

  //For L1
  for(int L1_trig_idx=0;L1_trig_idx<nof_L1_Triggers;L1_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_L1_trigList.at(L1_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(Hist_pT_Online_L1[L1_trig_idx][j],name);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_L1_trigList.at(L1_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(EtaPhiMap_Online_L1[L1_trig_idx][j],name);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_L1_trigList.at(L1_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<2;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(Hist_pT_Matched_L1[L1_trig_idx][i][j],name);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(EtaPhiMap_Matched_L1[L1_trig_idx][i][j],name);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_L1_trigList.at(L1_trig_idx).c_str());
    Outfile->WriteObject(DeltaR_L1[L1_trig_idx],name);
  }

  //For EtCut
  for(int EtCut_trig_idx=0; EtCut_trig_idx<nof_EtCut_Triggers;EtCut_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(Hist_pT_Online_EtCut[EtCut_trig_idx][j],name);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(EtaPhiMap_Online_EtCut[EtCut_trig_idx][j],name);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_trigList_EtCut_Photons.at(EtCut_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<3;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";
        if(j==2) Plot_Name+="_Direct_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(Hist_pT_Matched_EtCut[EtCut_trig_idx][i][j],name);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(EtaPhiMap_Matched_EtCut[EtCut_trig_idx][i][j],name);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_trigList_EtCut_Photons.at(EtCut_trig_idx).c_str());
    Outfile->WriteObject(DeltaR_EtCut[EtCut_trig_idx],name);
  }

  //For Id Triggers
  for(int Id_trig_idx=0; Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    for(int j=0;j<2;j++){
      string Plot_Name;
      if(j==0) Plot_Name+="_All";
      if(j==1) Plot_Name+="_Matched";

      sprintf(name,"Photon_Online_pT_%s_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(Hist_pT_Online_Id[Id_trig_idx][j],name);

      sprintf(name,"Photon_Online_EtaPhiMap_%s_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str(),Plot_Name.c_str());
      Outfile->WriteObject(EtaPhiMap_Online_Id[Id_trig_idx][j],name);
    }

    for(int i=0;i<max_photon_types;i++){
      Ghost_name = m_trigList_Id_Photons.at(Id_trig_idx);

      if(i==0) Ghost_name += "_Loose_LH_Off";
      if(i==1) Ghost_name += "_Medium_LH_Off";
      if(i==2) Ghost_name += "_Tight_LH_Off";

      for(int j=0;j<3;j++){
        string Plot_Name;
        if(j==0) Plot_Name+="_All_Match";
        if(j==1) Plot_Name+="_Actual_Match";
        if(j==2) Plot_Name+="_Direct_Match";

        sprintf(name,"Photon_Offline_pT_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(Hist_pT_Matched_Id[Id_trig_idx][i][j],name);

        sprintf(name,"Photon_Offline_EtaPhiMap_%s_%s",Ghost_name.c_str(),Plot_Name.c_str());
        Outfile->WriteObject(EtaPhiMap_Matched_Id[Id_trig_idx][i][j],name);
      }
    }

    sprintf(name,"Hist_DeltaR_%s",m_trigList_Id_Photons.at(Id_trig_idx).c_str());
    Outfile->WriteObject(DeltaR_Id[Id_trig_idx],name);
  }
  */

  Outfile->Close();
  ANA_MSG_INFO("All Histograms have been saved.");
}

// Define the Particle structure
struct Particle_Photon{
  int index; //Location of offline particle in the container
  double online_pT; //pT of Matched online pT object
  double pT; //pT of the particle in GeV.
  double eta;  //eta of the particle
  double phi; //phi of the particle
  double DeltaR; //DeltaR Value between the offline and online photons
  string Trigname_Pass; //Trigger which the particle passes
  bool type[3]; //True at indices 0,1 and 2 if photon is loose, medium and tight respectively
};

//Functions to return relevant indices.
int MyxAODAnalysis::Get_FcalEt_Index(double Sum_fcalEt){
  if(Sum_fcalEt>0 && Sum_fcalEt<500) return (0);
  if(Sum_fcalEt>500 && Sum_fcalEt<1000) return (1);
  if(Sum_fcalEt>1000 && Sum_fcalEt<1500) return (2);
  if(Sum_fcalEt>1500 && Sum_fcalEt<2000) return (3);
  if(Sum_fcalEt>2000 && Sum_fcalEt<2500) return (4);
  if(Sum_fcalEt>2500 && Sum_fcalEt<3000) return (5);
  if(Sum_fcalEt>3000 && Sum_fcalEt<3500) return (6);
  if(Sum_fcalEt>3500 && Sum_fcalEt<4000) return (7);
  if(Sum_fcalEt>4000 && Sum_fcalEt<4500) return (8);
  if(Sum_fcalEt>4500 && Sum_fcalEt<5000) return (9);

  return (-1); //To handle cases where Sum_fcalEt>=6TeV
}

int MyxAODAnalysis::Get_Eta_Index(double Eta_Val){
    
  if(fabs(Eta_Val) < 1.37) return 0; //Barrel
  if(fabs(Eta_Val)<2.37 && fabs(Eta_Val)>1.52 ) return 1; //End Caps

  return -1; //Else In the Crack Region or at the extreme edge. All Condition.

}


//Functions to perform the matching

//By hand execution of L1_EM12 matching for Photon
bool MyxAODAnalysis::L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match, double dR_Threshold){

  if (EMTauRoI->roiType() != xAOD::EmTauRoI::EMRoIWord){ //Checks if the EmTauRoI object is correct or not??Ask!!
      return (false);
  } //Condition checks if the ROI type is indeed for a Run 2 EM object. (Another two possiblities are Run 1 or Tau of Run 2) 

  const std::vector<std::string>& thresholdNames = EMTauRoI->thrNames();
  
  // bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
  bool EM12ThresholdPassed = (std::find(thresholdNames.begin(), thresholdNames.end(), "EM12") != std::end(thresholdNames));
  if(!EM12ThresholdPassed){
    return (false);
  }

  //Compute dR
  float eta = EMTauRoI->eta();
  float phi = EMTauRoI->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }

  return (match);
}

//By hand execution of L1_eEM15 matching for Photon
bool MyxAODAnalysis::L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match, double dR_Threshold){
  //No need for ROI type condition here. eEMRoI is implemented for newer data and Tau has its own container (eTauRoI).
  //That is why no such condition is required and no such function exists.

  if(eEM15->et()*1e-3 < 13){ //eEM15 objects should pass the threshold
    return(false);
  }

  //eEMRoI does not have threshold names.
  //But it has something called the TOB word or the xTOB word. I don't know if that needs to be matched or not.
  if(!(eEM15->isTOB())) {
    // ns<<"Not a trigger object. (TOB). Continue."<<endl;
    return (false);
  }
  //Sanity Checks. L1_eEMRoI stores only TOB objects. This is just the final nail in the coffin to make sure.

  //Compute dR
  float eta = eEM15->eta();
  float phi = eEM15->phi();
  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  return (match);
}

//By hand execution of Id Triggers matching for Photon
bool MyxAODAnalysis::HLT_TrigMatch(const xAOD::Photon *eg, const xAOD::Egamma *online_eg, bool match, double dR_Threshold){
  //Compute dR
  float eta = online_eg->eta();
  float phi = online_eg->phi();

  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  return (match);
}

//By hand execution of Et Triggers matching for Photon
bool MyxAODAnalysis::ET_TrigMatch(const xAOD::Photon *eg, const xAOD::CaloCluster *online_eg, bool match, double dR_Threshold){
  //Compute dR
  float eta = online_eg->eta();
  float phi = online_eg->phi();

  const float dR = xAOD::P4Helpers::deltaR(*eg, eta, phi, false);

  if (dR < dR_Threshold){  //If dR passes the condition the matching has occured. 
    match = true; //Change Value to true
  }  

  return (match);
}

StatusCode MyxAODAnalysis :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  //Main Code Here
  //Retrieve the eventInfo object from the event store
  const xAOD::EventInfo *eventInfo = nullptr; //Define the event info pointer
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));  //Get the event info pointer  

  ANA_MSG_DEBUG ("in execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber());
  
  // ns<<"In execute, runNumber = " << eventInfo->runNumber() << ", eventNumber = " << eventInfo->eventNumber()<<endl;

  if (!m_grl->passRunLB(*eventInfo)) { //Check if the run passes the good run list tool
    ANA_MSG_DEBUG ("drop event: GRL");
    // ns<<"Dropping Event:GRL->"<<eventInfo->runNumber()<<" : "<<eventInfo->eventNumber()<<endl;
    return StatusCode::SUCCESS;
  }
  
  //Compute fcalEt of the Event
  const xAOD::HIEventShapeContainer *hies = 0;  
  ANA_CHECK( evtStore()->retrieve(hies,"HIEventShape") );

  double m_sumEt_A = 0;
  double m_sumEt_C = 0;
  for (const xAOD::HIEventShape *ES : *hies) {
    double et = ES->et()*1e-3;
    double eta = ES->etaMin();

    int layer = ES->layer();
    if (layer != 21 && layer != 22 && layer != 23) continue;
    if (eta > 0) {
      m_sumEt_A += et;
    } else {
      m_sumEt_C += et;
    }
  }
  double m_sumEt = m_sumEt_A + m_sumEt_C; // in GeV
  int fcalEt_idx = Get_FcalEt_Index(m_sumEt);// fcalEt index is fixed for an event.


  const xAOD::PhotonContainer *photon_info = nullptr; //Define photon pointer
  ANA_CHECK (evtStore()->retrieve (photon_info, "Photons"));  //Get Offline photon pointer
  
  /*
  //Fill In Stuff for Offline Photons
  if(photon_info->size()>0){
    for(const auto eg : *photon_info){  //Loop Over Offline Photons
      bool off_photon_type[3];
      off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
      off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
      off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));

      for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
        if(!off_photon_type[photon_type_idx]) continue;
        Hist_pT_All[photon_type_idx]->Fill(eg->pt()*0.001); //GeV
        EtaPhiMap_All[photon_type_idx]->Fill(eg->eta(),eg->phi());
      }

    }
  }
  */
  
  //Get the L1 Photon Triggers
  const xAOD::EmTauRoIContainer *m_EMTauRoIs = nullptr; //Define the L1 particle info pointer
  ANA_CHECK (evtStore()->retrieve(m_EMTauRoIs, "LVL1EmTauRoIs"));  //Get the L1 (legacy) event info pointer  

  const xAOD::eFexEMRoIContainer *m_eEM15 = nullptr; //Define the L1 particle info pointer
  ANA_CHECK (evtStore()->retrieve(m_eEM15, "L1_eEMRoI"));  //Get the L1 (Phase-I) event info pointer

  double dR_Threshold = 0.1;

  std::vector<Particle_Photon> Photon_Pass_L1_Trig[nof_L1_Triggers];
  for(int i=0;i<nof_L1_Triggers;i++) Photon_Pass_L1_Trig[i].clear(); //Clear the vectors. Good Habits.
  
  for (const auto EMTauRoI: *m_EMTauRoIs){  //Online Egamma Containers for L1 Legacy Triggers
    string trigname = m_L1_trigList.at(0);
    Particle_Photon Ghost;
    int idx = 0;
    bool matched_once = false;

    //Fill In the Histograms for All Online Photons
    // Hist_pT_Online_L1[0][0]->Fill(EMTauRoI->eT()*0.001); //GeV
    // EtaPhiMap_Online_L1[0][0]->Fill(EMTauRoI->eta(), EMTauRoI->phi());

    if(photon_info->size()>0){
      for(const auto eg : *photon_info){  //Loop Over Offline Photons
        double eg_pt = eg->pt()*0.001;//GeV
        double eg_eta = eg->eta();   
        bool pass = false;

        pass = L1_EM12_TrigMatch(eg,EMTauRoI,pass, dR_Threshold);
        double val_DeltaR = xAOD::P4Helpers::deltaR(*eg, EMTauRoI->eta(), EMTauRoI->phi(), false);
        // DeltaR_L1[0]->Fill(val_DeltaR);

        if(pass){
          // bool off_photon_type[3];
          // off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
          // off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
          // off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
          // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
          //   if(!off_photon_type[photon_type_idx]) continue;
          //   Hist_pT_Matched_L1[0][photon_type_idx][0]->Fill(eg->pt()*0.001); //GeV
          //   EtaPhiMap_Matched_L1[0][photon_type_idx][0]->Fill(eg->eta(),eg->phi());
          // }
          

          if(matched_once==false) {
            //Fill In the Histograms for Matched Online Photons
            // Hist_pT_Online_L1[0][1]->Fill(EMTauRoI->eT()*0.001); //GeV
            // EtaPhiMap_Online_L1[0][1]->Fill(EMTauRoI->eta(), EMTauRoI->phi());

            matched_once=true;

            Ghost.index = idx;
            Ghost.pT = eg_pt;
            Ghost.online_pT = EMTauRoI->eT()*0.001; //GeV
            Ghost.eta = eg_eta;
            Ghost.phi = eg->phi();
            Ghost.DeltaR = val_DeltaR;
            Ghost.Trigname_Pass = trigname;
            Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
          }
          else{
            double online_Et =  EMTauRoI->eT()*0.001; //GeV
            double new_offline_Et = eg_pt;
            double old_offline_Et = Ghost.pT;

            if(abs(online_Et - new_offline_Et)< abs(online_Et - old_offline_Et)){ //Only fill if new object is closer in Et
              Ghost.index = idx;
              Ghost.pT = eg_pt;
              Ghost.online_pT = EMTauRoI->eT()*0.001; //GeV
              Ghost.eta = eg_eta;
              Ghost.phi = eg->phi();
              Ghost.DeltaR = val_DeltaR;
              Ghost.Trigname_Pass = trigname;
              Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            } 
          }
        }

        idx++;
      }//Loop over offline photon ends.
    }//Photon size condition

    if(matched_once) {
      Photon_Pass_L1_Trig[0].push_back(Ghost); //Put into pass vector if a match occurs.

      // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
      //   if(!Ghost.type[photon_type_idx]) continue;
      //   Hist_pT_Matched_L1[0][photon_type_idx][1]->Fill(Ghost.pT); //GeV
      //   EtaPhiMap_Matched_L1[0][photon_type_idx][1]->Fill(Ghost.eta,Ghost.phi);
      // }

      // ns<<"L1 EMTauRoI, Index: "<<Ghost.index<<endl;
      
      // if(Ghost.pT<12){ //Offline pT is less than threshold
      //   for(int photon_type=0;photon_type<max_photon_types;photon_type++){
      //     if(!Ghost.type[photon_type]) continue;
      //     Photon_L1_Bad[0][0][photon_type]->Fill(Ghost.eta,Ghost.phi);
      //     Photon_L1_Bad[0][1][photon_type]->Fill(Ghost.pT,Ghost.online_pT);
      //   }
      // }
    }
  }

  for (const xAOD::eFexEMRoI *eEM15: *m_eEM15){ 
    string trigname = m_L1_trigList.at(1);
    Particle_Photon Ghost;
    int idx = 0;
    bool matched_once = false;

    //Fill In the Histograms for All Online Photons
    // Hist_pT_Online_L1[1][0]->Fill(eEM15->et()*0.001); //GeV
    // EtaPhiMap_Online_L1[1][0]->Fill(eEM15->eta(), eEM15->phi());

    if(photon_info->size()>0){
      for(const auto eg : *photon_info){  //Loop Over Offline Photons
        double eg_pt = eg->pt()*0.001;//GeV
        double eg_eta = eg->eta();   
        bool pass = false;

        pass = L1_eEM15_TrigMatch(eg,eEM15,pass, dR_Threshold);
        double val_DeltaR = xAOD::P4Helpers::deltaR(*eg, eEM15->eta(), eEM15->phi(), false);
        // DeltaR_L1[1]->Fill(val_DeltaR);
        
        if(pass){
          // bool off_photon_type[3];
          // off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
          // off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
          // off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
          // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
          //   if(!off_photon_type[photon_type_idx]) continue;
          //   Hist_pT_Matched_L1[1][photon_type_idx][0]->Fill(eg->pt()*0.001); //GeV
          //   EtaPhiMap_Matched_L1[1][photon_type_idx][0]->Fill(eg->eta(),eg->phi());
          // }

          if(matched_once==false) {
            //Fill In the Histograms for Matched Online Photons
            // Hist_pT_Online_L1[1][1]->Fill(eEM15->et()*0.001); //GeV
            // EtaPhiMap_Online_L1[1][1]->Fill(eEM15->eta(), eEM15->phi());

            matched_once=true;

            Ghost.index = idx;
            Ghost.pT = eg_pt;
            Ghost.online_pT = eEM15->et()*0.001; //GeV
            Ghost.eta = eg_eta;
            Ghost.phi = eg->phi();
            Ghost.DeltaR = val_DeltaR;
            Ghost.Trigname_Pass = trigname;
            Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
          }
          else{
            double online_Et =  eEM15->et()*0.001; //GeV
            double new_offline_Et = eg_pt;
            double old_offline_Et = Ghost.pT;

            if(abs(online_Et - new_offline_Et)< abs(online_Et - old_offline_Et)){ //Only fill if new object is closer in Et
              Ghost.index = idx;
              Ghost.pT = eg_pt;
              Ghost.online_pT = eEM15->et()*0.001; //GeV
              Ghost.eta = eg_eta;
              Ghost.phi = eg->phi();
              Ghost.DeltaR = val_DeltaR;
              Ghost.Trigname_Pass = trigname;
              Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            } 
          }
        }

        idx++;
      }//Loop over offline photon ends.
    }

    if(matched_once) {
      Photon_Pass_L1_Trig[1].push_back(Ghost); //Put into pass vector if a match occurs.

      // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
      //   if(!Ghost.type[photon_type_idx]) continue;
      //   Hist_pT_Matched_L1[1][photon_type_idx][1]->Fill(Ghost.pT); //GeV
      //   EtaPhiMap_Matched_L1[1][photon_type_idx][1]->Fill(Ghost.eta,Ghost.phi);
      // }

      // ns<<"L1 eEM15, Index: "<<Ghost.index<<endl;

      // if(Ghost.pT<13){ //Offline pT is less than threshold
      //   for(int photon_type=0;photon_type<max_photon_types;photon_type++){
      //     if(!Ghost.type[photon_type]) continue;
      //     Photon_L1_Bad[1][0][photon_type]->Fill(Ghost.eta,Ghost.phi);
      //     Photon_L1_Bad[1][1][photon_type]->Fill(Ghost.pT,Ghost.online_pT);
      //   }
      // }
    }
  }
  

  //Sizes of L1 objects
  // for(int i=0;i<2;i++) ns<<"Size L1: Photon_Pass_L1_Trig["<<i<<"] is: "<<Photon_Pass_L1_Trig[i].size()<<endl;


  std::vector<Particle_Photon> Photon_Pass_EtCut_Trig[nof_EtCut_Triggers];
  for(int i=0;i<nof_EtCut_Triggers;i++) Photon_Pass_EtCut_Trig[i].clear(); //Clear the vectors. Good Habits.
  
  // std::string key = "HLT_egamma_Photons";
  std::string key = "HLT_HICaloEMClusters";

  for(int Et_trig_idx=0;Et_trig_idx<nof_EtCut_Triggers;Et_trig_idx++){
    string trigname = m_trigList_EtCut_Photons.at(Et_trig_idx);
    
    //Get the online objects in HLT_egamma_Photons that pass the trigger in online_ph_vec
    std::vector<const xAOD::CaloCluster*> online_ph_vec; online_ph_vec.clear();
    // std::vector<const xAOD::TrigPhoton*> online_ph_vec; online_ph_vec.clear();
    
    // auto vec =  m_trigDecTool->features<xAOD::PhotonContainer>(trigname, TrigDefs::Physics ,key );  
    auto vec =  m_trigDecTool->features<xAOD::CaloClusterContainer>(trigname, TrigDefs::Physics ,key );  
    // ns<<"Vec Size: "<<int(1.0*vec.size())<<endl;  

    for( auto &featLinkInfo : vec ){
      if(! featLinkInfo.isValid() ) continue;
      const auto *feat = *(featLinkInfo.link);
      if(!feat) continue;
      // ns<<"ETCut Pass through vec"<<endl;
      // ns<<"Eta: "<<feat->eta()<<", phi: "<<feat->phi()<<", pT(GeV): "<<feat->pt()*0.001<<endl;
      online_ph_vec.push_back(feat);
    }

    // ns<<"Size EtCut Online: online_ph_vec size is: "<<int(1*online_ph_vec.size())<<endl;

    for(int i=0;i<int(1*online_ph_vec.size());i++){
      const xAOD::CaloCluster *online_eg =  online_ph_vec.at(i);
      // ns<<"ETCut Loop Eta: "<<online_eg->eta()<<", phi: "<<online_eg->phi()<<", pT(GeV): "<<online_eg->pt()*0.001<<endl;

      //Fill In the Histograms for All Online Photons
      // Hist_pT_Online_EtCut[Et_trig_idx][0]->Fill(online_eg->pt()*0.001);
      // EtaPhiMap_Online_EtCut[Et_trig_idx][0]->Fill(online_eg->eta(),online_eg->phi());

      Particle_Photon Ghost;
      int idx = 0;
      bool matched_once = false;

      if(photon_info->size()>0){
        for(const auto eg : *photon_info){  //Loop Over Offline Photons
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();   
          bool pass = false;

          pass = ET_TrigMatch(eg,online_eg,pass, dR_Threshold);
          double val_DeltaR = xAOD::P4Helpers::deltaR(*eg, online_eg->eta(), online_eg->phi(), false);
          // DeltaR_EtCut[Et_trig_idx]->Fill(val_DeltaR);

          if(pass){
            // bool off_photon_type[3];
            // off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            // off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            // off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
            //   if(!off_photon_type[photon_type_idx]) continue;
            //   Hist_pT_Matched_EtCut[Et_trig_idx][photon_type_idx][0]->Fill(eg->pt()*0.001); //GeV
            //   EtaPhiMap_Matched_EtCut[Et_trig_idx][photon_type_idx][0]->Fill(eg->eta(),eg->phi());
            // }

            if(matched_once==false) {
              //Fill In the Histograms for Matched Online Photons
              // Hist_pT_Online_EtCut[Et_trig_idx][1]->Fill(online_eg->pt()*0.001);
              // EtaPhiMap_Online_EtCut[Et_trig_idx][1]->Fill(online_eg->eta(),online_eg->phi());

              matched_once=true;

              Ghost.index = idx;
              Ghost.pT = eg_pt;
              Ghost.online_pT = online_eg->pt()*0.001; //GeV
              Ghost.eta = eg_eta;
              Ghost.phi = eg->phi();
              Ghost.DeltaR = val_DeltaR;
              Ghost.Trigname_Pass = trigname;
              Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            }
            else{
              double online_pt =  online_eg->pt()*0.001; //GeV
              double new_offline_pt = eg_pt;
              double old_offline_pt = Ghost.pT;

              if(abs(online_pt - new_offline_pt)< abs(online_pt - old_offline_pt)){ //Only fill if new object is closer in Et
                Ghost.index = idx;
                Ghost.pT = eg_pt;
                Ghost.online_pT = online_eg->pt()*0.001; //GeV
                Ghost.eta = eg_eta;
                Ghost.phi = eg->phi();
                Ghost.DeltaR = val_DeltaR;
                Ghost.Trigname_Pass = trigname;
                Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
                Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
                Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
              } 
            }
          }

          idx++;
        }//Loop over offline photon ends.
      }

      if(matched_once) {
        Photon_Pass_EtCut_Trig[Et_trig_idx].push_back(Ghost); //Put into pass vector if a match occurs.

        // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
        //   if(!Ghost.type[photon_type_idx]) continue;
        //   Hist_pT_Matched_EtCut[Et_trig_idx][photon_type_idx][1]->Fill(Ghost.pT); //GeV
        //   EtaPhiMap_Matched_EtCut[Et_trig_idx][photon_type_idx][1]->Fill(Ghost.eta,Ghost.phi);
        // }

        // ns<<"Et_trig_idx: "<<Et_trig_idx<<", Index: "<<Ghost.index<<endl;

        // if(Ghost.pT<15){ //Offline pT is less than threshold
        //   for(int photon_type=0;photon_type<max_photon_types;photon_type++){
        //     if(!Ghost.type[photon_type]) continue;
        //     Photon_EtCut_Bad[Et_trig_idx][0][photon_type]->Fill(Ghost.eta,Ghost.phi);
        //     Photon_EtCut_Bad[Et_trig_idx][1][photon_type]->Fill(Ghost.pT,Ghost.online_pT);
        //   }
        // }  
      }
      // ns<<" Photon_Pass_EtCut_Trig Size: "<<int(1.0*Photon_Pass_EtCut_Trig[Et_trig_idx].size())<<endl;  
    }//Loop over online photons that pass the HLT Trigger 
  }
  
  
  // const xAOD::PhotonContainer *Online_photon_info = nullptr; //Define online photon pointer for HLT triggers
  // ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_egamma_Photons"));  //Get photon pointer for HLT triggers
  // std::vector<const xAOD::Egamma*> online_ph_vec_M2[2];
/*
  const xAOD::CaloClusterContainer *Online_photon_info = nullptr; //Define online photon pointer for HLT triggers
  ANA_CHECK (evtStore()->retrieve (Online_photon_info, "HLT_HICaloEMClusters"));  //Get photon pointer for HLT triggers
  std::vector<const xAOD::CaloCluster*> online_ph_vec_M2[2];

  for(int i=0;i<2;i++) online_ph_vec_M2[i].clear();

  for(int Et_trig_idx=0;Et_trig_idx<2;Et_trig_idx++){
    string trigname = m_trigList_EtCut_Photons.at(Et_trig_idx);
    double Et_Threhold= 0;
    if(trigname=="HLT_g15_etcut_ion_L1EM12") Et_Threhold = 15;
    if(trigname=="HLT_g15_etcut_ion_L1eEM15") Et_Threhold = 15;

    for(const auto eg : *Online_photon_info){  //Loop Over Offline Photons
      double eg_pt = eg->pt()*0.001;//GeV
      double eg_eta = eg->eta();  

      if(eg_pt>=Et_Threhold){
       online_ph_vec_M2[Et_trig_idx].push_back(eg);
      }
    } //Online photons that pass the EtCut Trigger found

    for(int i=0;i<int(1*online_ph_vec_M2[Et_trig_idx].size());i++){
      // const xAOD::Egamma *online_eg =  online_ph_vec_M2[Et_trig_idx].at(i);
      const xAOD::CaloCluster *online_eg =  online_ph_vec_M2[Et_trig_idx].at(i);
      Particle_Photon Ghost;
      int idx = 0;
      bool matched_once = false;

      if(photon_info->size()>0){
        for(const auto eg : *photon_info){  //Loop Over Offline Photons
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();   
          bool pass = false;

          // pass = HLT_TrigMatch(eg,online_eg,pass, dR_Threshold);
          pass = ET_TrigMatch(eg,online_eg,pass, dR_Threshold);
          
          if(pass){
            if(matched_once==false) {
              matched_once=true;

              Ghost.index = idx;
              Ghost.pT = eg_pt;
              Ghost.online_pT = online_eg->pt()*0.001; //GeV
              Ghost.eta = eg_eta;
              Ghost.phi = eg->phi();
              Ghost.Trigname_Pass = trigname;
              Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            }
            else{
              double online_pt =  online_eg->pt()*0.001; //GeV
              double new_offline_pt = eg_pt;
              double old_offline_pt = Ghost.pT;

              if(abs(online_pt - new_offline_pt)< abs(online_pt - old_offline_pt)){ //Only fill if new object is closer in Et
                Ghost.index = idx;
                Ghost.pT = eg_pt;
                Ghost.online_pT = online_eg->pt()*0.001; //GeV
                Ghost.eta = eg_eta;
                Ghost.phi = eg->phi();
                Ghost.Trigname_Pass = trigname;
                Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
                Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
                Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
              } 
            }
          }

          idx++;
        }//Loop over offline photon ends.
      }

      if(matched_once) Photon_Pass_EtCut_Trig[Et_trig_idx].push_back(Ghost); //Put into pass vector if a match occurs.
    }//Loop over online photons that pass the HLT Trigger  
  }
*/
  // for(int i=0;i<2;i++) ns<<"Size EtCut: online_ph_vec_M2["<<i<<"] is: "<<online_ph_vec_M2[i].size()<<endl;
  // for(int i=0;i<2;i++) ns<<"Size EtCut: Photon_Pass_EtCut_Trig["<<i<<"] is: "<<Photon_Pass_EtCut_Trig[i].size()<<endl;
  
  /*
  //Matching EtCut Triggers Using the match() function
  for(int Et_trig_idx=0;Et_trig_idx<nof_EtCut_Triggers;Et_trig_idx++){
    string trigname = m_trigList_EtCut_Photons.at(Et_trig_idx);
    Particle_Photon Ghost;
    int idx = 0;

    if(photon_info->size()>0){
      for(const auto eg : *photon_info){  //Loop Over Offline Photons
        bool pass = m_matchtool->match({eg}, trigname, dR_Threshold); //Check if the offline object matches with any online one
        double eg_pt = eg->pt()*0.001;//GeV
        double eg_eta = eg->eta(); 
        double val_DeltaR = 0.1;

        if(pass){
          Ghost.index = idx;
          Ghost.pT = eg_pt;
          Ghost.online_pT = eg_pt;
          Ghost.eta = eg_eta;
          Ghost.phi = eg->phi();
          Ghost.DeltaR = val_DeltaR;
          Ghost.Trigname_Pass = trigname;
          Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
          Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
          Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));

          // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
          //   if(!Ghost.type[photon_type_idx]) continue;
          //   Hist_pT_Matched_EtCut[Et_trig_idx][photon_type_idx][2]->Fill(eg->pt()*0.001); //GeV
          //   EtaPhiMap_Matched_EtCut[Et_trig_idx][photon_type_idx][2]->Fill(eg->eta(),eg->phi());
          // }

          Photon_Pass_EtCut_Trig[Et_trig_idx].push_back(Ghost); //Put into pass vector if a match occurs.
          // ns<<"Et_trig_idx: "<<Et_trig_idx<<", Index: "<<Ghost.index<<endl;
        }
        
        idx++;
      } //Loop over offline photon ends
    } //Offline size condition ends
  } //Et_Trig_idx loop ends
  */  

  

  std::vector<Particle_Photon> Photon_Pass_Id_Trig[nof_Id_Triggers];
  for(int i=0;i<nof_Id_Triggers;i++) Photon_Pass_Id_Trig[i].clear(); //Clear the vectors. Good Habits.
  
  
  key = "HLT_egamma_Photons";
  
  for(int Id_trig_idx=0;Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    string trigname = m_trigList_Id_Photons.at(Id_trig_idx);
    
    //Get the online objects in HLT_egamma_Photons that pass the trigger in online_ph_vec
    std::vector<const xAOD::Egamma*> online_ph_vec; online_ph_vec.clear();
    
    auto vec =  m_trigDecTool->features<xAOD::PhotonContainer>(trigname, TrigDefs::Physics ,key );      
    // ns<<"Id Vec Size: "<<int(1.0*vec.size())<<endl; 

    for( auto &featLinkInfo : vec ){
      if(! featLinkInfo.isValid() ) continue;
      const auto *feat = *(featLinkInfo.link);
      if(!feat) continue;
      // ns<<"Id Pass through vec"<<endl;
      // ns<<"Eta: "<<feat->eta()<<", phi: "<<feat->phi()<<", pT(GeV): "<<feat->pt()*0.001<<endl;      
      online_ph_vec.push_back(feat);
    }

    for(int i=0;i<int(1*online_ph_vec.size());i++){
      const xAOD::Egamma *online_eg =  online_ph_vec.at(i);
      // ns<<"Id Loop Eta: "<<online_eg->eta()<<", phi: "<<online_eg->phi()<<", pT(GeV): "<<online_eg->pt()*0.001<<endl;
      
      //Fill In the Histograms for All Online Photons
      // Hist_pT_Online_Id[Id_trig_idx][0]->Fill(online_eg->pt()*0.001);
      // EtaPhiMap_Online_Id[Id_trig_idx][0]->Fill(online_eg->eta(),online_eg->phi());

      Particle_Photon Ghost;
      int idx = 0;
      bool matched_once = false;

      if(photon_info->size()>0){
        for(const auto eg : *photon_info){  //Loop Over Offline Photons
          double eg_pt = eg->pt()*0.001;//GeV
          double eg_eta = eg->eta();   
          bool pass = false;

          pass = HLT_TrigMatch(eg,online_eg,pass, dR_Threshold);
          double val_DeltaR = xAOD::P4Helpers::deltaR(*eg, online_eg->eta(), online_eg->phi(), false);
          // DeltaR_Id[Id_trig_idx]->Fill(val_DeltaR);

          if(pass){
            // bool off_photon_type[3];
            // off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            // off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            // off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
            //   if(!off_photon_type[photon_type_idx]) continue;
            //   Hist_pT_Matched_Id[Id_trig_idx][photon_type_idx][0]->Fill(eg->pt()*0.001); //GeV
            //   EtaPhiMap_Matched_Id[Id_trig_idx][photon_type_idx][0]->Fill(eg->eta(),eg->phi());
            // }

            if(matched_once==false) {
              //Fill In the Histograms for Matched Online Photons
              // Hist_pT_Online_Id[Id_trig_idx][1]->Fill(online_eg->pt()*0.001);
              // EtaPhiMap_Online_Id[Id_trig_idx][1]->Fill(online_eg->eta(),online_eg->phi());

              matched_once=true;

              Ghost.index = idx;
              Ghost.pT = eg_pt;
              Ghost.online_pT = online_eg->pt()*0.001; //GeV
              Ghost.eta = eg_eta;
              Ghost.phi = eg->phi();
              Ghost.DeltaR = val_DeltaR;
              Ghost.Trigname_Pass = trigname;
              Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
              Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
              Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
            }
            else{
              double online_pt =  online_eg->pt()*0.001; //GeV
              double new_offline_pt = eg_pt;
              double old_offline_pt = Ghost.pT;

              if(abs(online_pt - new_offline_pt)< abs(online_pt - old_offline_pt)){ //Only fill if new object is closer in Et/pt
                Ghost.index = idx;
                Ghost.pT = eg_pt;
                Ghost.online_pT = online_eg->pt()*0.001; //GeV
                Ghost.eta = eg_eta;
                Ghost.phi = eg->phi();
                Ghost.DeltaR = val_DeltaR;
                Ghost.Trigname_Pass = trigname;
                Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
                Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
                Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));
              } 
            }
          }

          idx++;
        }//Loop over offline photon ends.
      }

      if(matched_once) {
        Photon_Pass_Id_Trig[Id_trig_idx].push_back(Ghost); //Put into pass vector if a match occurs.

        // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
        //   if(!Ghost.type[photon_type_idx]) continue;
        //   Hist_pT_Matched_Id[Id_trig_idx][photon_type_idx][1]->Fill(Ghost.pT); //GeV
        //   EtaPhiMap_Matched_Id[Id_trig_idx][photon_type_idx][1]->Fill(Ghost.eta,Ghost.phi);
        // }

        // ns<<"Id_trig_idx: "<<Id_trig_idx<<", Index: "<<Ghost.index<<endl;

        // if(Ghost.pT<15){ //Offline pT is less than threshold
        //   for(int photon_type=0;photon_type<max_photon_types;photon_type++){
        //     if(!Ghost.type[photon_type]) continue;
        //     Photon_Id_Bad[Id_trig_idx][0][photon_type]->Fill(Ghost.eta,Ghost.phi);
        //     Photon_Id_Bad[Id_trig_idx][1][photon_type]->Fill(Ghost.pT,Ghost.online_pT);
        //   }
        // }  

      }
      // ns<<" Photon_Pass_Id_Trig[Id_trig_idx] Size: "<<int(1.0*Photon_Pass_Id_Trig[Id_trig_idx].size())<<endl;
    }//Loop over online photons that pass the HLT Trigger 
  }
  
  /*
  //Matching Id Triggers Using the match() function
  for(int Id_trig_idx=0;Id_trig_idx<nof_Id_Triggers;Id_trig_idx++){
    string trigname = m_trigList_Id_Photons.at(Id_trig_idx);
    Particle_Photon Ghost;
    int idx = 0;

    if(photon_info->size()>0){
      for(const auto eg : *photon_info){  //Loop Over Offline Photons
        bool pass = m_matchtool->match({eg}, trigname, dR_Threshold); //Check if the offline object matches with any online one
        double eg_pt = eg->pt()*0.001;//GeV
        double eg_eta = eg->eta(); 
        double val_DeltaR = 0.1;

        if(pass){
          Ghost.index = idx;
          Ghost.pT = eg_pt;
          Ghost.online_pT = eg_pt;
          Ghost.eta = eg_eta;
          Ghost.phi = eg->phi();
          Ghost.DeltaR = val_DeltaR;
          Ghost.Trigname_Pass = trigname;
          Ghost.type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
          Ghost.type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
          Ghost.type[2] = bool(m_photonTightIsEMSelector->accept(eg));

          // for(int photon_type_idx=0;photon_type_idx<max_photon_types;photon_type_idx++){
          //   if(!Ghost.type[photon_type_idx]) continue;
          //   Hist_pT_Matched_Id[Id_trig_idx][photon_type_idx][2]->Fill(eg->pt()*0.001); //GeV
          //   EtaPhiMap_Matched_Id[Id_trig_idx][photon_type_idx][2]->Fill(eg->eta(),eg->phi());
          // }

          Photon_Pass_Id_Trig[Id_trig_idx].push_back(Ghost); //Put into pass vector if a match occurs.
          //  ns<<"Id_trig_idx: "<<Id_trig_idx<<", Index: "<<Ghost.index<<endl;
        }

        idx++;
      } //Loop over offline photon ends
    } //Offline size condition ends
  } //Id_Trig_idx loop ends
  */

  
  //Fill up the histograms

  //L1 wrt to MinBias
  // for(int L1_ref_trig = 0; L1_ref_trig<nof_L1_ref_triggers;L1_ref_trig++){
  //   string ref_trigname = m_L1_Ref_trigList.at(L1_ref_trig);
    
  //   for(int L1_trig=0;L1_trig<nof_L1_triggers;L1_trig++){
  //     string trigname = m_L1_trigList.at(L1_trig);
  //     int array_index = 0;
  //     if(trigname=="L1_EM12") array_index = 0;
  //     if(trigname=="L1_eEM15") array_index = 1;


  //     for(int i=0;i<int(Photon_Pass_L1_Trig[array_index].size()*1.0);i++){
  //       Particle_Photon ph = Photon_Pass_L1_Trig[array_index].at(i);
  //       int Eta_idx = Get_Eta_Index(ph.eta);
  //       double eg_pt = ph.pT;

  //       for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
  //           if(!ph.type[photon_type]) continue;

  //           if(Eta_idx>-1 && fcalEt_idx>-1) {
  //             Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcalEt_idx][Eta_idx]->Fill(eg_pt,prescale); //Particular fcal + Particular Eta
  //             Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
  //             Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta                   
  //           }
  //           if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
  //           if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta  

  //           Photon_Evt_L1[L1_trig][L1_ref_trig][photon_type][5][2]->Fill(eg_pt,prescale); //All fcalEt + All eta 
            
  //       }//photon type loop ends 
  //     }
  //   }
  // }
  
/*  
  //L1 wrt MinBias
  for(int L1_trig=0;L1_trig<nof_L1_triggers;L1_trig++){
    string trigname = m_L1_trigList.at(L1_trig);
    int array_index = 0;
    if(trigname=="L1_EM12") array_index = 0;
    if(trigname=="L1_eEM15") array_index = 1;

    float prescale = 1.0;
    float prescale_ref = 1.0; 

    for(int Ref_array_index = 0; Ref_array_index<nof_L1_ref_triggers;Ref_array_index++){
      string ref_trigname = m_L1_Ref_trigList.at(Ref_array_index);
      bool ref_trigDecision = trigDecisionTool->isPassed(ref_trigname);

      if(ref_trigDecision){
        //Photon Work Begins
        bool off_photon_type[max_photon_types]; //Category of Offline Photons. All,Loose,Medium,Tight
        if( photon_info->size()>0 && photon_info->size()>=int(Photon_Pass_L1_Trig[array_index].size()*1.0) ){
          for(const auto eg : *photon_info){  //Loop Over Offline Photons
            double eg_pt = eg->pt()*0.001;//GeV
            double eg_eta = eg->eta();   

            int Eta_idx = Get_Eta_Index(eg_eta);   

            off_photon_type[0] = bool(m_photonLooseIsEMSelector->accept(eg));
            off_photon_type[1] = bool(m_photonMediumIsEMSelector->accept(eg));
            off_photon_type[2] = bool(m_photonTightIsEMSelector->accept(eg));

            //Histograms showing all photons that passes the MinBias reference trigger
            for(int L1_trig=0;L1_trig<nof_L1_triggers;L1_trig++){
              for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
                if(!off_photon_type[photon_type]) continue;

                if(Eta_idx>-1 && fcalEt_idx>-1) {
                  Photon_Total_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][Eta_idx]->Fill(eg_pt,prescale_ref); //Particular fcal + Particular Eta
                  Photon_Total_L1[L1_trig][Ref_array_index][photon_type][5][Eta_idx]->Fill(eg_pt,prescale_ref);  //All fcal + Particular eta
                  Photon_Total_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale_ref); //Particular fcal + All eta                   
                }
                if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Total_L1[L1_trig][Ref_array_index][photon_type][5][Eta_idx]->Fill(eg_pt,prescale_ref);  //All fcal + Particular eta
                if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Total_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale_ref); //Particular fcal + All eta  

                Photon_Total_L1[L1_trig][Ref_array_index][photon_type][5][2]->Fill(eg_pt,prescale_ref); //All fcalEt + All eta 
                
              }//photon type loop ends for reference filling
            }//Loop over L1 trig ends
          }//Loop over offline photon ends.

          //Start filling the L1 Triggers that pass.
          for(int i=0;i<int(Photon_Pass_L1_Trig[array_index].size()*1.0);i++){
            Particle_Photon ph = Photon_Pass_L1_Trig[array_index].at(i);
            int Eta_idx = Get_Eta_Index(ph.eta);
            double eg_pt = ph.pT;

            for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
              if(!ph.type[photon_type]) continue;

              if(Eta_idx>-1 && fcalEt_idx>-1) {
                Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][Eta_idx]->Fill(eg_pt,prescale); //Particular fcal + Particular Eta
                Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
                Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta                   
              }
              if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
              if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta  

              Photon_Evt_L1[L1_trig][Ref_array_index][photon_type][5][2]->Fill(eg_pt,prescale); //All fcalEt + All eta 
              
            }//photon type loop ends 
          }//L1 Trigger fill loop ends
        }//Photon size condition
      }
    }    
  }



  //EtCut wrt L1
  for(int EtCut_trig = 0; EtCut_trig<nof_EtCut_Trigs_Photons;EtCut_trig++){
    string EtCut_Ref_Trigname = m_Ref_trigList_EtCut_Photons.at(EtCut_trig);
    int Ref_array_index = 0;
    if(EtCut_Ref_Trigname=="L1_EM12") Ref_array_index = 0;
    if(EtCut_Ref_Trigname=="L1_eEM15") Ref_array_index = 1;

    string EtCut_Trigname = m_trigList_EtCut_Photons.at(EtCut_trig);
    int array_index = 0;
    if(EtCut_Trigname=="HLT_g15_etcut_ion_L1EM12") array_index = 0;
    if(EtCut_Trigname=="HLT_g15_etcut_ion_L1eEM15") array_index = 1;

    // float prescale_ref = m_trigDecTool->getChainGroup(EtCut_Ref_Trigname)->getPrescale();
    // float prescale_tar = m_trigDecTool->getChainGroup(EtCut_Trigname)->getPrescale();

    ns<<"(EtCut_Ref_Trigname, Prescale): ("<<EtCut_Ref_Trigname<<" , "<<m_trigDecTool->getChainGroup(EtCut_Ref_Trigname)->getPrescale()<<")"<<endl;

    // float prescale = prescale_ref*prescale_tar;
    // float prescale = m_trigDecTool->getChainGroup(EtCut_Trigname)->getPrescale(); //Get Prescales
    
    float prescale = 1.0;
    float prescale_ref = 1.0;

    for(int i=0;i<int(Photon_Pass_L1_Trig[Ref_array_index].size()*1.0);i++){ //Loop over reference trigger
      Particle_Photon Ref_ph = Photon_Pass_L1_Trig[Ref_array_index].at(i);
      int Ref_photon_index = Ref_ph.index;
      double Ref_photon_eta = Ref_ph.eta;
      double Ref_photon_pT = Ref_ph.pT;

      int Eta_idx = Get_Eta_Index(Ref_photon_eta);
      double eg_pt = Ref_ph.online_pT;

      for(int j=0;j<int(Photon_Pass_EtCut_Trig[array_index].size()*1.0);j++){ //Loop over EtCut Trigger
        Particle_Photon ph = Photon_Pass_EtCut_Trig[array_index].at(j);
        int index = ph.index;
        double eta = ph.eta;
        double pT = ph.pT;

        if( (index==Ref_photon_index) && (eta==Ref_photon_eta) && (pT==Ref_photon_pT) ){ //Check reference and target trigger photons are the same
          //Fill In the Histograms for photons passing both
          for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
            if(!Ref_ph.type[photon_type]) continue;

            if(Eta_idx>-1 && fcalEt_idx>-1) {
              Photon_Evt_EtCut[EtCut_trig][photon_type][fcalEt_idx][Eta_idx]->Fill(eg_pt,prescale); //Particular fcal + Particular Eta
              Photon_Evt_EtCut[EtCut_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
              Photon_Evt_EtCut[EtCut_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta                   
            }
            if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Evt_EtCut[EtCut_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale);  //All fcal + Particular eta
            if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Evt_EtCut[EtCut_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale); //Particular fcal + All eta  

            Photon_Evt_EtCut[EtCut_trig][photon_type][5][2]->Fill(eg_pt,prescale); //All fcalEt + All eta 
            
          }//photon type loop ends

          break; //Because matching b/w reference and target triggers is one to one.
        }
      }

      //Now fill in all the photons that passes only the reference triggers
      for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
        if(!Ref_ph.type[photon_type]) continue;

        if(Eta_idx>-1 && fcalEt_idx>-1) {
          Photon_Total_EtCut[EtCut_trig][photon_type][fcalEt_idx][Eta_idx]->Fill(eg_pt,prescale_ref); //Particular fcal + Particular Eta
          Photon_Total_EtCut[EtCut_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale_ref);  //All fcal + Particular eta
          Photon_Total_EtCut[EtCut_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale_ref); //Particular fcal + All eta                   
        }
        if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Total_EtCut[EtCut_trig][photon_type][5][Eta_idx]->Fill(eg_pt,prescale_ref);  //All fcal + Particular eta
        if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Total_EtCut[EtCut_trig][photon_type][fcalEt_idx][2]->Fill(eg_pt,prescale_ref); //Particular fcal + All eta  

        Photon_Total_EtCut[EtCut_trig][photon_type][5][2]->Fill(eg_pt,prescale_ref); //All fcalEt + All eta 
          
      }//photon type loop ends
    }//Reference trigger loop ends

  }
*/  

  //Id wrt EtCut Triggers
  for(int Id_trig = 0; Id_trig<nof_Id_Triggers;Id_trig++){
    string Id_Ref_Trigname = m_Ref_trigList_Id_Photons.at(Id_trig);
    int Ref_array_index = 0;
    if(Id_Ref_Trigname=="HLT_g18_etcut_ion_L1EM12") Ref_array_index = 0;
    if(Id_Ref_Trigname=="HLT_g18_etcut_ion_L1eEM15") Ref_array_index = 1;

    string Id_Trigname = m_trigList_Id_Photons.at(Id_trig);
    int array_index = 0;
    if(Id_Trigname=="HLT_g20_loose_ion_L1EM12") array_index = 0;
    if(Id_Trigname=="HLT_g20_loose_ion_L1eEM15") array_index = 1;
    
    // float prescale_ref = m_trigDecTool->getChainGroup(Id_Ref_Trigname)->getPrescale();
    // float prescale_tar = m_trigDecTool->getChainGroup(Id_Trigname)->getPrescale();

    // float prescale = prescale_ref*prescale_tar;
    // float prescale = m_trigDecTool->getChainGroup(Id_Trigname)->getPrescale(); //Get Prescales
    
    float prescale = 1.0;
    float prescale_ref = 1.0; 

    for(int i=0;i<int(Photon_Pass_EtCut_Trig[Ref_array_index].size()*1.0);i++){ //Loop over reference EtCut trigger
      Particle_Photon Ref_ph = Photon_Pass_EtCut_Trig[Ref_array_index].at(i);
      int Ref_photon_index = Ref_ph.index;
      double Ref_photon_eta = Ref_ph.eta;
      double Ref_photon_pT = Ref_ph.pT;

      int Eta_idx = Get_Eta_Index(Ref_photon_eta);
      double eg_pt = Ref_ph.online_pT;
      
      for(int j=0;j<int(Photon_Pass_Id_Trig[array_index].size()*1.0);j++){  //Loop over Id Trigger
        Particle_Photon ph = Photon_Pass_Id_Trig[array_index].at(j);
        int index = ph.index;
        double eta = ph.eta;
        double pT = ph.pT;

        if( (index==Ref_photon_index) && (eta==Ref_photon_eta) && (pT==Ref_photon_pT) ){ //Check reference and target trigger photons are the same
          //Fill In the Histograms for photons passing both
          for(int axes_idx=0;axes_idx<nof_xaxes;axes_idx++){
            double Hist_xval;
            if(axes_idx==0) Hist_xval = Ref_ph.pT;
            if(axes_idx==1) Hist_xval = m_sumEt;
            if(axes_idx==2) Hist_xval = Ref_ph.eta;

            for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
              if(!Ref_ph.type[photon_type]) continue;

              for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
                if( !(Ref_ph.pT > pT_bin_Vals[pT_idx]) ) continue;

                if(Eta_idx>-1 && fcalEt_idx>-1) {
                  Photon_Evt_Id[Id_trig][photon_type][fcalEt_idx][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + Particular Eta
                  Photon_Evt_Id[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale);  //All fcal + Particular eta
                  Photon_Evt_Id[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + All eta                   
                }
                if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Evt_Id[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale);  //All fcal + Particular eta
                if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Evt_Id[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + All eta  
                

                Photon_Evt_Id[Id_trig][photon_type][max_fcalEt_bins-1][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //All fcalEt + All eta + All pT

              }//pT_idx loop ends
            }//photon type loop ends
          }//x axis loop ends

          break; //Because matching b/w reference and target triggers is one to one.
        }

      }

      //Now fill in all the photons that passes only the reference triggers
      for(int axes_idx=0;axes_idx<nof_xaxes;axes_idx++){
        double Hist_xval;
        if(axes_idx==0) Hist_xval = Ref_ph.pT;
        if(axes_idx==1) Hist_xval = m_sumEt;
        if(axes_idx==2) Hist_xval = Ref_ph.eta;

        for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
          if(!Ref_ph.type[photon_type]) continue;

          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            if( !(Ref_ph.pT > pT_bin_Vals[pT_idx]) ) continue;

            if(Eta_idx>-1 && fcalEt_idx>-1) {
              Photon_Total_Id[Id_trig][photon_type][fcalEt_idx][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + Particular Eta
              Photon_Total_Id[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref);  //All fcal + Particular eta
              Photon_Total_Id[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + All eta                   
            }
            if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Total_Id[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref);  //All fcal + Particular eta
            if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Total_Id[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + All eta  

            Photon_Total_Id[Id_trig][photon_type][max_fcalEt_bins-1][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //All fcalEt + All eta 
          
          }//pT_idx loop ends 
        }//photon type loop ends
      }//x axes loop ends
    }//Reference trigger loop ends

  }
  
  
  //Id wrt L1 Triggers
  for(int Id_trig = 0; Id_trig<nof_Id_Triggers;Id_trig++){
    string Id_Ref_Trigname = m_Ref_trigList_Id_L1_Photons.at(Id_trig);
    int Ref_array_index = 0;
    if(Id_Ref_Trigname=="L1_EM12") Ref_array_index = 0;
    if(Id_Ref_Trigname=="L1_eEM15") Ref_array_index = 1;

    string Id_Trigname = m_trigList_Id_Photons.at(Id_trig);
    int array_index = 0;
    if(Id_Trigname=="HLT_g20_loose_ion_L1EM12") array_index = 0;
    if(Id_Trigname=="HLT_g20_loose_ion_L1eEM15") array_index = 1;
    
    // float prescale_ref = m_trigDecTool->getChainGroup(Id_Ref_Trigname)->getPrescale();
    // float prescale_tar = m_trigDecTool->getChainGroup(Id_Trigname)->getPrescale();

    // float prescale = prescale_ref*prescale_tar;
    // float prescale = m_trigDecTool->getChainGroup(Id_Trigname)->getPrescale(); //Get Prescales
    
    float prescale = 1.0;
    float prescale_ref = 1.0; 

    for(int i=0;i<int(Photon_Pass_L1_Trig[Ref_array_index].size()*1.0);i++){ //Loop over reference L1 trigger
      Particle_Photon Ref_ph = Photon_Pass_L1_Trig[Ref_array_index].at(i);
      int Ref_photon_index = Ref_ph.index;
      double Ref_photon_eta = Ref_ph.eta;
      double Ref_photon_pT = Ref_ph.pT;

      int Eta_idx = Get_Eta_Index(Ref_photon_eta);
      double eg_pt = Ref_ph.online_pT;
      
      for(int j=0;j<int(Photon_Pass_Id_Trig[array_index].size()*1.0);j++){  //Loop over Id Trigger
        Particle_Photon ph = Photon_Pass_Id_Trig[array_index].at(j);
        int index = ph.index;
        double eta = ph.eta;
        double pT = ph.pT;

        if( (index==Ref_photon_index) && (eta==Ref_photon_eta) && (pT==Ref_photon_pT) ){ //Check reference and target trigger photons are the same
          //Fill In the Histograms for photons passing both
          for(int axes_idx=0;axes_idx<nof_xaxes;axes_idx++){
            double Hist_xval;
            if(axes_idx==0) Hist_xval = Ref_ph.pT;
            if(axes_idx==1) Hist_xval = m_sumEt;
            if(axes_idx==2) Hist_xval = Ref_ph.eta;

            for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
              if(!Ref_ph.type[photon_type]) continue;

              for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
                if( !(Ref_ph.pT > pT_bin_Vals[pT_idx]) ) continue;

                if(Eta_idx>-1 && fcalEt_idx>-1) {
                  Photon_Evt_Id_L1[Id_trig][photon_type][fcalEt_idx][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + Particular Eta
                  Photon_Evt_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale);  //All fcal + Particular eta
                  Photon_Evt_Id_L1[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + All eta                   
                }
                if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Evt_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale);  //All fcal + Particular eta
                if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Evt_Id_L1[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //Particular fcal + All eta  
                

                Photon_Evt_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale); //All fcalEt + All eta + All pT

              }//pT_idx loop ends
            }//photon type loop ends
          }//x axis loop ends

          break; //Because matching b/w reference and target triggers is one to one.
        }

      }

      //Now fill in all the photons that passes only the reference triggers
      for(int axes_idx=0;axes_idx<nof_xaxes;axes_idx++){
        double Hist_xval;
        if(axes_idx==0) Hist_xval = Ref_ph.pT;
        if(axes_idx==1) Hist_xval = m_sumEt;
        if(axes_idx==2) Hist_xval = Ref_ph.eta;

        for(int photon_type=0;photon_type<max_photon_types;photon_type++){  
          if(!Ref_ph.type[photon_type]) continue;

          for(int pT_idx=0;pT_idx<max_pT_bins;pT_idx++){
            if( !(Ref_ph.pT > pT_bin_Vals[pT_idx]) ) continue;

            if(Eta_idx>-1 && fcalEt_idx>-1) {
              Photon_Total_Id_L1[Id_trig][photon_type][fcalEt_idx][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + Particular Eta
              Photon_Total_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref);  //All fcal + Particular eta
              Photon_Total_Id_L1[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + All eta                   
            }
            if(fcalEt_idx==-1 && Eta_idx>-1)  Photon_Total_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][Eta_idx][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref);  //All fcal + Particular eta
            if(fcalEt_idx>-1 && Eta_idx==-1)  Photon_Total_Id_L1[Id_trig][photon_type][fcalEt_idx][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //Particular fcal + All eta  

            Photon_Total_Id_L1[Id_trig][photon_type][max_fcalEt_bins-1][max_Eta_bins-1][pT_idx][axes_idx]->Fill(Hist_xval,prescale_ref); //All fcalEt + All eta 
          
          }//pT_idx loop ends 
        }//photon type loop ends
      }//x axes loop ends
    }//Reference trigger loop ends

  }
  

  // ns<<endl<<"\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\";
  // ns<<endl;

  return StatusCode::SUCCESS;
}



StatusCode MyxAODAnalysis :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  ANA_MSG_INFO ("In Finalize.");

  // std::string outfilename = "/afs/cern.ch/user/a/adimri/Egamma_Trigger/Output_New.root";
  std::string outfilename = "Output_New.root";
  Save_Hists(outfilename);

  // ns<<endl<<"....................End....................."<<endl;
  // ns.close();
  return StatusCode::SUCCESS;
}
