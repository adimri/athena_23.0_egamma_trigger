#ifndef MyAnalysis_MyxAODAnalysis_H
#define MyAnalysis_MyxAODAnalysis_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include "AthenaBaseComps/AthAlgorithm.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"
#include "TrigEgammaMatchingTool/TrigEgammaMatchingToolMT.h"
#include <AsgTools/ToolHandle.h> 

//Taken from Qipeng
#include "TriggerMatchingTool/R3MatchingTool.h"
// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h> // GRL
#include <AsgTools/AnaToolHandle.h>
#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <MCTruthClassifier/MCTruthClassifier.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>


#include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

#include "TrigEgammaMatchingTool/ITrigEgammaMatchingTool.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include <xAODTrigger/EmTauRoIContainer.h>
#include <xAODTrigger/eFexEMRoIContainer.h>
#include "GaudiKernel/ToolHandle.h"
#include <TH1.h>
#include <TH2.h>
#include <fstream>

// #include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"
#include "TriggerMatchingTool/IMatchingTool.h"
#include <map>

//Include from R3 MatchingTool
#include "TriggerMatchingTool/IMatchScoringTool.h" 

//Some Basic Global Variables
const int nof_L1_Triggers = 2;
const int nof_Id_Triggers = 2;
const int nof_EtCut_Triggers = 2;


const int max_photon_types = 3; //Loose, Medium and Tight
const int max_fcalEt_bins = 11; //0.25,0.75,1.25, ... , 4.75. And All.
const int max_Eta_bins = 3; //Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

const int nof_xaxes = 3; //pT, fcalEt, Eta.
const int max_pT_bins = 3; //pT bins. pT>20, pT>35, All pT.
const double pT_bin_Vals[3] = {20,35,0};

class MyxAODAnalysis : public EL::AnaAlgorithm
{
  private:


  public:
  std::ofstream ns; //File Handle

  // this is a standard algorithm constructor
  MyxAODAnalysis (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;
  
  //My Functions
  void Init_Hists();
  void Save_Hists(std::string);

  //Likelihood Tool Selectors
  asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grl; //grl tool
  AsgPhotonIsEMSelector *m_photonTightIsEMSelector; 
  AsgPhotonIsEMSelector *m_photonLooseIsEMSelector; 
  AsgPhotonIsEMSelector *m_photonMediumIsEMSelector; 

  //Trigger Decision Tools
  ToolHandle<Trig::TrigDecisionTool> m_trigDecTool; //Using directly the Trigdecision Tool
  ToolHandle<Trig::ITrigDecisionTool> trigDecisionTool; //Trig Decision Tool
  ToolHandle<Trig::IMatchingTool> m_matchtool;  //Matching 
  // ToolHandle<TrigEgammaMatchingToolMT> m_matchTool_MT;
  // const ToolHandle<TrigEgammaMatchingToolMT>& match() const {return m_matchTool_New;}

  //L1 wrt MinBias
  std::vector<std::string> m_L1_trigList = {"L1_EM12", "L1_eEM15"};

  std::vector<std::string> m_L1_Ref_trigList = {
  "HLT_mb_sptrk_pc_L1ZDC_A_C_VTE50",
  "HLT_noalg_L1TE600p0ETA49",
  "HLT_noalg_L1TE50_VTE600p0ETA49"
  }; //3 reference for each the L1 triggers

  // TH1D *Photon_Evt_L1[2][3][3][6][3];
  // TH1D *Photon_Total_L1[2][3][3][6][3];
  //First [] is for the L1 triggers
  //Second [] is for the MinBias Triggers wrt to which we need to compute efficiency
  //Third [] is for loose,medium and tight photons
  //Fourth [] is for Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
  //Fifth [] is fot Eta Coverage: Barrel, Eta, All

  // TH2D *Photon_L1_Bad[nof_L1_Triggers][2][3];
  //First [] is for the L1 triggers
  //Second [] is for the EtaPhi and Offline-Online pT. x-y
  //Third [] is for loose,medium and tight photons

  //EtCut wrt L1
  std::vector<std::string> m_trigList_EtCut_Photons= {
    "HLT_g18_etcut_ion_L1EM12",
    "HLT_g18_etcut_ion_L1eEM15"
  };

  std::vector<std::string> m_Ref_trigList_EtCut_Photons= {
    "L1_EM12",
    "L1_eEM15" 
  };

  
  // TH1D *Photon_Evt_EtCut[2][3][6][3];
  // TH1D *Photon_Total_EtCut[2][3][6][3];
  //First []: Nof of Triggers.
  //Second []: Loose, Medium, Tight Photons
  //Third []: Fcal Et limits. (0-1,1-2,2-3,3-4,4-6,All)
  //Fourth []: Eta limits.  0->|eta| < 1.37 (Barrel), 1->1.52 < |eta| < 2.37 (End-Caps), 2->All Eta

  // TH2D *Photon_EtCut_Bad[nof_EtCut_Triggers][2][3];

  //HLT wrt EtCut
  std::vector<std::string> m_trigList_Id_Photons= {
    "HLT_g20_loose_ion_L1EM12",
    "HLT_g20_loose_ion_L1eEM15"
  };

  //Reference Triglist Photons
  std::vector<std::string> m_Ref_trigList_Id_Photons= {
    "HLT_g18_etcut_ion_L1EM12",
    "HLT_g18_etcut_ion_L1eEM15"
  };

  std::vector<std::string> m_Ref_trigList_Id_L1_Photons= {
    "L1_EM12",
    "L1_eEM15" 
  };
  

  TH1D *Photon_Evt_Id[nof_Id_Triggers][max_photon_types][max_fcalEt_bins][max_Eta_bins][max_pT_bins][nof_xaxes];
  TH1D *Photon_Total_Id[nof_Id_Triggers][max_photon_types][max_fcalEt_bins][max_Eta_bins][max_pT_bins][nof_xaxes];

  TH1D *Photon_Evt_Id_L1[nof_Id_Triggers][max_photon_types][max_fcalEt_bins][max_Eta_bins][max_pT_bins][nof_xaxes];
  TH1D *Photon_Total_Id_L1[nof_Id_Triggers][max_photon_types][max_fcalEt_bins][max_Eta_bins][max_pT_bins][nof_xaxes];
  
  //First []: Nof of Triggers.

  // TH2D *Photon_Id_Bad[nof_Id_Triggers][2][3];
 
  /*
  //Histograms
  //For Offline Photons Only
  TH1D *Hist_pT_All[max_photon_types];
  TH2D *EtaPhiMap_All[max_photon_types];

  //For L1
  TH1D *Hist_pT_Online_L1[nof_L1_Triggers][2];  //Online All and Matched.
  TH2D *EtaPhiMap_Online_L1[nof_L1_Triggers][2]; //Corresponding EtaPhiMap

  TH1D *Hist_pT_Matched_L1[nof_L1_Triggers][max_photon_types][2];    //Offline(All Match), Offline (Actual Match)
  TH2D *EtaPhiMap_Matched_L1[nof_L1_Triggers][max_photon_types][2];  //Corresponding EtaPhiMap

  TH1D *DeltaR_L1[nof_L1_Triggers]; //Distribution of deltaR irrespective of match.

  //For EtCut
  TH1D *Hist_pT_Online_EtCut[nof_EtCut_Triggers][2]; //Online All and Matched.
  TH2D *EtaPhiMap_Online_EtCut[nof_EtCut_Triggers][2]; //Corresponding EtaPhiMap

  TH1D *Hist_pT_Matched_EtCut[nof_EtCut_Triggers][max_photon_types][3]; //Offline(All Match), Offline (Actual Match), Offline (match() function)
  TH2D *EtaPhiMap_Matched_EtCut[nof_EtCut_Triggers][max_photon_types][3];  //Corresponding EtaPhiMap

  TH1D *DeltaR_EtCut[nof_EtCut_Triggers]; //Distribution of deltaR irrespective of match.

  //For ID
  TH1D *Hist_pT_Online_Id[nof_Id_Triggers][2]; //Online All and Matched.
  TH2D *EtaPhiMap_Online_Id[nof_Id_Triggers][2]; //Corresponding EtaPhiMap

  TH1D *Hist_pT_Matched_Id[nof_Id_Triggers][max_photon_types][3]; //Offline(All Match), Offline (Actual Match), Offline (match() function)
  TH2D *EtaPhiMap_Matched_Id[nof_Id_Triggers][max_photon_types][3];  //Corresponding EtaPhiMap

  TH1D *DeltaR_Id[nof_Id_Triggers]; //Distribution of deltaR irrespective of match.
  */

  //Function to get relevant indices
  int Get_FcalEt_Index(double Sum_fcalEt);
  int Get_Eta_Index(double Eta_Val);

  //Function  to perform the matching by hand
  bool L1_EM12_TrigMatch(const xAOD::Photon *eg, const xAOD::EmTauRoI *EMTauRoI, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM12 matching
  bool L1_eEM15_TrigMatch(const xAOD::Photon *eg, const xAOD::eFexEMRoI *eEM15, bool match=false, double dR_Threshold=0.1);  //By hand execution of L1_EM15 matching
  bool HLT_TrigMatch(const xAOD::Photon *eg, const xAOD::Egamma *online_eg, bool match=false, double dR_Threshold=0.1);  //By hand execution of ID Triggers matching
  bool ET_TrigMatch(const xAOD::Photon *eg, const xAOD::CaloCluster *online_eg, bool match=false, double dR_Threshold=0.1);  //By hand execution of ET Triggers matching
};
#endif