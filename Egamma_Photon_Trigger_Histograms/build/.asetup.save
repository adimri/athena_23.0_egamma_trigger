#Release cmake

export LANG="C"
export LC_ALL="C"
export COOL_ORA_ENABLE_ADAPTIVE_OPT="Y"
export ASETUP_PRINTLEVEL="0"
export BINARY_TAG="x86_64-centos7-gcc11-opt"
export CMTCONFIG="x86_64-centos7-gcc11-opt"

if [ -d /tmp/adimri ]; then
   export ASETUP_SYSBIN=`mktemp -d /tmp/adimri/.asetup-sysbin-XXXXXX_$$`
else
   export ASETUP_SYSBIN=`mktemp -d /home/adimri/Egamma_Photon_Trigger_Histograms/build/.asetup-sysbin-XXXXXX_$$`
fi
source $AtlasSetup/scripts/sys_exe-alias.sh ''
if [ -n "${MAKEFLAGS:+x}" ]; then
    asetup_flags=`echo ${MAKEFLAGS} | \grep ' -l'`
    if [ -z "${asetup_flags}" ]; then
        export MAKEFLAGS="${MAKEFLAGS} -l10"
    fi
else
    export MAKEFLAGS="-j10 -l10"
fi
source /cvmfs/sft.cern.ch/lcg/releases/gcc/11.2.0-8a51a/x86_64-centos7/setup.sh
if [ -z "${CC:+x}" ]; then
    export CC=`\env which gcc 2>/dev/null`
    [[ -z "$CC" ]] && unset CC
fi
if [ -z "${CXX:+x}" ]; then
    export CXX=`\env which g++ 2>/dev/null`
    [[ -z "$CXX" ]] && unset CXX
fi
if [ -z "${CUDAHOSTCXX:+x}" ]; then
    export CUDAHOSTCXX=`\env which g++ 2>/dev/null`
    [[ -z "$CUDAHOSTCXX" ]] && unset CUDAHOSTCXX
fi
if [ -z "${FC:+x}" ]; then
    export FC=`\env which gfortran 2>/dev/null`
    [[ -z "$FC" ]] && unset FC
fi
export CMAKE_NO_VERBOSE="1"
type lsetup >/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
   source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet
fi
source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh --quiet "cmake 3.24.3"
if [ -z "${AtlasSetup:+x}" ]; then
    export AtlasSetup="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/AtlasSetup/V02-02-10/AtlasSetup"
    export AtlasSetupVersion="AtlasSetup-02-02-10"
fi
export FRONTIER_SERVER="(serverurl=http://atlasfrontier-local.cern.ch:8000/atlr)(serverurl=http://atlasfrontier-ai.cern.ch:8000/atlr)(proxyurl=http://ca-proxy-atlas.cern.ch:3128)(proxyurl=http://ca-proxy-meyrin.cern.ch:3128)(proxyurl=http://ca-proxy.cern.ch:3128)(proxyurl=http://atlasbpfrontier.cern.ch:3127)(proxyurl=http://atlasbpfrontier.fnal.gov:3127)"
export ATLAS_POOLCOND_PATH="/cvmfs/atlas-condb.cern.ch/repo/conditions"
export ATLAS_DB_AREA="/cvmfs/atlas.cern.ch/repo/sw/database"
export DBRELEASE_OVERRIDE="current"
export SITEROOT="/cvmfs/atlas.cern.ch/repo/sw/software/23.0"
export AtlasBaseDir="/cvmfs/atlas.cern.ch/repo/sw/software/23.0"
export LCG_RELEASE_BASE="/cvmfs/atlas.cern.ch/repo/sw/software/23.0/sw/lcg/releases"
export AtlasBuildStamp="2024-01-10T0231"
export AtlasReleaseType="stable"
export AtlasBuildBranch="23.0"
export AtlasProject="Athena"
export TDAQ_RELEASE_BASE="/cvmfs/atlas.cern.ch/repo/sw/tdaq/offline"
export ATLAS_RELEASE_BASE="/cvmfs/atlas.cern.ch/repo/sw/software/23.0"
export ATLAS_RELEASEDATA="/cvmfs/atlas.cern.ch/repo/sw/software/23.0/atlas/offline/ReleaseData"
export AtlasArea="/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.54"
export G4PATH="/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Geant4"
export AtlasVersion="23.0.54"
source /cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.54/InstallArea/x86_64-centos7-gcc11-opt/setup.sh
asetup_status=$?
if [ ${asetup_status} -ne 0 ]; then
    \echo "AtlasSetup(ERROR): sourcing release setup script (/cvmfs/atlas.cern.ch/repo/sw/software/23.0/Athena/23.0.54/InstallArea/x86_64-centos7-gcc11-opt/setup.sh) failed"
fi
export TestArea="/home/adimri/Egamma_Photon_Trigger_Histograms/build"
echo $LD_LIBRARY_PATH | egrep "LCG_[^/:]*/curl/" >/dev/null
if [ $? -eq 0 ]; then
    alias_sys_exe_envU git
fi
\expr 1 \* 1 + 1 >/dev/null 2>&1
if [ $? -ne 0 ]; then
    echo -e '\nMaking workaround-alias for expr on this *OLD* machine'; alias_sys_exe expr
fi
export PATH="${ASETUP_SYSBIN}:${PATH}"

#Release Summary as follows:
#Release base=/cvmfs/atlas.cern.ch/repo/sw/software/23.0
#Release project=Athena
#Release releaseNum=23.0.54
#Release asconfig=x86_64-centos7-gcc11-opt

# Execute user-specified epilog

source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/swConfig/asetup/asetupEpilog.sh
script_status=$?
if [ ${script_status} -ne 0 ]; then
    \echo "AtlasSetup(ERROR): User-specified epilog (source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/swConfig/asetup/asetupEpilog.sh) reported failure (error ${script_status})"
fi
